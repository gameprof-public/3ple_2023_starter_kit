using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour
{
    private enum RotationAxis { X, Y, Z}

    [SerializeField] private RotationAxis rotationAxis = RotationAxis.Z;

    [SerializeField] private float rotationPerSecond;
    void Update()
    {
        switch (rotationAxis)
        {
            case RotationAxis.X:
                transform.Rotate(rotationPerSecond * Time.deltaTime, 0, 0);
                break;
            case RotationAxis.Y:
                transform.Rotate(0, rotationPerSecond * Time.deltaTime, 0);
                break;
            case RotationAxis.Z:
                transform.Rotate(0, 0, rotationPerSecond * Time.deltaTime);
                break;
        }
    }
}
