using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classes can inherit from this in order to be easily interactable with a button.
/// </summary>
[System.Serializable]
public class ButtonInteractable : MonoBehaviour
{
    public virtual void OnPress()
    {
    }
    public virtual void OnUnpress()
    {
    }

    public virtual void OnDrawGizmosSelected()
    {
    }
}
