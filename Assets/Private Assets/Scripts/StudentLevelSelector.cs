using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class StudentLevelSelector : MonoBehaviour {
    static public bool INITED {
        get {
            return ( STUDENT_NAME != "" ) && ( SCENE_NAME != "" );
        }
    }
    public const string MAIN_SCENE_NAME = "____Main Menu";
    static public string STUDENT_NAME = "";
    static public string SCENE_NAME = "";
    static public List<string> SCENE_NAMES;

    [Header( "Inscribed" )]
    public TextAsset textStudents;
    public TextAsset textScenes;
    public TMP_Dropdown dropStudents, dropScenes;
    public XnTelemetry.SO_TelemetrySettings telemetrySettings;

    //[Header( "Actions" )]
    //public bool checkToGenerateSceneList = false;

    // Start is called before the first frame update
    void Start() {
        // Pull student names
        List<string> studentNames = new List<string>( textStudents.text.Split( '\n' ) );
        //studentNames.Sort();
        dropStudents.ClearOptions();
        dropStudents.AddOptions( studentNames );

        // Scene names
        GenerateSceneList();
        dropScenes.ClearOptions();
        dropScenes.AddOptions( SCENE_NAMES );
    }

    public void LaunchScene() {
        telemetrySettings.playerName = STUDENT_NAME = dropStudents.options[dropStudents.value].text;
        telemetrySettings.sceneName = SCENE_NAME = dropScenes.options[dropScenes.value].text;
        //Scene scene = SceneManager.GetSceneByName(SCENE_NAME);

        XnBugReporter_3PLE_Connection.SetVals( STUDENT_NAME, SCENE_NAME );

        Debug.Log($"Attempting to load scene \"{SCENE_NAME}\".");
        SceneManager.LoadScene(SCENE_NAME);


        //Debug.Log($"Manually loading scene \"_Rocky_CharacterPlayground\".");
        //SceneManager.LoadScene("_Rocky_CharacterPlayground");
    }

    void GenerateSceneList() {
        //int numScenes = SceneManager.sceneCountInBuildSettings;
        //SCENE_NAMES = new List<string>();
        //for (int buildIndex = 1; buildIndex < numScenes; buildIndex++) {
        //    SCENE_NAMES.Add(SceneManager.GetSceneByBuildIndex(buildIndex).name);
        //}


        SCENE_NAMES = new List<string>(textScenes.text.Split('\n'));
        for (int i = 0; i < SCENE_NAMES.Count; i++) {
            SCENE_NAMES[i] = SCENE_NAMES[i].Trim();
        }
        SCENE_NAMES.Remove(MAIN_SCENE_NAME);
        SCENE_NAMES.Sort();

        //for (int i=0; i<num; i++) {
        //    var scene = SceneManager.GetSceneByBuildIndex( i );
        //    if ( scene.name != "____Main Menu" ) {
        //        sceneNames.Add( scene.name );
        //    }
        //}
        //sceneNames.Sort();
    }

//#if UNITY_EDITOR
//    private void OnDrawGizmos() {
//        if ( Application.isEditor ) {
//            if ( checkToGenerateSceneList ) {
//                List<string> scenes = new List<string>();
//                //foreach ( EditorBuildSettingsScene scene in EditorBuildSettings.scenes ) {
//                //    if ( scene.enabled )
//                //        scenes.Add( scene.name );
//                //}
//                checkToGenerateSceneList = false;
//            }
//        }
//    }
//#endif
}