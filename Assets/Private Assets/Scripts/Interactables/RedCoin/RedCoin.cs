using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(MeshFlasher))]
public class RedCoin : Pickup
{
    private RedCoinChallengeStarter myStarter;

    private MeshFlasher myMeshFlasher;

    private void Start()
    {
        InitializeRedCoin();
    }

    public void InitializeRedCoin()
    {
        //insure our layer is the hitbox layer
        gameObject.layer = LayerMask.NameToLayer("Hitbox");
        GetComponent<SphereCollider>().radius = 1; //Set this here as well, since coins already in levels are disconnected from the prefab.

        if (myMeshFlasher == null)
        {
            myMeshFlasher = GetComponent<MeshFlasher>();
        }
    }

    public void StartFlashing()
    {
        if (myMeshFlasher == null)
        {
            Debug.LogWarning("Red coin is mising a mesh flasher.");
        }
        else
        {
            myMeshFlasher.StartFlashing();
        }
    }

    public void StopFlashing()
    {
        if (myMeshFlasher == null)
        {
            Debug.LogWarning("Red coin is mising a mesh flasher.");
        }
        else
        {
            myMeshFlasher.HaltFlashing();
        }
    }

    protected override void GetPickedUp()
    {
        //Report my pickup to my parent object
        myStarter.ReportRedCoinGrab(this);

        DisableMe();
    }

    public void DisableMe()
    {
        gameObject.SetActive(false);
    }

    public void EnableMe()
    {
        gameObject.SetActive(true);
    }

    public void RegisterChallengeStarter(RedCoinChallengeStarter rccs)
    {
        myStarter = rccs;
    }
#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        transform.parent?.GetComponent<RedCoinChallengeStarter>()?.OnDrawGizmosSelected();
    }
#endif
}

#if UNITY_EDITOR
//Uncomment this block to disable this script in the editor
[CustomEditor(typeof(RedCoin))]
public class RedCoinEditor : Editor
{
    GUIStyle style;

    public override void OnInspectorGUI()
    {
        if ( style == null ) {
            style = EditorStyles.label;
            style.fontStyle = FontStyle.Italic;
        }
    
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
            base.OnInspectorGUI();
        } else {
            GUILayout.Label("You do not need to edit any of these fields", style);
        }
    }
}
#endif