using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XnTools;

/// <summary>
/// A class for trampoline-like objects that will bounce the player
/// </summary>
public class Bouncer : MonoBehaviour
{
    [SerializeField] private float bounceForce = 10f;
    private float lastBounceTime = -1;
    /// <summary>
    /// This function should be called by a hitbox on the bouncer
    /// </summary>
    public void CheckIfPlayerAndBounce(Collider coll) {
        //Make sure it's the player
        RockyCharacterController rCC = RockyCharacterController.GetRCCFromCollider(coll);
        if ( rCC == null ) return;

        //BOUNCE
        //Local space
        if ( lastBounceTime != Time.time ) {
            Vector3 bounceVel = transform.up * bounceForce;
            rCC.HandleOtherBounce( bounceVel );
            //rCC.AddVelocity( bounceVel, eAxes.y, true );
            lastBounceTime = Time.time;
        }
        //
        // Vector3 velToAdd = Vector3.up * bounceForce;
        //
        // velToAdd = transform.TransformVector(velToAdd);
        //
        // pCon.ZeroOutVelocity();
        // pCon.AddVelocity(velToAdd);
        // pCon.ForceJumpState();
    }

    #if UNITY_EDITOR
    private void OnDrawGizmos() {
        Handles.color = Color.cyan;
        Vector3[] pts = new Vector3[2];
        pts[0] = transform.position + transform.up;
        pts[1] = pts[0] + ( transform.up * ( bounceForce / 10f ) );
        Handles.DrawAAPolyLine( 10, pts );
        Handles.ConeHandleCap(
            0,
            pts[1],
            transform.rotation * Quaternion.LookRotation(Vector3.up),
            0.5f,
            EventType.Repaint
        );
        // Gizmos.color = Color.green;
        // Gizmos.DrawRay(transform.position, transform.up*(bounceForce/4f));
    }
    #endif
}
