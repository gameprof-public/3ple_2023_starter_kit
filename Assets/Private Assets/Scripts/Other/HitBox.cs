using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class HitBox : MonoBehaviour {
    [SerializeField] private HitBoxEvent onHitboxEnter;
    [SerializeField] private HitBoxEvent onHitboxExit;


    [SerializeField] private HitBoxClassEvent onHitboxClassEnter;
    [SerializeField] private HitBoxClassEvent onHitboxClassExit;

    //public Collider collThatHitMe = null; // I thought I would need this but instead made onHitboxClass___ UnityEvents

    private void OnTriggerEnter(Collider coll) {
        if ( RockyCharacterController.IsRCCCollider( coll ) ) {
            if ( onHitboxClassEnter.GetPersistentEventCount() > 0 ) {
                onHitboxClassEnter.Invoke( coll, this );
            } else {
                onHitboxEnter.Invoke( coll );
            }
        }
    }

    private void OnTriggerExit(Collider coll) {
        if ( RockyCharacterController.IsRCCCollider( coll ) ) {
            if ( onHitboxClassExit.GetPersistentEventCount() > 0 ) {
                onHitboxClassExit.Invoke( coll, this );
            } else {
                onHitboxExit.Invoke( coll );
            }
        }
    }

    private void Awake() {
        collider = GetComponent<Collider>();
    }

    private Collider collider;
    
    public float hitBoxBottomY {
        get {
            if (collider != null) return collider.bounds.min.y;
            return transform.position.y;
        }
    }

    public float hitBoxTopY {
        get {
            if ( collider != null ) return collider.bounds.max.y;
            return transform.position.y;
        }
    }
}

[System.Serializable]
public class HitBoxEvent: UnityEvent<Collider> { }

[System.Serializable]
public class HitBoxClassEvent : UnityEvent<Collider, HitBox> { }
