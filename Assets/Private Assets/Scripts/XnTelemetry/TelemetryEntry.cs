#define EXPLICIT_RECTS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace XnTelemetry {


    [System.Serializable]
    public class TelemetryEntry {
        static string[] HEADERS;

        public bool show = false;
        public int recNum;
        public string project;
        public string semester;
        public string version;
        public string sceneName;
        public string playerName;
        public string dateTime;
        public string data;
        [HideInInspector]
        public Telemetry_Cloud.Telem[] telems;
        [HideInInspector]
        public float maxTime = -1f;

        static public void SET_HEADERS(string headerString) {
            string[] bits = headerString.Split('\t');
            for (int i = 0; i < bits.Length; i++) {
                bits[i] = bits[i].Trim();
            }
            HEADERS = bits;
        }

        static public TelemetryEntry MAKE(string str) {
            TelemetryEntry entry = new TelemetryEntry();

            string[] bits = str.Split('\t');
            if (bits.Length == 0 || bits[0] == "") return null;
            if (HEADERS == null || HEADERS.Length < bits.Length) {
                Debug.LogError("HEADERS has not been set properly!\n"
                    + ((HEADERS == null) ? "HEADERS is null" : "[ " + string.Join("\t", HEADERS) + " ]"));
                return null;
            }
            for (int i = 0; i < bits.Length; i++) {
                bits[i] = bits[i].Trim();
                switch (HEADERS[i]) {
                    case "recNum":
                        int.TryParse(bits[i], out entry.recNum);
                        break;

                    case "project":
                        entry.project = bits[i];
                        break;

                    case "semester":
                        entry.semester = bits[i];
                        break;

                    case "version":
                        entry.version = bits[i];
                        break;

                    case "sceneName":
                        entry.sceneName = bits[i];
                        break;

                    case "playerName":
                        entry.playerName = bits[i];
                        break;

                    case "dateTime":
                        entry.dateTime = bits[i];
                        break;

                    case "data":
                        entry.data = bits[i];
                        break;
                }
            }

            entry.ParseDataToTelems();

            return entry;
        }

        internal void ParseDataToTelems() {
            string[] lines;
            Telemetry_Cloud.Telem telem;
            data = data.Replace("\\n", "\n").Replace("\\t", "\t");
            lines = data.Split('\n');
            // The first line is the legend, so it is skipped. - JGB
            int num = lines.Length - 1;
            List<Telemetry_Cloud.Telem> telemList = new List<Telemetry_Cloud.Telem>();
            for (int i = 1; i < lines.Length; i++) {
                if (lines[i].Length == 0) continue;
                telem = Telemetry_Cloud.Telem.FromString(lines[i].Trim());
                if (telem.tag == "ERROR") continue;
                telemList.Add(telem);
                if (telem.time > maxTime) maxTime = telem.time;
            }
            telems = telemList.ToArray();
        }

        internal float GetMaxTime() {
            if (telems == null || telems.Length == 0) ParseDataToTelems();
            // If it's STILL null or Length 0, return 0;
            if (telems == null || telems.Length == 0) return 0;
            return (telems[telems.Length - 1].time);
        }

        internal int FindTelemAtTime(float time) {
            if (telems == null || telems.Length == 0) return 0;

            // Always return at least 2 if possible
            if (time == 0 && telems.Length > 1) return 2;

            int nL = 0;
            int nR = telems.Length - 1;
            int nM;

            while (nL <= nR) {
                nM = (nL + nR) / 2;
                switch (CompareTelemToTime(nM, time)) {
                    case -1: nL = nM + 1; break;
                    case 0:
                        // Always return at least 2 if possible
                        if (nM < 2 && telems.Length > 1) return 2;
                        return nM;
                    case 1: nR = nM - 1; break;
                }
            }
            return -1;
        }

        int CompareTelemToTime(int ndx, float time) {
            if (telems == null || telems.Length == 0) return 0;
            if (telems[ndx].time > time) return 1;
            if (ndx == telems.Length - 1) return 0; // If this is the last element, and its time is < time, then this is the one we're looking for.
            if (telems[ndx + 1].time < time) return -1;
            //if (telems[ndx].time <= time && (ndx == telems.Length - 1 || telems[ndx + 1].time >= time)) return 0;
            return 0;
        }

    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(TelemetryEntry))]
    public class TelemetryEntry_Drawer : PropertyDrawer {
        SerializedProperty m_show, m_recNum, m_playerName, m_dateTime;

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            // Init the SerializedProperty fields
            //if ( m_show == null ) m_show = property.FindPropertyRelative( "show" );
            //if ( m_recNum == null ) m_recNum = property.FindPropertyRelative( "recNum" );
            //if ( m_playerName == null ) m_playerName = property.FindPropertyRelative( "playerName" );
            //if ( m_dateTime == null ) m_dateTime = property.FindPropertyRelative( "dateTime" );
            m_show = property.FindPropertyRelative("show");
            m_recNum = property.FindPropertyRelative("recNum");
            m_playerName = property.FindPropertyRelative("playerName");
            m_dateTime = property.FindPropertyRelative("dateTime");

            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), GUIContent.none);// label );

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

#if EXPLICIT_RECTS
            // Calculate rects
            float left = 0, spacing = 4;
            Rect showR = new Rect(position.x + left, position.y, position.height, position.height);
            left += showR.width + spacing;
            //Rect recNumR = new Rect( position.x + left, position.y, 40, position.height );
            //left += recNumR.width + spacing;
            Rect labelR = new Rect(position.x + left, position.y, position.width - left, position.height);
            //Rect playerName = new Rect( position.x+70, position.y, position.width-70, position.height );;

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.PropertyField(showR, m_show, GUIContent.none);
            EditorGUI.LabelField(labelR, $"{m_recNum.intValue}\t{m_playerName.stringValue}\t{m_dateTime.stringValue}");
            //EditorGUI.LabelField(recNumR, m_recNum.intValue.ToString());
            //EditorGUI.LabelField(labelR, m_playerName.stringValue
            //    + "\t" + m_dateTime.stringValue);
            //EditorGUI.PropertyField( recNumR, property.FindPropertyRelative( "recNum" ), GUIContent.none );
            //EditorGUI.PropertyField( playerName, property.FindPropertyRelative( "playerName" ), GUIContent.none );
#else
            EditorGUILayout.PropertyField(m_show, GUIContent.none);
            EditorGUILayout.LabelField(m_recNum.intValue.ToString());
            EditorGUILayout.LabelField(m_playerName.stringValue + "\t" + m_dateTime.stringValue);

#endif

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
#endif
}
