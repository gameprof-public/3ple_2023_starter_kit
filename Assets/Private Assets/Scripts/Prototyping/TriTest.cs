using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

static public class ProBuilderExtensionMethods {
    public static int[] VertToSharedVertArray( this ProBuilderMesh pbm ) {
        int[] vToSv = new int[pbm.vertexCount];
        SharedVertex sv;
        for ( int svNum = 0; svNum < pbm.sharedVertices.Count; svNum++ ) {
            sv = pbm.sharedVertices[svNum];
            for ( int i = 0; i < sv.Count; i++ ) {
                vToSv[sv[i]] = svNum;
            }
        }
        return vToSv;
    }

    public static int[] DistinctSharedIndexes( this Face face, ProBuilderMesh pBMesh ) {
        int[] vToSv = pBMesh.VertToSharedVertArray();
        return face.DistinctSharedIndexes( vToSv );
    }

    public static int[] DistinctSharedIndexes( this Face face, int[] vertToSharedVert ) {
        System.Collections.ObjectModel.ReadOnlyCollection<int> dIs = face.distinctIndexes;
        int[] dSIs = new int[dIs.Count];
        for ( int i = 0; i < dIs.Count; i++ ) {
            dSIs[i] = vertToSharedVert[dIs[i]];
        }
        return dSIs;
    }
}

/// <summary>
/// Jeremy's attempt to make the ProBuilder triangle test that Aaron wrote work
/// </summary>
public class TriTest : MonoBehaviour {
    const bool DEBUG = true;
    const bool DEBUG_DRAW = true;

    public float testDistance = 10f;
    public LayerMask layersToCheck;
    public eTestMode testMode = eTestMode.position;
    public bool drawGizmos = true;
    public enum eTestMode { position, hit, triangleIndex };

    private void OnDrawGizmos() {
        if ( !drawGizmos ) return;

        if (DEBUG_DRAW) Debug.DrawLine( transform.position, transform.position + transform.forward * testDistance, Color.red, 0f );
        FindHitFace( testDistance );
    }


    // Update is called once per frame
    Face FindHitFace(float dist) {
        // Check the raycast and return if nothing is hit.
        RaycastHit hit;
        if ( !Physics.Raycast( transform.position, transform.forward, out hit, dist, layersToCheck ) ) {
            return null;
        }

        // We know that something WAS hit, but does it have a ProBuilder component?
        ProBuilderMesh pbm = hit.collider.GetComponent<ProBuilderMesh>();
        if ( pbm == null ) return null;

        // This is not actually necessary for eTestMode.triangleIndex
        // Get the position of the hitpoint in local mesh coordinates
        // This is MUCH faster than the GetPositionOfTriangle code that transforms every local vertex to world space
        Vector3 localTestPoint;
        if ( testMode == eTestMode.position ) {
            localTestPoint = pbm.transform.InverseTransformPoint( transform.position );
        } else {
            localTestPoint = pbm.transform.InverseTransformPoint( hit.point );
        }

        Face foundFace = null;

        switch ( testMode ) {
        case eTestMode.triangleIndex:
            // Do ProBuilder and MeshRenderer share the same mesh?
            if ( pbm.meshSyncState != MeshSyncState.InSync) {
                if ( DEBUG ) Debug.LogWarning( "pbm.meshSyncState = " + pbm.meshSyncState );
                return null;
            }
            // Is the Collider a MeshCollider?
            if (!(hit.collider is MeshCollider)) {
                if (DEBUG) Debug.LogWarning( "Collider type is " + hit.collider.GetType());
                return null;
            }
            // Does the MeshCollider share a mesh with the MeshRenderer?
            MeshFilter meshFilter = hit.collider.GetComponent<MeshFilter>();
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (meshFilter.sharedMesh != meshCollider.sharedMesh) {
                if (DEBUG) Debug.LogWarning( "MeshCollider mesh is " + meshCollider.sharedMesh + " but MeshRenderer mesh is " + meshFilter.sharedMesh );
                return null;
            }

            // So, now we know that all three share the same mesh!!
            // This means that we can use the triangleIndex from the RaycastHit on all of them.
            int triangleIndex3 = hit.triangleIndex * 3; // Index of the 0th vert of this triangle in the triangles array.
            // Get the indices of the three verts that make up this triangle (same indices in the normals array)
            int[] vertNums = new int[3];
            Vector3[] localPositions = new Vector3[3];
            Vector3[] positions = new Vector3[3];
            for (int i=0; i<3; i++ ) {
                vertNums[i] = meshCollider.sharedMesh.triangles[triangleIndex3 + i];
                localPositions[i] = meshCollider.sharedMesh.vertices[vertNums[i]];
                positions[i] = meshCollider.transform.TransformPoint(meshCollider.sharedMesh.vertices[vertNums[i]]);
            }

            // Draw the found triangle
            for (int i=0; i<3; i++ ) {
                if (DEBUG_DRAW) Debug.DrawLine( positions[i], positions[( i + 1 ) % 3], Color.yellow );
            }

            // Get the positions of the pbm.sharedVertices
            List<Vector3> pbVertPositions = new List<Vector3>();
            foreach (SharedVertex sv in pbm.sharedVertices) {
                pbVertPositions.Add( pbm.positions[ sv[0] ] );
            }
            // Find the sharedVertices at the localPositions
            int[] sharedVertNums = new int[3];
            float distThreshold = 0.01f;
            for (int i=0; i<pbVertPositions.Count; i++) {
                for (int j=0; j<3; j++) {
                    if ((pbVertPositions[i] - localPositions[j]).sqrMagnitude <= distThreshold) {
                        sharedVertNums[j] = i;
                    }
                }
            }
            //if (DEBUG) Debug.Log( "VertNums: [ "
            //    + vertNums[0] + ", " + vertNums[1] + ", " +vertNums[2]
            //    + " ]   sharedVertNums: [ "
            //    + sharedVertNums[0] + ", " + sharedVertNums[1] + ", " + sharedVertNums[2]
            //    + " ]" );

            // Get the array to look up sharedVertNums from Face vert nums
            int[] vToSv = pbm.VertToSharedVertArray();

            // Find the Face that contains this triangle
            int[] faceDSIs;
            foreach ( Face face in pbm.faces ) {
                // Check to see if all three sharedVertNums are in this Face
                faceDSIs = face.DistinctSharedIndexes( vToSv );
                // As soon as one fails, continue to the next Face
                if ( System.Array.IndexOf<int>( faceDSIs, sharedVertNums[0] ) == -1 ) continue;
                if ( System.Array.IndexOf<int>( faceDSIs, sharedVertNums[1] ) == -1 ) continue;
                if ( System.Array.IndexOf<int>( faceDSIs, sharedVertNums[2] ) == -1 ) continue;
                // If we got here, all three are in this Face. Assign foundFace and break the loop
                foundFace = face;
                break;
            }

            break;

        case eTestMode.hit:
        case eTestMode.position:

            // Loop through the faces.
            IList<Face> faces = pbm.faces;
            Vector3 faceVertsDelta;
            float minFaceVertsDist = 1000f;
            float faceVertsDist;
            foreach ( Face face in faces ) {
                faceVertsDelta = Vector3.zero;
                foreach ( int ndx in face.indexes ) {
                    faceVertsDelta += pbm.positions[ndx] - localTestPoint;
                }
                faceVertsDist = faceVertsDelta.magnitude; // This square root is a bit slow but is done once per vert
                if ( faceVertsDist < minFaceVertsDist ) {
                    minFaceVertsDist = faceVertsDist;
                    foundFace = face;
                }
            }

            break;
        }


        if ( foundFace == null ) {
            if (DEBUG) Debug.LogWarning( "foundFace was null. :(" );
            return null;
        }

        //Debug.Log( "Closest face textureGroup is " + closestFace.textureGroup );

        // pbm.normals was null, so I'm trying this.
        Vector3[] normals = pbm.GetNormals();

        Matrix4x4 transMat = pbm.transform.localToWorldMatrix;

        // Draw normals of the closest face
        foreach ( int ndx in foundFace.indexes ) {
            if (DEBUG_DRAW) Debug.DrawRay( pbm.transform.TransformPoint( pbm.positions[ndx] ), normals[ndx], Color.red );
        }

        foreach ( Edge edge in foundFace.edges ) {
            if (DEBUG_DRAW) Debug.DrawLine( pbm.transform.TransformPoint( pbm.positions[edge.a] ),
                pbm.transform.TransformPoint( pbm.positions[edge.b] ), Color.green );
        }

        return foundFace;
    }
}



////First, check if the object is climbable, and climb if so
//        if (pbm != null)
//        {

//            //Fire a raycast at the collission point to find the triangle
//            //Vector3 direction = possibleBonk.ClosestPointOnBounds(transform.position) - transform.position;
//            Vector3 direction = transform.forward;
//Ray r = new Ray( transform.position, direction );
//RaycastHit hit;
//int mask = -1;

////debug a line too
//Debug.DrawLine(transform.position, transform.position + (transform.forward* 1.5f), Color.red, 3f);

//            if (Physics.Raycast(r, out hit, 1f, mask))
//            {
//                    //Triangle looping version
//                    Mesh mesh = collderToCheck.GetComponent<MeshFilter>().mesh;

//    float distanceOfClosestTriangle = 333;
//    int indexOfClosestTriangle = -1;
//    Vector3 savedPositionOfClosestTriangle = Vector3.zero;

//                    for (int i = 0; i<(mesh.triangles.Length / 3); i++)
//                    {
//                        Vector3 tPos = GetPositionOfTriangle( mesh, i, collderToCheck.transform );
//    float dist = Vector3.Distance( transform.position, tPos );

//    //Check if the normal if the including face points towards the player.

//    //Get the normal of the 3 verticies of the triangle and average them
//    Vector3[] verticeNormals = new Vector3[3];
//    verticeNormals[0] = mesh.normals[mesh.triangles[i]];
//                        verticeNormals[1] = mesh.normals[mesh.triangles[i + 1]];
//                        verticeNormals[2] = mesh.normals[mesh.triangles[i + 2]];
//                        Vector3 faceNormal = ( verticeNormals[0] + verticeNormals[1] + verticeNormals[2] ) / 3;

//    //Get the face normal and interpret it in world space
//    //Vector3 faceNormal = mesh.normals[mesh.triangles[i]];
//    faceNormal = hit.transform.TransformDirection(faceNormal);

//                        Vector3 directionTowardsPlayer = ( transform.position - tPos ).normalized;
//    bool facesPlayer = CheckIfNormalFacesSomething( faceNormal, directionTowardsPlayer );


//                        if (!facesPlayer) continue;

//                        Debug.DrawLine(tPos, tPos + faceNormal, Color.green, 5f);

//                        if (dist<distanceOfClosestTriangle)
//                        {
//                            distanceOfClosestTriangle = dist;
//                            indexOfClosestTriangle = i;
//                            savedPositionOfClosestTriangle = tPos;
//                        }
//                    }

//                    if ( indexOfClosestTriangle == -1 ) {
//    Debug.Log( "aaron this failed somehow." );
//} else {
//    Debug.Log( "aaron number of triangles evaluated: " + ( mesh.triangles.Length / 3 ).ToString() );
//    //TriangleHitDebug(mesh, indexOfClosestTriangle, possibleBonk.transform);
//    Debug.DrawLine( savedPositionOfClosestTriangle, transform.position, Color.cyan, 10f );
//}



//                    //Mesh collider version, which didn't work
//                    //MeshCollider mC = possibleBonk as MeshCollider;
//                    ////Quit here if this wasn't a mesh collider, otherwise continue
//                    //if (mC != null)
//                    //{

//                    //    Mesh mesh = mC.sharedMesh;
//                    //    MeshRenderer mRend = possibleBonk.GetComponent<MeshRenderer>();

//                    //    Debug.Log("aaron submesh count: " + mesh.subMeshCount.ToString());

//                    //    //Set up a triangle from the info from the hit
//                    //    //int[] triangle = new int[3]
//                    //    //{
//                    //    //    mesh.triangles[hit.triangleIndex *3 + 0],
//                    //    //    mesh.triangles[hit.triangleIndex * 3 + 1],
//                    //    //    mesh.triangles[hit.triangleIndex *3 + 2]
//                    //    //};

//                    //    TriangleHitDebug(mesh, hit.triangleIndex, hit.transform);

//                    //    Material matDetected = null;
//                    //    foreach (Face f in pbm.faces)
//                    //    {
//                    //        if (f.indexes.Contains(hit.triangleIndex))
//                    //        {
//                    //            Debug.Log("aaron submesh index: " + f.submeshIndex.ToString());
//                    //            matDetected = mRend.materials[f.submeshIndex];
//                    //            break;
//                    //        }
//                    //    }
//                    //    if (matDetected != null) //we found a face, detected it with the raycast, and found a material
//                    //    {
//                    //        Debug.Log("aaron mat found: " + matDetected.name);
//                    //    }
//                    //}
//                }
//                else {
//    Debug.Log( "aaron raycast didn't hit anything" );
//}
//            }
//        }