using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TurtleEnemy : Enemy
{
    [Header("Turtle-Specific Settings")]
    [SerializeField] private float buttonUpDuration = 5;
    private float timeButtonUpEnd = -1;


    private TurtleEnemyState _myState;

    [Header("Turtle Testing Variables")]
    public bool startTest;
    public Material nonAggroMat;
    public Material aggroMat;
    public MeshRenderer headMRend;

    [Header( "HitBox References" )]
    public HitBox hbJumpedOnStanding;
    public HitBox hbJumpedOnButtonUp;
    public HitBox hbHurtbox;

    public enum TurtleEnemyState { IDLE, WALKING, CHASING, SLOWING, BUTTONUP }

    public TurtleEnemyState myState {
        get { return _myState; }
        private set {
            // Switch Hitboxes
            if (value == TurtleEnemyState.BUTTONUP) {
                hbJumpedOnStanding.gameObject.SetActive( false );
                hbHurtbox.gameObject.SetActive( false );
                hbJumpedOnButtonUp.gameObject.SetActive( true );
            } else {
                hbJumpedOnStanding.gameObject.SetActive( true );
                hbHurtbox.gameObject.SetActive( true );
                hbJumpedOnButtonUp.gameObject.SetActive( false );
            }
            _myState = value;
        }
    }

    //Animation
    private Animator anim;

    public override void Start()
    {
        base.Start();
        myState = TurtleEnemyState.IDLE;
        anim = GetComponentInChildren<Animator>();
    }

    public override void Update()
    {
        base.Update(); //Made a check in the Enemy class regarding wether to switch to custom points or not

        //This was the old animation prototype stuff, no longer needed:
        //Temporary color change for prototyping player feedback
        //if (myState == TurtleEnemyState.CHASING) headMRend.material = aggroMat;
        //else headMRend.material = nonAggroMat;

        switch(myState)
        {
            case TurtleEnemyState.IDLE:
                //Set idle end time if hasn't yet been done
                if (timeIdleEnd == -1)
                {
                    float timeAdd = Random.Range(minIdleTime, maxIdleTime);
                    timeIdleEnd = Time.time + timeAdd;
                }

                //See if player is detected
                if (PlayerDetected())
                {
                    myState = TurtleEnemyState.CHASING;
                }

                //Check if the time to be idle has ended
                if (Time.time >= timeIdleEnd)
                {
                    //Reset idle time
                    timeIdleEnd = -1;

                    if(this.switchToCustomPoints == false) //If we are not using custom points
                    { 
                        //Choose a destincation and set it
                        Vector3 destination = RandomNavmeshPointWithinSphere(patrolRadius);
                        SetMoveDestination(destination);
                    }

                    else 
                    {
                        if(onPatrolPath == false) 
                        {
                            Vector3 destination = FindNearestPointOnPath(this.patrolPoints, this.transform.position);
                        }

                    }
                    myState = TurtleEnemyState.WALKING;
                    
                }
                break;

            case TurtleEnemyState.WALKING:
                if (Vector3.Distance(transform.position, agent.destination) < .05f)
                {
                    myState = TurtleEnemyState.IDLE;
                }
                break;

            case TurtleEnemyState.CHASING:
                //Check if we need to stop chasing (too far, or too above//below) This may need to change if the enemy can walk or up ramps
                Vector3 playerPos = PlayerController.GetPlayerPosition();
                if (Vector3.Distance (transform.position, playerPos) >= escapeRange || Mathf.Abs(transform.position.y - playerPos.y) >= 2.5f)
                {
                    IEnumerator coro = SlowDownAndStop(.75f);
                    StartCoroutine(coro);

                    myState = TurtleEnemyState.SLOWING;
                }

                //Keep the player the destination if not
                Vector3 newDest = GetPositionOnNavmesh(playerPos);
                SetMoveDestination(newDest);
                break;

            case TurtleEnemyState.BUTTONUP:
                float timeRemaining = (timeButtonUpEnd - (Time.time));

                if (Time.time >= timeButtonUpEnd)
                {
                    UnButton();
                }
                break;
        }

        HandleAnimation();
        //Debug.Log(myState);
    }




    public override bool GetJumpedOn()
    {
        bool jumpedOn =base.GetJumpedOn();

        if (jumpedOn)
        {
            //This should reset the button up if the player jumps on them
            bool alreadyButtoned = false;
            if (myState == TurtleEnemyState.BUTTONUP) alreadyButtoned = true;

            if (!alreadyButtoned)
            {
                myState = TurtleEnemyState.BUTTONUP;
                ClearDestination();

                //Temporary animation
                //Vector3 newScale = transform.localScale;
                //newScale.y *= .5f;
                //transform.localScale = newScale;
            }

            timeButtonUpEnd = Time.time + buttonUpDuration;

        }

        return jumpedOn;

        //Should disable the enemies basic hurtbox while in this mode, and possibly allow the player to push the shell around a bit
    }

    private void UnButton()
    {
        //temporary animation
        //Vector3 newScale = transform.localScale;
        //newScale.y *= 2;
        //transform.localScale = newScale;

        timeButtonUpEnd = -1;

        myState = TurtleEnemyState.IDLE;
    }

    private IEnumerator SlowDownAndStop(float slowSpeed)
    {
        float t = 0;
        float speedSave = agent.speed;

        while (t < 1)
        {
            if (myState == TurtleEnemyState.BUTTONUP) yield break; //Just quit this if I get buttoned up

            agent.speed = Mathf.Lerp(speedSave, 0, t);
            t += Time.deltaTime * slowSpeed;
            yield return new WaitForEndOfFrame();
        }

        ClearDestination();
        agent.speed = speedSave;
        myState = TurtleEnemyState.IDLE;
    }

    public override void HurtPlayer(Collider other)
    {
        if (myState == TurtleEnemyState.BUTTONUP) return; //Don't hurt the player while we're buttoned up.
        base.HurtPlayer(other);
    }

    private void HandleAnimation()
    {
        switch (myState)
        {
            case TurtleEnemyState.IDLE:
                DisableAllAnimationBools();
                break;
            case TurtleEnemyState.WALKING:
            case TurtleEnemyState.CHASING:
            case TurtleEnemyState.SLOWING:
                anim.SetBool("Moving", true);
                break;
            case TurtleEnemyState.BUTTONUP:
                anim.SetBool("Buttonup", true);
                break;
        }
    }

    private void DisableAllAnimationBools()
    {
        anim.SetBool("Moving", false);
        anim.SetBool("Buttonup", false);
    }
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(TurtleEnemy))]
public class TurtleEnemyEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif