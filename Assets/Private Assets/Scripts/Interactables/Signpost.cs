using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;

public class Signpost : Interactable
{
    [HideInInspector]  [SerializeField] private TMP_Text signText;
    [TextArea(1,3)] public string textToShowOnSign;
    // [HideInInspector] [SerializeField] private GameObject preinteractableUIObject;
    // [HideInInspector] [SerializeField] private GameObject interactableUIObject;
    [HideInInspector] [SerializeField] private GameObject signTextObject;

    

    public void Start()
    {
        CloseSign();
        signText.text = textToShowOnSign;
    }

    void OnValidate() {
        signText.text = textToShowOnSign;
    }



    public override void Interact()
    {
        // interactableUIObject.SetActive(false);
        signTextObject.SetActive(true);
    }

    public void AdvanceSign()
    {

    }

    public void CloseSign()
    {
        signTextObject.SetActive(false);
        // preinteractableUIObject.SetActive(false);
        // interactableUIObject.SetActive(false);

        Quaternion zero = new Quaternion(0, 0, 0, 0);

        signTextObject.transform.rotation = zero;
        // preinteractableUIObject.transform.rotation = zero;
        // interactableUIObject.transform.rotation = zero;
    }

    public override void RegisterInteractableIfAppropriate(Collider other)
    {
        base.RegisterInteractableIfAppropriate(other);
        //Changing this to just immediately display the text of the sign
        //if (!signTextObject.activeInHierarchy)
        //    preinteractableUIObject.SetActive(true);
        Interact(); //Simply open the sign when the player moves near
    }

    public override void UnregisterInteractableIfAppropriate(Collider other)
    {
        base.UnregisterInteractableIfAppropriate(other);
        CloseSign();
    }

    public override void AllowInteraction() //Called by the PlayerController when they have stopped and can interact with this object.
    {
        if (!signTextObject.activeInHierarchy)
        {
            // preinteractableUIObject.SetActive(false);
            // interactableUIObject.SetActive(true);
        }

    }
}

#if UNITY_EDITOR
//Uncomment this block to disable this script in the editor
[UnityEditor.CustomEditor(typeof(Signpost))]
public class Signpost_Editor : UnityEditor.Editor {
    private SerializedProperty sp_textToShowOnSign;
    
    void OnEnable() {
        sp_textToShowOnSign = serializedObject.FindProperty( "textToShowOnSign" );
    }

    public override void OnInspectorGUI()
    {
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
            base.OnInspectorGUI();
        }
        
        
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update ();
        
        // Signpost signpost = target as Signpost;

        EditorGUILayout.PropertyField( sp_textToShowOnSign );

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties ();
        

    }
}
#endif