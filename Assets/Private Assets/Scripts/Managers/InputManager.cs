using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public enum InputType { PLAYERMOVE, PLAYERLOOK, PLAYERJUMP, PLAYERCROUCH, COUNT}

public enum LastInputDevice { PAD, KEYBOARD}

public class InputManager : MonoBehaviour
{
    [Header("Character Controller Input")]
    [SerializeField] private InputActionReference playerMoveInput;
    [SerializeField] private InputActionReference playerLookInput;
    [SerializeField] private InputActionReference playerJumpInput;
    [SerializeField] private InputActionReference playerCrouchInput;

    private static InputAction playerMoveAction;
    private static InputAction playerLookAction;
    private static InputAction playerJumpAction;
    private static InputAction playerCrouchAction;

    public static InputManager s;

    private LastInputDevice lastDeviceUsed;

    private void Update()
    {
        //Debug.Log("Input scheme: " + lastDeviceUsed.ToString());
    }

    public void ReportLastInputDevice(string scheme)
    {
        if (scheme == "Keyboard") lastDeviceUsed = LastInputDevice.KEYBOARD;
        else if (scheme == "Generic Gamepad") lastDeviceUsed = LastInputDevice.PAD;
    }

    public LastInputDevice GetLastInputDevice()
    {
        return lastDeviceUsed;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Assign singleton
        if (s == null) s = this;

        if (s != this) Debug.LogWarning("More than one input manager is present in the scene, please remove one.");
        else s = this;

        //Assign actions
        playerMoveAction = playerMoveInput.ToInputAction();
        playerLookAction = playerLookInput.ToInputAction();
        playerJumpAction = playerJumpInput.ToInputAction();
        playerCrouchAction = playerCrouchInput.ToInputAction();

        //Enable inputs, this should probably be expanded later for menu input, and so on
        EnablePlayerInput();
    }

    public void DisablePlayerInput()
    {
        playerMoveAction.Disable();
        playerLookAction.Disable();
        playerJumpAction.Disable();
        playerCrouchAction.Disable();
    }

    public void EnablePlayerInput()
    {
        playerMoveAction.Enable();
        playerLookAction.Enable();
        playerJumpAction.Enable();
        playerCrouchAction.Enable();
    }


    public static Vector2 GetInputAxis(InputType type)
    {
        Vector2 value = Vector2.zero;

        switch (type)
        {
            //Proper values
            case InputType.PLAYERLOOK:
                value = playerLookAction.ReadValue<Vector2>();
                break;
            case InputType.PLAYERMOVE:
                value = playerMoveAction.ReadValue<Vector2>();
                break;

            //Improper values
            case InputType.PLAYERJUMP:
            default:
                Debug.LogWarning("Attempted to get axis input for an input action that cannot return as a vector 2.");
                break;
        }

        return value;
    }

    public static bool GetInputButton(InputType type)
    {
        bool value = false;

        switch (type)
        {
            //Proper values
            case InputType.PLAYERJUMP:
                if (playerJumpAction.ReadValue<float>() > 0) value = true;
                break;
            case InputType.PLAYERCROUCH:
                if (playerCrouchAction.ReadValue<float>() > 0) value = true;
                break;

            //Improper type
            default:
                Debug.LogWarning("Attempted to get a bool from a non-button input.");
                break;
        }

        return value;
    }

    public static float GetInputFloat (InputType type)
    {
        float value = 0;

        switch (type)
        {
            //Proper values
            case InputType.PLAYERJUMP:
                value = playerJumpAction.ReadValue<float>();
                break;
            case InputType.PLAYERCROUCH:
                value = playerCrouchAction.ReadValue<float>();
                break;

            //Improper values
            default:
                Debug.LogWarning("Attempted to get input as a float from an input that cannot return a float.");
                break;
        }

        return value;
    }
}
