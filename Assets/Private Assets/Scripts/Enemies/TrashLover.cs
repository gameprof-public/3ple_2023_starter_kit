using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif


/// <summary>
/// This class is a companion class for enemies that want to steal trash for themselves.
/// It keeps a static list of claimed pieces of trash to make sure that multiple enemies don't get all weird going for the same piece of trash.
/// </summary>
[RequireComponent(typeof(Enemy))]
public class TrashLover : MonoBehaviour
{
    [Tooltip("An emptry transform that tells trash where to go when it's held by the enemy.")]
    [SerializeField] private Transform trashHeldEmptyTransform;

    //Variables for animating the trash when it's picked up or dropped
    private static float heldTrashScaleMult = .3f; //A float that is applied to monster-held trash meshes
    private static float animationMoveSpeed = 3f;
    private static float animationScaleSpeed = 3f;

    

    //Trash desire variables
    private Trash desiredTrash = null; // Used for tracking the world position of trash that is spotted but not yet picked up
    bool trashDesired = false; // this is true while we're heading to a trash, and used in update to check how far we are. should be faster than doing a null check in update
    private float trashPickupRange = .75f;

    //Actually holding trash variables
    private Trash actuallyHeldTrash = null; //Holds the reference for an actually possessed trash

    //events
    private UnityEvent trashDetectedEvent;
    private UnityEvent trashSwipedEvent; //this event happens when a desired trash is grabed by another agent.
    private UnityEvent trashPickedUpEvent;

    private void Start()
    {

        trashDetectedEvent = new UnityEvent();
        trashSwipedEvent = new UnityEvent();
        trashPickedUpEvent = new UnityEvent();
    }

    private void Update()
    {
        if (trashDesired)
        {
            if (Vector3.Distance(transform.position, desiredTrash.transform.position) <= trashPickupRange)
            {
                ActuallyPickUpTrash(desiredTrash);
            }
        }
    }

    #region static functions, for stuff that deals with the claimed trash dict
    /// <summary>
    /// Call this function from an Enemy class when that enemy starts going for a piece of trash.
    /// </summary>
    /// <param name="trash"></param>
    public static void ClaimTrash (Trash trash, TrashLover tL)
    {
        GameManager.dictClaimedTrash.Add(trash, tL);
    }

    /// <summary>
    /// Call this function when an Enemy get distracted while going for a claimed trash,
    /// or the player picks up a claimed trash.
    /// </summary>
    /// <param name="trash"></param>
    public static void UnclaimTrash(Trash trash)
    {
        if (GameManager.dictClaimedTrash.ContainsKey(trash))
        {
            GameManager.dictClaimedTrash.Remove(trash);
        }
    }
    #endregion

    /// <summary>
    /// Call this function from a Hitbox when a trash is detected
    /// </summary>
    /// <param name="other"></param>
    public void CheckIfTrash(Collider other)
    {
        //make sure it's a trash before we do anything
        Trash trashScript = other.GetComponent<Trash>();
        if (trashScript == null) return;

        //Return if we already are interested in a trash, or already are holding one. Enemies can only hold one trash
        if (desiredTrash != null || actuallyHeldTrash != null)
        {
            return;
        }

        //Check if the trash is already claimed
        if (GameManager.dictClaimedTrash.ContainsKey(trashScript))
        {
            //Someone is already going for it or holding it
            return;
        }


        Debug.Log("A trashlover detected a trash.");

        //OK time to actually claim it
        TrashLover.ClaimTrash(trashScript, this);
        trashDetectedEvent.Invoke(); //Use this to send the message to the Enemy class, which should be subscribed to this
        desiredTrash = trashScript;
        trashDesired = true;
    }

    public void CheckIfTrash( Collider other, HitBox hitBox ) {
        CheckIfTrash( other );
    }


    public Vector3 GetPositionOfDesiredTrash()
    {
        return desiredTrash.transform.position;
    }

    private void ActuallyPickUpTrash(Trash trash)
    {
        actuallyHeldTrash = trash;

        ForgetTrash();

        trashPickedUpEvent.Invoke(); //This goes to enemy listeners

        trash.DisableCollider();

        //Animate the trash to the pickup point
        Transform trashTransform = trash.transform;

        AnimationEvent animateEndEvent = new AnimationEvent();

        animateEndEvent.AddListener(SetTrashParentToHeldEmpty);

        //Animate Position
        StartCoroutine(AnimationUtility.AnimatePositionToTransform(trashTransform, trashHeldEmptyTransform, TrashLover.animationMoveSpeed, eventToCallAfterFunctionEnd: animateEndEvent));

        //Animate scale
        Vector3 targetScale = trashTransform.localScale * TrashLover.heldTrashScaleMult;
        StartCoroutine(AnimationUtility.AnimateScale(trashTransform, targetScale, TrashLover.animationScaleSpeed));
    }



    private void SetTrashParentToHeldEmpty(Transform trashTransform)
    {
        trashTransform.SetParent(trashHeldEmptyTransform);
    }


    public void DropTrashIfImHoldingOne()
    {
        if (actuallyHeldTrash != null)
        {
            DropTrash(actuallyHeldTrash);
        }
    }
    private void DropTrash(Trash trash)
    {
        AnimationEvent endEvent = new AnimationEvent();
        endEvent.AddListener(trash.ReEnableCollider);

        //Use this as the endpoint so it's guaranteed to not fall off the map
        Vector3 endPoint = Enemy.GetPositionOnNavmesh(transform.position, maxRadius: 5f);
        Vector3 bPoint = AnimationUtility.GetBezierBPointStartingFromHighPosition(trash.transform.position, endPoint);

        IEnumerator coro = AnimationUtility.AnimateAlongBezierCurve(trash.transform, endPoint, bPoint, 3, eventToCallAfterFunctionEnd: endEvent);
        trash.StartCoroutine(coro);
    }

    private void ForgetTrash()
    {
        trashDesired = false;
        desiredTrash = null;
    }

    #region Event registration
    public void RegisterWithTrashDetectedEvent(UnityAction subscriber)
    {
       //trashDetectedEvent.AddListener(subscriber);
    }

    public void DeregisterWithTrashDetectedEvent(UnityAction unsubscriber)
    {
        //trashDetectedEvent.RemoveListener(unsubscriber);
    }

    public void RegisterWithTrashSwipedEvent(UnityAction subscriber)
    {
        //trashSwipedEvent.AddListener(subscriber);
    }

    public void DeregisterWithTrashSwipedEvent(UnityAction unsubscriber)
    {
        //trashSwipedEvent.RemoveListener(unsubscriber);
    }

    public void RegisterWithTrashPickedUpEvent(UnityAction subscriber)
    {
        //trashPickedUpEvent.AddListener(subscriber);
    }

    public void DeregisterWithPickedUpEvent(UnityAction unsubscriber)
    {
        //trashPickedUpEvent.RemoveListener(unsubscriber);
    }

    public void InvokeTrashSwipedEvent()
    {
        ForgetTrash();
        trashSwipedEvent.Invoke();
    }
    #endregion
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(TrashLover))]
public class TrashLoverEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif