using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputErrorWorkaround : MonoBehaviour {
    private PlayerInput pInput;

    private void Start() {
        pInput = GetComponent<PlayerInput>();
    }

    private void OnDisable() {
        if (pInput != null) pInput.actions = null;
    }
}