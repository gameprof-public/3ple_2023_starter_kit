using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TrashInventory : MonoBehaviour
{
    public static TrashInventory s;

    [Header("Inventory Settings")]
    [SerializeField] private Transform[] InventoryUITransforms;
    [SerializeField] private Camera inventoryCamera;
    [Tooltip("VFX applied when a Trash is moved to the dumbpster")]
    [SerializeField] private GameObject inventoryToDumpsterVFXPrefab;
    [Tooltip("VFX will spawn this many meters in front of the 3rd person follow camera when items move to the dumpster")]
    [SerializeField] private float vfxLocalZOffset;
    [SerializeField] private float vfxAnimationDuration = 2;

    private int trashInDumpster = 0;
    private int totalTrashInLevel = 0;

    private List<Trash> trashInInventory;

    private int inventoryLayer = 999;

    private void Awake()
    {
        InitializeTrashInventory();
    }

    public void InitializeTrashInventory()
    {
        if (s != this)
        {
            if (s != null)
            {
                Debug.LogWarning("More than one inventory in scene, please remove the extra(s)");
            }
            s = this;
        }
        else { return; } //Don't do the stuff after this if we're already initialized.

        trashInInventory = new List<Trash>();

        if (inventoryLayer == 999)
        {
            inventoryLayer = LayerMask.NameToLayer("Inventory");
        }
    }

    public bool AddTrashToInventory(Trash t)
    {
        if (trashInInventory.Count == InventoryUITransforms.Length) {
            // There are already too many pieces of trash, so refuse to add the trash
            return false;
        }

        trashInInventory.Add(t);

        int indexOfLocation = trashInInventory.Count - 1;

        t.transform.position = InventoryUITransforms[indexOfLocation].position;

        SetGraphicsToLayer(inventoryLayer, t.GetGraphicsEmptyObject());

        return true;
    }

    private void SetGraphicsToLayer(int layerToSet, GameObject emptyGraphicsParent)
    {
        int childCount = emptyGraphicsParent.transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            GameObject go = emptyGraphicsParent.transform.GetChild(i).gameObject;
            go.layer = layerToSet;

            //Also do this to this object's children so we go all the way down into the hierarchy
            SetGraphicsToLayer(layerToSet, go);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //If the player enters our trigger, deposit any trash they have
        if (other.tag.Contains("Player"))
        {
            if (trashInInventory.Count > 0)
            {
                SendAllTrashToDumpster();
            }
        }
    }

    private void SendAllTrashToDumpster()
    {
        List<Trash> trashToRemove = new List<Trash>();
        foreach (Trash t in trashInInventory)
        {
            trashToRemove.Add(t);
            SendInventoriedTrashToDumpster(t);
            trashInDumpster++;
        }

        foreach (Trash t2 in trashToRemove)
        {
            trashInInventory.Remove(t2);
        }
    }

    private void SendInventoriedTrashToDumpster(Trash inventoriedTrash)
    {
        //Translate the screen position of an inventoried item into a corrosponding world position in the play space.
        Vector3 screenPOS = inventoryCamera.WorldToScreenPoint(inventoriedTrash.gameObject.transform.position);
        screenPOS.z = vfxLocalZOffset;         //Apply an offset in the camera's local Z so it's not spawning right on top of the camera
        Vector3 vfxSpawnPos = Camera.main.ScreenToWorldPoint(screenPOS);

        GameObject vfx = GameObject.Instantiate<GameObject>(inventoryToDumpsterVFXPrefab);
        vfx.transform.position = vfxSpawnPos;

        //Set the VFX moving towards the dumpster
        IEnumerator coro = AnimateVFXToDumpsterThenCleanUpObjects(vfx, inventoriedTrash);
        StartCoroutine(coro);
    }

    private IEnumerator AnimateVFXToDumpsterThenCleanUpObjects(GameObject vfx, Trash trash)
    {
        //TODO This should probably check the original screen position each frame and lerp that way
        //If you only check that at the start of the animation, then rapidly rotate the camera it looks a bit odd.

        trash.gameObject.SetActive(false);
        bool animationComplete = false;
        Vector3 startingPos = vfx.transform.position;
        Vector3 destinationPos = Dumpster.s.transform.position;
        float u = 0;

        while (!animationComplete)
        {
            u += (Time.deltaTime / vfxAnimationDuration);
            vfx.transform.position = Vector3.Lerp(startingPos, destinationPos, u);

            if (u >= 1)
            {
                animationComplete = true;
            }
            yield return new WaitForEndOfFrame();
        }

        Destroy(vfx);
        Destroy(trash.gameObject);
    }

    public int GetTrashInDumpsterCount()
    {
        return trashInDumpster;
    }

    public int GetTotalTrashInLevelCount()
    {
        return totalTrashInLevel;
    }

    public void ReportTrashIsInLevel()
    {
        totalTrashInLevel++;
    }

    //This should be called by a red coin challenge when it is completed, to preserve the correct trash count.
    public void DeductTrashInLevel()
    {
        totalTrashInLevel--;
    }
}

#if UNITY_EDITOR
//Uncomment this block to disable the player controller's editor
[CustomEditor(typeof(TrashInventory))]
public class TrashInventoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif
