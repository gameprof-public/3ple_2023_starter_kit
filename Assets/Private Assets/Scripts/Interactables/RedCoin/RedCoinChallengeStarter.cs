using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class RedCoinChallengeStarter : Pickup
{
    [SerializeField] private float redCoinDuration = 10f;
    [HideInInspector] [SerializeField] private GameObject myGraphicsObject;
    [HideInInspector] [SerializeField] private GameObject redCoinPrefab;
    [HideInInspector] [SerializeField] private GameObject trashPrefab;
    [HideInInspector] [SerializeField] private RedCoinTrashSpawnHandle myTrashSpawnLocation;
    [FormerlySerializedAs( "myRedCoins" )]
    [KinematicCharacterController.ReadOnly] [SerializeField] private RedCoin[] _redCoins;

    private bool challengeInactive = true; //Is this active? Should turn off while red coins exist, or after the challenge is defeated
    private float timeActivated;

    private float flashingRedCoinDuration = 5f;  //The amount of seconds at the end of a red coin challenge where the coins flash.
    private bool flashingCoins;

    private Dictionary<RedCoin, bool> redCoinPickedUpDictionary;

    private void Start()
    {
        TrashInventory.s.ReportTrashIsInLevel();

        RefreshMyRedCoins();

        redCoinPickedUpDictionary = new Dictionary<RedCoin, bool>();

        foreach (RedCoin r in _redCoins)
        {
            r.RegisterChallengeStarter(this);
            r.DisableMe();
            redCoinPickedUpDictionary.Add(r, false);
        }
    }

    private void RefreshMyRedCoins()
    {
        _redCoins = GetComponentsInChildren<RedCoin>(true);
    }

    protected override void GetPickedUp()
    {
        if (!challengeInactive) return;

        ActivateChallenge();
    }

    private void ActivateChallenge()
    {
        timeActivated = Time.time;

        challengeInactive = false;

        myGraphicsObject.SetActive(false);

        foreach (RedCoin c in _redCoins)
        {
            c.InitializeRedCoin();
            c.EnableMe();
            c.StopFlashing();
        }
    }

    private void DeactivateChallenge()
    {
        challengeInactive = true;

        myGraphicsObject.SetActive(true);

        flashingCoins = false;

        foreach (RedCoin c in _redCoins)
        {
            c.DisableMe();
            redCoinPickedUpDictionary[c] = false;
        }

        timeActivated = -1f;
    }

    private void Update()
    {
        if (!challengeInactive && timeActivated != -1f)
        {
            if ((Time.time -timeActivated) > redCoinDuration)
            {
                DeactivateChallenge();
            }
            else if (TimeToFlashCoins() && !flashingCoins)
            {
                flashingCoins = true;
                foreach (RedCoin c in _redCoins)
                {
                    c.StartFlashing();
                }
            }
        }
    }

    #if UNITY_EDITOR
    private void OnValidate() {
        RefreshMyRedCoins();
    }
    
    // private bool triggerMyRedCoinRefresh = false;
    // private void OnDrawGizmos() {
    //     if ( triggerMyRedCoinRefresh ) {
    //         RefreshMyRedCoins();
    //         triggerMyRedCoinRefresh = false;
    //     }
    // }
    #endif

    private bool TimeToFlashCoins()
    {
        float timeToStartFlashing = timeActivated + (redCoinDuration - flashingRedCoinDuration);

        return (Time.time >= timeToStartFlashing);
    }

    private void ChallengeSucceeded()
    {
        GameObject newTrash = GameObject.Instantiate<GameObject>(trashPrefab);
        newTrash.transform.position = myTrashSpawnLocation.transform.position;
        Dumpster.s.SetUpTrashGraphics(newTrash.GetComponent<Trash>());
        TrashInventory.s.DeductTrashInLevel();

        foreach(RedCoin c in _redCoins)
        {
            Destroy(gameObject);
        }
    }

    #region Coin Hooks
    public void ReportRedCoinGrab(RedCoin coin)
    {
        redCoinPickedUpDictionary[coin] = true;

        foreach (KeyValuePair<RedCoin, bool> kvp in redCoinPickedUpDictionary)
        {
            if (!kvp.Value)
            {
                return;
            }
        }

        //If we get to this point, the challenge was completed
        ChallengeSucceeded();
    }
    #endregion

    #if UNITY_EDITOR
    #region Editor Hooks
    public void AddRedCoin()
    {
        GameObject redCoinObject = PrefabUtility.InstantiatePrefab(redCoinPrefab) as GameObject;
        redCoinObject.transform.SetParent(transform);
        redCoinObject.transform.localPosition = Vector3.up + Vector3.forward;
        RefreshMyRedCoins();
    }

    public void RemoveRedCoin()
    {
        for (int i = transform.childCount -1; i > -1; i--)
        {
            RedCoin rc = transform.GetChild(i).GetComponent<RedCoin>();
            if (rc != null)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
                RefreshMyRedCoins();
                return;
            }
        }
    }

    public void OnDrawGizmosSelected()
    {
        //Draw handles to the Components of the challenge
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.name == "Graphics" || child.name == "Pipe") continue;
            AnimationUtility.DrawBezierGizmo(transform, child, Color.red);
        }
    }
    #endregion
    #endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(RedCoinChallengeStarter))]
public class RedCoinChallengeStarterEditor : Editor
{
    private RedCoinChallengeStarter rccs;

    private void OnEnable()
    {
        rccs = (RedCoinChallengeStarter)target;
    }

    public override void OnInspectorGUI() {
        
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            GUILayout.Space( 10 );
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
            
        }
        
        base.OnInspectorGUI();

        if (GUILayout.Button("Add Red Coin"))
        {
            rccs.AddRedCoin();
        }

        if (GUILayout.Button("Remove Red Coin"))
        {
            rccs.RemoveRedCoin();
        }
        
    }
}
#endif