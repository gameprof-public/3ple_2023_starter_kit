using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;
using UnityEngine.Playables;
using PathCreation;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine.Diagnostics;
using XnTools;

// This was in the KCC example, but I don't know why. - JGB 2020-10-03
//public struct MyMovingPlatformState {
//    public PhysicsMoverState MoverState;
//    public float DirectorTime;
//}

[RequireComponent(typeof(PhysicsMover))]
public class MovingPlatform : MonoBehaviour, IMoverController
{

    public InspectorInfo info = new InspectorInfo("MovingPlatform Instructions", 
        "• Every <b>MovingPlatform</b> must be the child of a <b>MovingPlatform_Path</b>\n" +
        "• <b>AtEndOfPath</b> should be <b>Reverse</b> for most paths but <b>Loop</b> for closed paths\n" +
        "• <b>CurveType</b> affects the movement <b>even if it is not time-based</b>. For a non-time-based curve," +
        " <b>MetersPerSecond</b> will be the average speed for the <b>MovingPlatform</b>\n" +
        "• Only <b>position</b> is animated across the path; <b>rotation</b> is not affected.\n",
        true);
    
    // public InspectorInfo info = new InspectorInfo("MovingPlatform Instructions", 
    //     "- Every MovingPlatform must be the child of a\n"      +
    //     "     MovingPlatform_Path \n"                          +
    //     "- AtEndOfPath should be Reverse for most paths\n"     +
    //     "     but Loop for closed paths \n"                    +
    //     "- CurveType affects the movement even if it is\n"     +
    //     "     not time-based. With a curve, MetersPerSecond\n" +
    //     "     will be the average MetersPerSecond for the\n"   +
    //     "     MovingPlatform \n"                               +
    //     "- Only position is animated across the path.\n"       +
    //     "     Rotation is not affected.",
    //     true);

    public EndOfPathInstruction atEndOfPath = EndOfPathInstruction.Reverse;
    public bool timeBased = false;

    [ShowIf("timeBased")] public float secondsPerLeg = 10f;
    [ShowIf("timeBased")] public float secondsOffset = 0;
    [HideIf("timeBased")] public float metersPerSecond = 4;
    [HideIf("timeBased")] public float metersOffset = 0;

    public CurveType curveType = CurveType.linear;
    [Tooltip("The curveModifier defaults are 2 for Pow curves and 0.2 for Sin curves")]
    public float curveExponent = 2;
    
    
    [KinematicCharacterController.ReadOnly]
    [Tooltip("pathCreate is automatically set to the PathCreator component on the parent of this GameObject")]
    public PathCreator pathCreate;

    private bool previewingInEditor = false;
    private float previewStartTime = 0;

#if UNITY_EDITOR
    [HideIf("previewingInEditor")]
    [Button("Preview In Editor")]
    void PreviewInEditor() {
        previewingInEditor = true;
        previewStartTime = (float) EditorApplication.timeSinceStartup;
        EditorApplication.update += EditorPreviewUpdate;
    }

    [ShowIf("previewingInEditor")]
    [Button("Stop Preview")]
    void StopPreview() {
        previewingInEditor = false;
        EditorApplication.update -= EditorPreviewUpdate;
        transform.position = PositionAtTime(0);
    }
#endif
    
    private PhysicsMover Mover;
    private Transform _transform;

    /// <summary>
    /// In this case, OnDrawGizmos() is used in Editor to set the value of pathCreate
    /// </summary>
    void OnDrawGizmos() {
        if (Application.isPlaying) return;
        if (transform.parent == null) {
            pathCreate = null;
        } else {
            if (pathCreate == null) pathCreate = transform.parent.GetComponent<PathCreator>();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        OnDrawGizmos();
        transform.parent?.GetComponent<RedCoinChallengeStarter>()?.OnDrawGizmosSelected();
    }
#endif

    private void Start() {
        //pathCreate = GetComponent<PathCreator>(); // pathCreate should be set in OnValidate() - JGB 2022-10-04
        if (pathCreate == null) {
            Debug.LogWarning("The parent of this MovingPlatform must have a PathCreator component for this to move.");   
        }
        Mover = GetComponent<PhysicsMover>();

        _transform = this.transform;

        Mover.MoverController = this;
    }



    // This is called every FixedUpdate by our PhysicsMover in order to tell it what pose it should go to
    public void UpdateMovement( out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime ) {
        goalPosition = PositionAtTime( Time.time );
        goalRotation = _transform.rotation;
        
        // None of this is necessary because we send back a position rather than having a Director move the position. - JGB 2022-10-06
        // // Remember pose before animation
        // Vector3 _positionBeforeAnim = _transform.position;
        // Quaternion _rotationBeforeAnim = _transform.rotation;
        //
        // // Update animation
        // PositionAtTime( Time.time );
        //
        //
        // // Set our platform's goal pose to the animation's
        // goalPosition = _transform.position;
        // goalRotation = _transform.rotation;
        //
        // // Reset the actual transform pose to where it was before evaluating. 
        // // This is so that the real movement can be handled by the physics mover; not the animation
        // _transform.position = _positionBeforeAnim;
        // _transform.rotation = _rotationBeforeAnim;
    }

    public Vector3 PositionAtTime( float time ) {
        if ( pathCreate == null ) return transform.position;
        // TODO: Implement this to work with the Bezier Path Creator
        float u = 0;
        float duration = secondsPerLeg, offset = secondsOffset;
        if (!timeBased) {
            // Convert the distance to time so that u can be curved
            duration = pathCreate.path.length / metersPerSecond;
            offset = metersOffset / metersPerSecond;
        }
        
        u = (time + offset) / duration;
        u = VertexPath.ManageEndOfPath(u, atEndOfPath); // Handle extrapolation before curving u
        u = XnInterpolation.Curve(u, curveType, curveExponent);
        return pathCreate.path.GetPointAtTime(u);
    }

#if UNITY_EDITOR
    void EditorPreviewUpdate() {
        if (!previewingInEditor) {
            Debug.LogError("EditorPreviewUpdate() called, but previewingInEditor is false");
        }
        float t = (float) EditorApplication.timeSinceStartup - previewStartTime;
        transform.position = PositionAtTime(t);
    }
#endif
}