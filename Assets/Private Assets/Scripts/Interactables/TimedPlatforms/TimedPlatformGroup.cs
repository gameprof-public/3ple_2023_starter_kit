using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// A class for a manager object of a timed platform challenge triggered by a button.
/// </summary>
public class TimedPlatformGroup : ButtonInteractable
{
    [Tooltip("The duration my platforms will appear after a linked button press.")]
    [SerializeField] private float platformDuration = 12f;
    [SerializeField] private GameObject platformPrefab;

    private float timeFlashingThreshhold = 5f; //How many seconds a platform flashes before it disappears
    private float timeButtonActivated = -1f;
    private bool platformsFlashing = false;
    [SerializeField][KinematicCharacterController.ReadOnly]
    private TimedPlatform[] myTimedPlatforms;

    // Start is called before the first frame update
    void Start()
    {
        RefreshMyTimedPlatforms();
        foreach (TimedPlatform tp in myTimedPlatforms)
        {
            tp.Disappear();
        }
    }

    void RefreshMyTimedPlatforms() {
        myTimedPlatforms = GetComponentsInChildren<TimedPlatform>(true);
    }

    private void Update()
    {
        if (timeButtonActivated != -1)
        {
            if ((Time.time - platformDuration) >= timeButtonActivated)
            {
                DisappearMyPlatforms();
            }
            else if (!platformsFlashing && TimeToFlash())
            {
                FlashMyPlatforms();
            }
        }
    }

    private bool TimeToFlash()
    {
        float timeToStartFlashing = timeButtonActivated + (platformDuration - timeFlashingThreshhold);

        return (Time.time >= timeToStartFlashing);
    }

    public override void OnPress()
    {
        timeButtonActivated = Time.time;
        AppearMyPlatforms();
        
    }

    private void AppearMyPlatforms()
    {
        platformsFlashing = false;

        foreach (TimedPlatform tp in myTimedPlatforms)
        {
            tp.Appear();
            tp.HaltFlashing();
        }
    }

    //Cause the platforms to start flashing to indicate they will soon disappear.
    private void FlashMyPlatforms()
    {
        platformsFlashing = true;
        foreach (TimedPlatform tp in myTimedPlatforms)
        {
            tp.StartFlashingMyMesh();
        }
    }

    private void DisappearMyPlatforms()
    {
        platformsFlashing = false;

        timeButtonActivated = -1f;

        foreach (TimedPlatform tp in myTimedPlatforms)
        {
            tp.Disappear();
        }
    }

    #region Editor Hooks

    private void OnValidate() {
        RefreshMyTimedPlatforms();
    }

    public void AddPlatform()
    {
#if UNITY_EDITOR
        GameObject newPlatform = PrefabUtility.InstantiatePrefab(platformPrefab) as GameObject;
        newPlatform.transform.SetParent(transform);
        newPlatform.transform.localPosition = Vector3.up;
        RefreshMyTimedPlatforms();
#endif
    }

    public void RemovePlatform()
    {
        int indexToRemove = transform.childCount - 1;
        DestroyImmediate(transform.GetChild(indexToRemove).gameObject);
        RefreshMyTimedPlatforms();
    }
    #endregion

#if UNITY_EDITOR
    public override void OnDrawGizmosSelected()
    {
        //Draw handles to the platforms
        for (int i =0; i< transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            AnimationUtility.DrawBezierGizmo(transform, child, Color.cyan);
        }
    }
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(TimedPlatformGroup))]
public class TimedPlatformGroupEditor: Editor
{
    private TimedPlatformGroup tpg;
    private void OnEnable()
    {
        tpg = (TimedPlatformGroup)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Add Platform"))
        {
            tpg.AddPlatform();
        }
        
        if (GUILayout.Button("Remove Platform"))
        {
            tpg.RemovePlatform();
        }
    }
}
#endif
