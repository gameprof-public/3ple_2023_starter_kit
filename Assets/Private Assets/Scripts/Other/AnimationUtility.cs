using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// A class for storing broadly useful animation functions and coroutines.
/// </summary>
public class AnimationUtility
{
    public static IEnumerator AnimatePositionToTransform(Transform animatedTransform, Transform target, float speed, AnimationEvent eventToCallAfterFunctionEnd = null)
    {
        Vector3 start = animatedTransform.position;
        float u = 0;

        while (Vector3.Distance(animatedTransform.position, target.position) > .02f)
        {
            animatedTransform.position = Vector3.Lerp(start, target.position, u);
            u += (Time.deltaTime * speed);

            yield return new WaitForEndOfFrame();
        }


        if (eventToCallAfterFunctionEnd != null)
        {
            eventToCallAfterFunctionEnd.Invoke(animatedTransform);
        }
    }    
    
    public static IEnumerator AnimateScale(Transform animatedTransform, Vector3 target, float speed, AnimationEvent eventToCallAfterFunctionEnd = null)
    {
        Vector3 start = animatedTransform.localScale;
        float u = 0;

        while (Vector3.Distance(animatedTransform.localScale, target) > .02f)
        {
            animatedTransform.localScale = Vector3.Lerp(start, target, u);
            u += (Time.deltaTime * speed);

            yield return new WaitForEndOfFrame();
        }

        if (eventToCallAfterFunctionEnd != null)
        {
            eventToCallAfterFunctionEnd.Invoke(animatedTransform);
        }
    }

    public static IEnumerator AnimateAlongBezierCurve(Transform animatedTransform, Vector3 target, Vector3 bPoint, float speed, AnimationEvent eventToCallAfterFunctionEnd = null)
    {
        Vector3 start = animatedTransform.position;
        float u = 0;

        while (Vector3.Distance(animatedTransform.position, target) > .02f)
        {
            animatedTransform.position = GetPositionAlongBezierCurve(start, target, bPoint, u);
            u += Time.deltaTime * speed;

            yield return new WaitForEndOfFrame();
        }

        if (eventToCallAfterFunctionEnd != null)
        {
            eventToCallAfterFunctionEnd.Invoke(animatedTransform);
        }
    }

    /// <summary>
    /// Gets a nice bezier b point for an object curving down from a high position
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static Vector3 GetBezierBPointStartingFromHighPosition(Vector3 start, Vector3 end)
    {
        Vector3 bPoint = Vector3.zero;
        bPoint.y = start.y;
        bPoint.x = end.x;
        bPoint.z = end.z;
        return bPoint;
    }

    private static Vector3 GetPositionAlongBezierCurve(Vector3 startPosition, Vector3 endPosition, Vector3 bPointPosition, float u)
    {
        Vector3 pA = startPosition;
        Vector3 pC = endPosition;
        Vector3 pB = bPointPosition;

        Vector3 pAB = (1 - u) * pA + u * pB;
        Vector3 pBC = (1 - u) * pB + u * pC;
        Vector3 pABC = (1 - u) * pAB + u * pBC;

        return pABC;
    }

#if UNITY_EDITOR
    /// <summary>
    /// From Freya Holmer's video: https://www.youtube.com/watch?v=pZ45O2hg_30&t=1287s
    /// </summary>
    /// <param name="target"></param>
    public static void DrawBezierGizmo(Transform origin,Transform target, Color chosenColor)
    {
        Vector3 targetPos = target.position;
        Vector3 myPos = origin.position;
        float halfHeight = (targetPos.y - myPos.y) / 2;
        Vector3 bPoint = Vector3.up * halfHeight;

        Handles.DrawBezier(myPos, targetPos, (targetPos - bPoint), (myPos + bPoint), chosenColor, EditorGUIUtility.whiteTexture, 3f);
    }
#endif
}

/// <summary>
/// A UnityEvent that takes a transform as a parameter.
/// </summary>
public class AnimationEvent : UnityEvent<Transform> { }
