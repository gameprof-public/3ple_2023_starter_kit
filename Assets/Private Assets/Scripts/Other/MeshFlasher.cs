using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshFlasher : MonoBehaviour
{
    [SerializeField] private MeshRenderer myMeshRenderer;
    private float flashDuration = .1f;

    public void StartFlashing()
    {
        if (gameObject.activeInHierarchy) StartCoroutine(FlashMyMesh(flashDuration));
    }

    public void HaltFlashing()
    {
        myMeshRenderer.enabled = true;
        StopAllCoroutines();
    }

    private IEnumerator FlashMyMesh(float flashDuration)
    {
        float timeLastFlashProcced = Time.time;

        while (true)
        {
            if ((Time.time - timeLastFlashProcced) >= flashDuration)
            {
                timeLastFlashProcced += flashDuration;
                ToggleMeshRenderer();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void ToggleMeshRenderer()
    {
        if (myMeshRenderer.enabled)
        {
            myMeshRenderer.enabled = false;
        }
        else
        {
            myMeshRenderer.enabled = true;
        }
    }
}
