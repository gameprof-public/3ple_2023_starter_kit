using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(TrashLover))]
public class PossumEnemy : Enemy
{
    private enum ePossumState { IDLE, WALKING, CHASING, SLOWING, DEFEATEDING, DEPLOYING, HEADINGTOTRASH }

    [Header("Possum Enemy Settings")]
    [SerializeField] private GameObject babyPossumPrefab;
    [SerializeField] private int numberBabiesToSpawn;
    [SerializeField] private GameObject graphicsGameObject;
    [Tooltip("Assign an empty transform to the position we want the baby possums to appear from.")]
    [SerializeField] private Transform pouchExitTransform;
    [Tooltip("Babies spawn with a velocity of this magnatude applied to their rigidbody.")]

    [Header("Possum Baby Spawning Settings")]
    [SerializeField] private float babySpawnMagnitude = 3;
    [SerializeField] private GameObject hitboxGameObject;

    [SerializeField] private Vector3 babySpawnDirection;

    [Header("Turtle Testing Variables")]
    //private bool testStarted = false;
    public bool startTest;

    private ePossumState myState = ePossumState.IDLE;

    //References to my components
    Rigidbody myRigid;
    //agent is my agent

    private float timeSpawned; //used during baby spawning to make sure we don't leave deploying mode too early

    //Trashlover variables
    private TrashLover trashLover;

    [SerializeField] private Animator animator;

    public override void Start()
    {
        base.Start();
        InitializeAgentAndRigidbody();

        //Set up the trashlover and subscribe to the trash events
        trashLover = GetComponent<TrashLover>();
        trashLover.RegisterWithTrashDetectedEvent(ChangeStateDueToTrashDetection);
        trashLover.RegisterWithTrashSwipedEvent(ForceIdle);
        trashLover.RegisterWithTrashPickedUpEvent(ForceIdle);

        if (animator == null)
        {
            Debug.LogWarning("A possom enemy is missing its animator component reference.");
        }
    }

    private void InitializeAgentAndRigidbody()
    {
        if (myRigid == null)
        {
            myRigid = GetComponent<Rigidbody>();
        }

        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }
    }

    public override void Update()
    {
        base.Update();  //Made a check in the Enemy class regarding wether to switch to custom points or not

        switch (myState)
        {
            case ePossumState.IDLE:
                SetMovingAnimation(false);

                //Set idle end time if hasn't yet been done
                if (timeIdleEnd == -1)
                {
                    float timeAdd = Random.Range(minIdleTime, maxIdleTime);
                    timeIdleEnd = Time.time + timeAdd;
                }

                //See if player is detected
                if (PlayerDetected())
                {
                    myState = ePossumState.CHASING;
                }

                //Check if the time to be idle has ended
                if (Time.time >= timeIdleEnd)
                {
                    //Reset idle time
                    timeIdleEnd = -1;

                    if (this.switchToCustomPoints == false) //If we are not using custom points
                    {
                        //Choose a destincation and set it
                        Vector3 destination = RandomNavmeshPointWithinSphere(patrolRadius);
                        SetMoveDestination(destination);
                    }

                    else
                    {
                        if (onPatrolPath == false)
                        {
                            Vector3 destination = FindNearestPointOnPath(this.patrolPoints, this.transform.position);
                        }

                    }
                    myState = ePossumState.WALKING;

                }
                break;

            case ePossumState.WALKING:
                SetMovingAnimation(true);
                if (Vector3.Distance(transform.position, agent.destination) < .05f)
                {
                    myState = ePossumState.IDLE;
                }
                break;

            case ePossumState.CHASING:
                SetMovingAnimation(true);
                Vector3 playerPos = PlayerController.GetPlayerPosition();
                //Check if we need to stop chasing (too far, or too above//below) This may need to change if the enemy can walk or up ramps
                if (Vector3.Distance(transform.position, playerPos) >= escapeRange || Mathf.Abs(transform.position.y - playerPos.y) >= 2.5f)
                {
                    IEnumerator coro = SlowDownAndStop(.75f);
                    StartCoroutine(coro);

                    myState = ePossumState.SLOWING;
                }

                //Keep the player the destination if not
                Vector3 newDest = GetPositionOnNavmesh(playerPos);
                SetMoveDestination(newDest);
                break;
            case ePossumState.DEFEATEDING:
                //I think we just handle this in another coroutine, but this is here so we stop doing other activities in Update
                break;
            case ePossumState.DEPLOYING:
                if ((Time.time - timeSpawned) > .5f) //Don't cancel deploying instantly
                {
                    //Fire a raycast down to see if we're hitting the ground
                    float halfHeight = agent.height / 2;
                    halfHeight *= transform.localScale.y;
                    Vector3 rayVector = Vector3.down;
                    Ray r = new Ray(transform.position, rayVector);
                    RaycastHit hit;

                    if (Physics.Raycast(r, out hit, halfHeight))
                    {
                        //See if this is the floor
                        if (hit.collider.gameObject.layer == 0 && !hit.collider.tag.Contains("Player"))
                        {
                            LeaveSpawnMode();
                        }
                    }
                }
                break;
            case ePossumState.HEADINGTOTRASH: //This state is entered via the trash detected event on my trash lover
                SetMovingAnimation(true);
                SetMoveDestination(trashLover.GetPositionOfDesiredTrash());
                break;
        }
    }

    public override bool GetJumpedOn()
    {
        bool jumpedOn = base.GetJumpedOn();
        if (jumpedOn)
        {
            StartCoroutine(GetSquishedAndDeployBabies(.5f, .5f));
        }
        return jumpedOn;
    }

    private IEnumerator GetSquishedAndDeployBabies(float stunDuration, float delayBeforeDestruction)
    {
        graphicsGameObject.SetActive(false);

        myState = ePossumState.DEFEATEDING;

        //Spawn some kind of smoke thing

        //Spawn the babies and put them in being spawned mode. This current implementation only works with 4 babies
        Vector3 babySpawnVector = babySpawnDirection.normalized * babySpawnMagnitude;
        for (int i = 0; i< numberBabiesToSpawn; i++)
        {
            GameObject newBaby = GameObject.Instantiate<GameObject>(babyPossumPrefab);
            newBaby.transform.position = pouchExitTransform.position;

            //This prefab will spawn in with a kinimatic rigidbody and a navmesh agent. We need to disable the agent and enable the physics while we're tossing the babies around.
            PossumEnemy newPossum = newBaby.GetComponent<PossumEnemy>();
            //Rigidbody newRigid = newBaby.GetComponent<Rigidbody>();
            //NavMeshAgent newAgent = newBaby.GetComponent<NavMeshAgent>();

            //PossumSpawnData psd = new PossumSpawnData(newPossum, newRigid, newAgent);
            //psdList.Add(psd);

            //newRigid.isKinematic = false;
            //newAgent.enabled = false;

            //Vector3 spawnAngle = Vector3.up;
            //spawnAngle.x += Random.Range(-1, 1);
            //spawnAngle.z += Random.Range(-1, 1);
            //spawnAngle.Normalize();
            //spawnAngle *= babySpawnMagnitude;

            newPossum.GetSpawned(babySpawnVector);

            Debug.DrawLine(transform.position, (transform.position + babySpawnVector), Color.red, 10f);

            //Alternating even and odd, flip the X or Z access of the spawn vector.
            if (i % 2 == 0)
            {
                babySpawnVector.x = -babySpawnVector.x;
            }
            else
            {
                babySpawnVector.z = -babySpawnVector.z;
            }
        }

        yield return new WaitForSeconds(delayBeforeDestruction);
        Die();
    }

    protected override void Die()
    {
        trashLover.DropTrashIfImHoldingOne();
        base.Die();
    }

    public void GetSpawned(Vector3 spawnVelocity)
    {
        numberBabiesToSpawn = 0;
        myState = ePossumState.DEPLOYING;

        InitializeAgentAndRigidbody();

        myRigid.isKinematic = false;
        myRigid.useGravity = true;
        myRigid.velocity = spawnVelocity;
        //hitboxGameObject.SetActive(false);
        agent.enabled = false;
        timeSpawned = Time.time;
    }

    private void LeaveSpawnMode()
    {
        myState = ePossumState.IDLE;
        myRigid.isKinematic = true;
        myRigid.velocity = Vector3.zero;
        hitboxGameObject.SetActive(true);
        agent.enabled = true;
    }

    private IEnumerator SlowDownAndStop(float slowSpeed)
    {
        float t = 0;
        float speedSave = agent.speed;

        while (t < 1)
        {
            agent.speed = Mathf.Lerp(speedSave, 0, t);
            t += Time.deltaTime * slowSpeed;
            yield return new WaitForEndOfFrame();
        }

        ClearDestination();
        agent.speed = speedSave;
        myState = ePossumState.IDLE;
    }

    /// <summary>
    /// A data-holding class used when spawning new possums.
    /// </summary>
    //private class PossumSpawnData
    //{
    //    public PossumEnemy possum;
    //    public Rigidbody rigidbody;
    //    public NavMeshAgent agent;

    //    public PossumSpawnData(PossumEnemy possumValue, Rigidbody rigidbodyValue, NavMeshAgent agentValue)
    //    {
    //        possum = possumValue;
    //        rigidbody = rigidbodyValue;
    //        agent = agentValue;
    //    }
    //}

    #region Trashlover Interactions
    private void ChangeStateDueToTrashDetection()
    {
        myState = ePossumState.HEADINGTOTRASH;
    }

    /// <summary>
    /// Used by TrashLover to send me to idle.
    /// </summary>
    private void ForceIdle()
    {
        myState = ePossumState.IDLE;
    }
    #endregion

    #region Animation
    private void SetMovingAnimation(bool moving)
    {
        animator.SetBool("Moving", moving);
    }
    #endregion
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(PossumEnemy))]
public class PossumEnemyEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif
