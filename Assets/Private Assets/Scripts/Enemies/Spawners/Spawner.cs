using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class for controlling game objects that continually spawn enemies
/// </summary>
public class Spawner : MonoBehaviour
{
    [SerializeField] private int maxSpawnedEnemies = 2;
    [SerializeField] private GameObject spawnableEnemyPrefab;
    [SerializeField] private float timeBetweenSpawns = 8f;
    [SerializeField] private Transform spawnLocationEmptyTransform;
    private float timeLastSpawnPrepped = 0;

    private List<Enemy> mySpawnedEnemies;

    // Start is called before the first frame update
    public virtual void Start()
    {
        mySpawnedEnemies = new List<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mySpawnedEnemies.Count == maxSpawnedEnemies)
        {
            timeLastSpawnPrepped = Time.time;
            return;
        }

        if (Time.time - timeLastSpawnPrepped >= timeBetweenSpawns)
        {
            SpawnEnemy();
            timeLastSpawnPrepped = Time.time;
        }
    }

    protected virtual void SpawnEnemy()
    {
        GameObject enemyOb = GameObject.Instantiate<GameObject>(spawnableEnemyPrefab, spawnLocationEmptyTransform.position, transform.rotation);
        Enemy myEnemy = enemyOb.GetComponent<Enemy>();
        myEnemy.ForceEventInitialization();
        mySpawnedEnemies.Add(myEnemy);
        myEnemy.SubscribeToOnDie(RemoveEnemyOnDeath);
    }

    private void RemoveEnemyOnDeath(Enemy enemy)
    {
        mySpawnedEnemies.Remove(enemy);
    }
}
