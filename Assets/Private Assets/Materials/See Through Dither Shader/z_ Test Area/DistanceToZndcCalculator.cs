using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This method doesn't actually seem to work the way I'd hoped. :( - JGB 2023-10-07
/// </summary>
[ExecuteInEditMode]
public class DistanceToZndcCalculator : MonoBehaviour {
    [XnTools.ReadOnly]
    [SerializeField]
    private float camNear, camFar;
    [Range( 0, 100 )]
    [Tooltip("This is the distance in front of the camera that you want to convert" +
        " into a Zndc (Z Normalized Device Coordinates) value that can be compared" +
        " against the depth buffer.")]
    public float distance = 10;
    [XnTools.ReadOnly]
    [SerializeField]
    private float Wc, Zc, Zndc;

    // Update is called in EditMode whenever a value is changed
    void Update() {
        if ( Application.isPlaying ) return;

        Camera cam = GetComponent<Camera>();
        if (cam == null) {
            Debug.LogError( "The DistanceToZndcCalculator script must be on a Camera to work!" );
            return;
        }
        camNear = cam.nearClipPlane;
        camFar = cam.farClipPlane;

        // Create a Vector3 in world space that is distance in front of the camera
        Vector4 vW = transform.position + (transform.forward * distance);

        // Move this into Camera Space
        Vector4 vCam = cam.worldToCameraMatrix * vW;
        vCam.w = 1; // Add a w component of 1

        // Use the Camera projectionMatrix to calculate Zndc from distance
        Vector4 clipSpace = cam.projectionMatrix * vCam;
        Wc = clipSpace.w;
        Zc = clipSpace.z;
        Zndc = clipSpace.z / clipSpace.w;
    }
}