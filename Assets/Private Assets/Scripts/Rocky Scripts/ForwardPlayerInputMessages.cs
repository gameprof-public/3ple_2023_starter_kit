using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ForwardPlayerInputMessages : MonoBehaviour {
    public GameObject[] recipientGameObjects;

    #region PlayerInput Functions
    private void OnMove( InputValue value ) {
        foreach (GameObject go in recipientGameObjects) {
            go.SendMessage( "OnMove", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnLookInput( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnLookInput", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnLook( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnLook", value, SendMessageOptions.DontRequireReceiver );
        }
    }
    
    private void OnLookMod( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnLookMod", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnJump( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnJump", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnCrouch( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnCrouch", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnDive( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnDive", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnGroundPound( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnGroundPound", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnMousePosition( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnMousePosition", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnRightClick( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnRightClick", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnMouseRightButtonDown( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnMouseRightButtonDown", value, SendMessageOptions.DontRequireReceiver );
        }
    }

    private void OnMouseRightButtonUp( InputValue value ) {
        foreach ( GameObject go in recipientGameObjects ) {
            go.SendMessage( "OnMouseRightButtonUp", value, SendMessageOptions.DontRequireReceiver );
        }
    }
    #endregion
}
