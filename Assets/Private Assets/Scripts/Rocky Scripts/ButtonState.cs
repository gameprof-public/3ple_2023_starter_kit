/// <summary>
/// Allows a single enum to store all phases of a button press.
/// Button is down the same frame as pressed and up the same frame as released
/// </summary>
public struct ButtonState {
    public enum eInputButtonState { free, held, up, down };
    public eInputButtonState state;


    /// <summary>
    /// Is this button currently NOT held (i.e., free)?
    /// </summary>
    public bool isFree {
        get { return ( state == eInputButtonState.free || state == eInputButtonState.up ); }
    }
    /// <summary>
    /// Is this button currently held.
    /// </summary>
    public bool isHeld {
        get { return ( state == eInputButtonState.held || state == eInputButtonState.down ); }
    }
    /// <summary>
    /// Was this button released this frame (more exactly since the last Progress() was called)?
    /// </summary>
    public bool up {
        get { return ( state == eInputButtonState.up ); }
    }
    /// <summary>
    /// Was this button pressed this frame (more exactly since the last Progress() was called)?
    /// </summary>
    public bool down {
        get { return ( state == eInputButtonState.down ); }
    }
    public override string ToString() {
        if ( state == eInputButtonState.free ) return "____";
        if ( state == eInputButtonState.held ) return "HELD";
        if ( state == eInputButtonState.down ) return "DOWN";
        if (state  == eInputButtonState.up)    return "_UP_";
        return "_REL";
    }
    /// <summary>
    /// Converts eIBS.pressed to eIBS.down
    /// Converts eIBS.released to eIBS.up
    /// </summary>
    public void Progress() {
        if ( state == eInputButtonState.down ) {
            state = eInputButtonState.held;
        }
        if ( state == eInputButtonState.up ) {
            state = eInputButtonState.free;
        }
    }

    /// <summary>
    /// This assigns the value of the ButtonState based on a bool.
    /// If the state is up or released, and buttonVal==true, state will become pressed
    /// If the state is down or pressed, and buttonVal==false, state will become released
    /// This does NOT automatically Progress() the ButtonState!!!
    /// </summary>
    /// <param name="buttonVal"></param>
    public void Set( bool buttonVal ) {
        if ( buttonVal && isFree ) {
            state = eInputButtonState.down;
        }
        if ( !buttonVal && isHeld ) {
            state = eInputButtonState.up;
        }
    }

    public static implicit operator bool (ButtonState bs) {
        return bs.isHeld;
    }

    public static implicit operator ButtonState(bool b) {
        ButtonState bs = new ButtonState();
        bs.Set(b);
        return bs;
    }
}
/*
public enum eInputButtonState { up, down, released, pressed };
public static class eInputButtonStateExtensions {
    static public bool Up( this eInputButtonState eIBS ) {
        return ( eIBS == eInputButtonState.up || eIBS == eInputButtonState.released );
    }
    static public bool Down( this eInputButtonState eIBS ) {
        return ( eIBS == eInputButtonState.down || eIBS == eInputButtonState.pressed );
    }
    static public bool Released( this eInputButtonState eIBS ) {
        return ( eIBS == eInputButtonState.released );
    }
    static public bool Pressed( this eInputButtonState eIBS ) {
        return ( eIBS == eInputButtonState.pressed );
    }
    static public string ToString( this eInputButtonState eIBS ) {
        if ( eIBS == eInputButtonState.up ) return "_UP_";
        if ( eIBS == eInputButtonState.down ) return "DOWN";
        if ( eIBS == eInputButtonState.pressed ) return "PRES";
        return "RELS";
    }
    /// <summary>
    /// Converts eIBS.pressed to eIBS.down
    /// Converts eIBS.released to eIBS.up
    /// </summary>
    /// <param name="eIBS"></param>
    static public void Progress( this eInputButtonState eIBS ) {
        if ( eIBS == eInputButtonState.pressed ) {
            eIBS = eInputButtonState.down;
        }
        if ( eIBS == eInputButtonState.released ) {
            eIBS = eInputButtonState.up;
        }
    }
    /// <summary>
    /// This assigns the value of the eInputButtonState based on a bool.
    /// If the eIBS is up or released, and bool==true, it will become pressed
    /// If the eIBS is down or pressed, and bool==false, it will become released
    /// This does NOT automatically Progress() the eIBS!!!
    /// </summary>
    /// <param name="eIBS"></param>
    /// <param name="buttonVal"></param>
    static public void Set( this eInputButtonState eIBS, bool buttonVal ) {
        if ( buttonVal && eIBS.Up() ) {
            eIBS = eInputButtonState.pressed;
        }
        if ( !buttonVal && eIBS.Down() ) {
            eIBS = eInputButtonState.released;
        }
    }
}
*/