using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPointHandle : MonoBehaviour
{
    private void OnDrawGizmosSelected()
    {
    
        Gizmos.color = Color.red; 
        float radius = 1;
        Vector3 gizmoCenter = transform.position + Vector3.up * radius;
        Gizmos.DrawSphere(gizmoCenter, radius);
    }
}
