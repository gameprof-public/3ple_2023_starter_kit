using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VersionTextDisplayer : MonoBehaviour
{
    [SerializeField][KinematicCharacterController.ReadOnly]
    TMP_Text myText;

    // Start is called before the first frame update
    void Start()
    {
        myText = GetComponent<TMP_Text>();
        myText.text = "v: " + Application.version;
    }
    
    #if UNITY_EDITOR
    void OnDrawGizmos() {
        if (myText == null) {
            myText = GetComponent<TMP_Text>();
        }
        myText.text = "v: " + Application.version;
    }

#endif
}
