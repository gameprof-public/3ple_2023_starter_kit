using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

public class MeshUtilities
{
    /// <summary>
    /// Code written by Jeremy. Returns a PB face given the requisite data.
    /// The object needs a concave mesh collider for it to work.
    /// </summary>
    /// <param name="pbm"></param>
    /// <param name="hit"></param>
    /// <returns></returns>
    public static Face GetFaceFromProbuilderMeshAndRaycastHit(ProBuilderMesh pbm, RaycastHit hit)
    {
        Face foundFace = null;

        // Do ProBuilder and MeshRenderer share the same mesh?
        if (pbm.meshSyncState != MeshSyncState.InSync)
        {
            return null;
        }
        // Is the Collider a MeshCollider?
        if (!(hit.collider is MeshCollider))
        {
            return null;
        }
        // Does the MeshCollider share a mesh with the MeshRenderer?
        MeshFilter meshFilter = hit.collider.GetComponent<MeshFilter>();
        MeshCollider meshCollider = hit.collider as MeshCollider;
        if (meshFilter.sharedMesh != meshCollider.sharedMesh)
        {
            return null;
        }

        // So, now we know that all three share the same mesh!!
        // This means that we can use the triangleIndex from the RaycastHit on all of them.
        int triangleIndex3 = hit.triangleIndex * 3; // Index of the 0th vert of this triangle in the triangles array.
                                                    // Get the indices of the three verts that make up this triangle (same indices in the normals array)
        int[] vertNums = new int[3];
        Vector3[] localPositions = new Vector3[3];
        Vector3[] positions = new Vector3[3];
        for (int i = 0; i < 3; i++)
        {
            vertNums[i] = meshCollider.sharedMesh.triangles[triangleIndex3 + i];
            localPositions[i] = meshCollider.sharedMesh.vertices[vertNums[i]];
            positions[i] = meshCollider.transform.TransformPoint(meshCollider.sharedMesh.vertices[vertNums[i]]);
        }

        // Get the positions of the pbm.sharedVertices
        List<Vector3> pbVertPositions = new List<Vector3>();
        foreach (SharedVertex sv in pbm.sharedVertices)
        {
            pbVertPositions.Add(pbm.positions[sv[0]]);
        }
        // Find the sharedVertices at the localPositions
        int[] sharedVertNums = new int[3];
        float distThreshold = 0.01f;
        for (int i = 0; i < pbVertPositions.Count; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if ((pbVertPositions[i] - localPositions[j]).sqrMagnitude <= distThreshold)
                {
                    sharedVertNums[j] = i;
                }
            }
        }

        // Get the array to look up sharedVertNums from Face vert nums
        int[] vToSv = pbm.VertToSharedVertArray();

        // Find the Face that contains this triangle
        int[] faceDSIs;
        foreach (Face face in pbm.faces)
        {
            // Check to see if all three sharedVertNums are in this Face
            faceDSIs = face.DistinctSharedIndexes(vToSv);
            // As soon as one fails, continue to the next Face
            if (System.Array.IndexOf<int>(faceDSIs, sharedVertNums[0]) == -1) continue;
            if (System.Array.IndexOf<int>(faceDSIs, sharedVertNums[1]) == -1) continue;
            if (System.Array.IndexOf<int>(faceDSIs, sharedVertNums[2]) == -1) continue;
            // If we got here, all three are in this Face. Assign foundFace and break the loop
            foundFace = face;
            break;
        }

        return foundFace;
    }
}
