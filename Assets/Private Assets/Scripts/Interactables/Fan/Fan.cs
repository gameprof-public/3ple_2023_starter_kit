using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Fan : ButtonInteractable
{
    public enum eFanMode { ALWAYSON, BUTTONACTIVATEDPERMANENT, BUTTONACTIVATEDTIMER, MODULATING }

    [FormerlySerializedAs( "blowagePerSecond" )]
    [SerializeField] private float accelerationForce = 20f;
    [Header("References")]
    [HideInInspector] [SerializeField] private ParticleSystem pSystem;
    //[SerializeField] private Transform fanHubTransform;
    [SerializeField] private eFanMode fanMode;


    private bool                     playerInAirstream = false;
    private PlayerController         pConSave;
    private Animator                 anim;


    private bool on;

    //Modulating Settings
    [HideInInspector] public float modulationDuration = 5f;
    private float timeModulationLastChanged;

    //Button Press Timer Settings
    [HideInInspector] public float timerDuration;
    private float timeTimerStarted = -1f;

    //Animation settings
    //private float fullSpeed = 270f;
    //private float accel = .02f;
    //private float decel = .02f;
    //private float curSpeed = 0f;


    private void Start()
    {
        anim = GetComponent<Animator>();

        if (fanMode == eFanMode.ALWAYSON)
        {
            Activate();
        }
        else if (fanMode == eFanMode.MODULATING)
        {
            timeModulationLastChanged = Time.time;
        }
    }

    private void FixedUpdate()
    {
        if (fanMode == eFanMode.MODULATING)
        {
            HandleModulation();
        }

        if (fanMode == eFanMode.BUTTONACTIVATEDTIMER)
        {
            HandleTimer();
        }

        if (playerInAirstream && on)
        {
            pConSave.Accelerate(transform.up * accelerationForce);
            // Vector3 velToAdd = transform.up * (accelerationForce * Time.fixedDeltaTime);
            // pConSave.AddVelocity(velToAdd);
        }

        //Old animation implementation, we'll do it a different way once Diego does his animations.
        //HandleAnimation();
    }

    #region Fan Functions
    private void Activate()
    {
        on = true;

        pSystem.Play();
    }

    private void Deactivate()
    {
        on = false;

        pSystem.Stop();
    }

    private void HandleModulation()
    {
        if ((Time.time - timeModulationLastChanged) > modulationDuration)
        {
            timeModulationLastChanged = Time.time;
            if (on)
            {
                Deactivate();
            }
            else
            {
                Activate();
            }
        }
    }

    //private void HandleAnimation()
    //{
    //    float targetSpeed = 0f;
    //    float t = decel;
    //    if (on)
    //    {
    //        targetSpeed = fullSpeed;
    //        t = accel;
    //    }

    //    curSpeed = Mathf.Lerp(curSpeed, targetSpeed, t);
    //    Vector3 euler = new Vector3(0, curSpeed * Time.fixedDeltaTime, 0);


    //    fanHubTransform.Rotate(euler);
    //}

    private void HandleTimer()
    {
        if (timeTimerStarted != -1f) //Make sure the timer is actually started
        {
            if ((Time.time - timeTimerStarted) >= timerDuration)
            {
                Deactivate();
                timeTimerStarted = -1f;
            }
        }
    }
    #endregion

    #region Button Interaction
    public override void OnPress()
    {
        switch (fanMode)
        {
            case eFanMode.BUTTONACTIVATEDPERMANENT:
                Activate();
                break;
            case eFanMode.BUTTONACTIVATEDTIMER:
                Activate();
                timeTimerStarted = Time.time;
                break;
        }
    }
    #endregion

    #region Hitbox Hooks
    public void CheckAndAddPlayerToAirstream(Collider other) {
        // RockyCharacterController rcc = RockyCharacterController.GetRCCFromCollider( other );
        // if ( rcc != null ) {
        //     playerInAirstream = true;
        //     _rcc = rcc;
        //     switch (rcc.GetPlayerState())
        //     pConSave = pCon;
        //     if (PlayerController.GetPlayerState() != PlayerController.ePlayerState.JUMPING || PlayerController.GetPlayerState() != PlayerController.ePlayerState.DIVING)
        //     {
        //         pCon.ForceJumpState(delayBeforeNextGroundCheck: .5f);
        //     }
        // }
        
        PlayerController pCon = other.GetComponent<PlayerController>();
        if (pCon != null)
        {
            playerInAirstream = true;
            pConSave = pCon;
            if (PlayerController.GetPlayerState() != PlayerController.ePlayerState.JUMPING || PlayerController.GetPlayerState() != PlayerController.ePlayerState.DIVING)
            {
                pCon.ForceJumpState(delayBeforeNextGroundCheck: .5f);
            }
        }
    }

    public void CheckAndRemovePlayerFromAirstream(Collider other)
    {
        PlayerController pCon = other.GetComponent<PlayerController>();
        if (pCon != null)
        {
            playerInAirstream = false;
        }
    }
    #endregion

    #region Getters / Setters
    public eFanMode GetFanMode()
    {
        return fanMode;
    }
    #endregion
}

#if UNITY_EDITOR
[CustomEditor(typeof(Fan))]
public class FanEditor: Editor
{
    private SerializedProperty modulationDurationSerialized;
    private SerializedProperty timerDurationSerialized;

    private Fan myFan;

    private void OnEnable()
    {
        myFan = (Fan)target;

        modulationDurationSerialized = serializedObject.FindProperty("modulationDuration");
        timerDurationSerialized = serializedObject.FindProperty("timerDuration");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        Fan.eFanMode mode = myFan.GetFanMode();

        switch (mode)
        {
            case Fan.eFanMode.MODULATING:
                modulationDurationSerialized.floatValue = EditorGUILayout.FloatField("Modulation Duration", modulationDurationSerialized.floatValue);
                break;
            case Fan.eFanMode.BUTTONACTIVATEDTIMER:
                timerDurationSerialized.floatValue = EditorGUILayout.FloatField("Timer Duration", timerDurationSerialized.floatValue);
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif
