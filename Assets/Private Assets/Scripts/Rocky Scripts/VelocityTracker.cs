using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityTracker : MonoBehaviour {
    [Header("Dynamic")]
    public Transform   trackedTransform;
    public bool        tracking = false;
    public List<Datum> data;
    
    private Vector3     lastPos;
    private float       startTime, duration;

    [System.Serializable]
    public struct Datum {
        public float   t;
        public Vector3 pos;
        public Vector3 vel;
        public float   speed;

        public override string ToString() {
            return $"{t:0.000}\t\t{pos.x:0.000}\t{pos.y:0.000}\t{pos.z:0.000}\t\t{vel.x:0.000}\t{vel.y:0.000}\t{vel.z:0.000}\t\t{speed:0.000}";
        }
    }

    public void StartTracking( Transform _t, float _duration ) {
        if (tracking) StopTracking();
        
        startTime = Time.fixedTime;
        duration = _duration;
        trackedTransform = _t;
        data.Clear();
        lastPos = _t.position;
        tracking = true;
    }

    private void FixedUpdate() {
        if ( !tracking ) return;
        Datum d = new Datum();
        d.t = Time.fixedTime;
        d.pos = trackedTransform.position;
        Vector3 delta = d.pos - lastPos;
        d.vel = delta / Time.fixedDeltaTime;
        d.speed = d.vel.magnitude;
        data.Add( d );
        lastPos = trackedTransform.position;

        if ( d.t >= startTime + duration ) {
            StopTracking();
        }
    }

    public void StopTracking() {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine( $"Tracking of {trackedTransform.name} at {startTime}:" );
        foreach ( Datum d in data ) {
            sb.AppendLine( d.ToString() );
        }

        Debug.LogWarning( sb.ToString() );
        tracking = false;
    }
}