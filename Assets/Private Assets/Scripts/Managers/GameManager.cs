using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject debugObject;

    public static GameManager s;

    private WorldUISpawner worldUI;

    public static Dictionary<Trash, TrashLover> dictClaimedTrash;

    private void Awake()
    {
        if (s != this)
        {
            if (s != null)
            {
            Debug.Log("Extra game manager detected, deleting it.");
            Destroy(s);
            }
            s = this;
        }

        worldUI = GetComponent<WorldUISpawner>();

        if (dictClaimedTrash == null)
        {
            dictClaimedTrash = new Dictionary<Trash, TrashLover>();
        }
    }

    public void SpawnWorldUI(Vector3 position, float lifetime)
    {
        worldUI.SpawnWorldUIObject(position, lifetime);
    }

    public void SpawnDebugGameObject(Vector3 debugObjectPosition)
    {
        GameObject gO = GameObject.Instantiate<GameObject>(debugObject);
        gO.transform.position = debugObjectPosition;
    }
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif
