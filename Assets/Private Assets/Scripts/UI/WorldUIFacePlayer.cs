using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUIFacePlayer : MonoBehaviour
{
    private float rotateSpeed = 5;
    private Transform cameraTransform;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotationSave = transform.rotation;

        transform.LookAt(cameraTransform);
        transform.Rotate(0, 180, 0);

        Quaternion targetRotation = transform.rotation;

        transform.rotation = Quaternion.Lerp(rotationSave, targetRotation, rotateSpeed * Time.deltaTime);
    }
}
