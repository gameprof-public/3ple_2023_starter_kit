using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Test_SwitchCameraPOI : MonoBehaviour {
    public List<Transform> transforms;
    private int nextTransform = 0;

    private Rect buttonPos = new Rect( 10, 10, 200, 32 );
    private CinemachineFreeLook cfl;

    void Awake() {
        cfl = GetComponent<CinemachineFreeLook>();
    }

    void OnGUI() {
        if (GUI.Button(buttonPos, $"POI to {transforms[nextTransform].name}")) {
            cfl.Follow = transforms[nextTransform];
            cfl.LookAt = transforms[nextTransform];
            nextTransform = ( nextTransform + 1 ) % transforms.Count;
        }
    }
}
