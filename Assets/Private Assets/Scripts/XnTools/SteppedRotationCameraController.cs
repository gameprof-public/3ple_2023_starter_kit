using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using XnTools;

public class SteppedRotationCameraController : MonoBehaviour {
    public Transform pointOfInterest;
    public Transform lookAt;
    public Transform camTrans;
    public int camRotSteps = 8;
    public float camDist = 16;
    public float camDistPercent = 0.75f;
    public float camElevationDegrees = 30;
    public int camElevationSteps = 9;
    public float easingU = 0.1f;
    public float easingRotU = 0.2f;
    public bool invertRotation = false;

    public enum eUpdateMode { Update, FixedUpdate };
    public eUpdateMode updateMode = eUpdateMode.FixedUpdate;
    
    // TODO: Implement lookAhead for this camera
    //public float lookAhead = 0;
    //private Rigidbody poiRigid;
    private float rotStepDeg;
    private int currRotStep;
    private float currRot;
    float toCamDist;
    float currCamDist;
    float elevationStepDeg;
    int currCamElevationStep;
    float currCamElevationDeg;
    float lookAtTargetYMult; // The multiple of camDist that LookAt is above the pointOfInterest
    
    void Start() {
        transform.position = pointOfInterest.position;
        rotStepDeg = 360f / camRotSteps;
        currRotStep = 0;

        currCamElevationDeg = camElevationDegrees;
        elevationStepDeg = 90f / camElevationSteps;
        currCamElevationStep = Mathf.RoundToInt( currCamElevationDeg / elevationStepDeg );

        toCamDist = currCamDist = camDist;
        lookAtTargetYMult = lookAt.localPosition.y / camDist;
        
        transform.rotation = Quaternion.Euler( camElevationDegrees, currRot, 0 );
        camTrans.localPosition = new Vector3(0, 0, -camDist);
    }

    void Update() {
        if ( updateMode == eUpdateMode.Update ) {
            // Make easing time-based
            float eU = easingU * ( Time.deltaTime / Time.fixedDeltaTime );
            float eRotU = easingRotU * ( Time.deltaTime / Time.fixedDeltaTime );

            UpdateLoop( eU, eRotU );
        }
    }

    void FixedUpdate() {
        if (updateMode == eUpdateMode.FixedUpdate) UpdateLoop( easingU, easingRotU );
    }

    void UpdateLoop(float eU, float eRotU) {
        // Position easing
        transform.position = ( 1 - eU ) * transform.position + eU * pointOfInterest.position;

        // Distance easing
        camTrans.localPosition = Vector3.Lerp( camTrans.localPosition, new Vector3( 0, 0, -toCamDist ), eU );
        // lookAt Y easing
        lookAt.localPosition = Vector3.Lerp(lookAt.localPosition, new Vector3(0, toCamDist * lookAtTargetYMult), eU);
        
        // Rotation easing
        // Elevation easing
        currCamElevationDeg = currCamElevationDeg.LerpTo(currCamElevationStep * elevationStepDeg, eRotU);
        // Yaw easing
        currRot = currRot.LerpTo( currRotStep * rotStepDeg, eRotU );
        transform.rotation = Quaternion.Euler( currCamElevationDeg, currRot, 0 );
        
        camTrans.LookAt(lookAt, Vector3.up);
    }

    const   float REQ_AMT               = 0.8f;
    private float lastLookRotTime       = -1000;
    public  float delayBetweenLookSteps = 0.25f;
    void OnLook( InputValue value ) {
        Vector2 v2 = value.Get<Vector2>();
        if ( invertRotation ) v2.x = -v2.x;
        if ( Time.time - lastLookRotTime < delayBetweenLookSteps ) return;
        if (v2.x > REQ_AMT ) {
            currRotStep++;
            if (currRotStep >= camRotSteps) {
                currRotStep %= camRotSteps;
                currRot -= 360;
            }
            lastLookRotTime = Time.time;
        } else if (v2.x < -REQ_AMT ) {
            currRotStep--;
            if (currRotStep < 0) {
                currRotStep += camRotSteps;
                currRot += 360;
            }
            lastLookRotTime = Time.time;
        }
    }

    void OnLookMod(InputValue value) {
        Vector2 v2 = value.Get<Vector2>();
        // x controls dist
        if (v2.x > REQ_AMT) {
            if (toCamDist > 1) toCamDist *= camDistPercent;
        } else if (v2.x < -REQ_AMT) {
            if (toCamDist < 100) toCamDist /= camDistPercent;
        }
        // y controls elevation
        if (v2.y > REQ_AMT) {
            if (currCamElevationStep < camElevationSteps - 1) currCamElevationStep++;
        } else if (v2.y < -REQ_AMT) {
            if (currCamElevationStep > 0) currCamElevationStep--;
        }
    }

    public  float   pixelsPerCamRotationStep = 400;
    private Vector2 baseMousePosition;
    private int     baseStep;
    void OnMousePosition( InputValue inVal ) {
        Vector2 v2 = inVal.Get<Vector2>();
        if ( rightButtonHeld ) {
            Vector2 delta = v2 - baseMousePosition;
            delta.x /= pixelsPerCamRotationStep;
            int stepDelta = (int) delta.x;
            currRotStep = baseStep + stepDelta;
        } else {
            baseMousePosition = v2;
        }
    }

    private bool rightButtonHeld = false;

    void OnMouseRightButtonDown( InputValue inVal ) {
        // Debug.Log( "OnMouseRightButtonDown" );
        rightButtonHeld = true;
        baseStep = currRotStep;
    }

    void OnMouseRightButtonUp( InputValue inVal ) {
        // Debug.Log( "OnMouseRightButtonUp" );
        rightButtonHeld = false;
    }
}
