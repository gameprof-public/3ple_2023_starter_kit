using System.Collections;
using System.Collections.Generic;
using System.Text;
using KinematicCharacterController;
using UnityEngine;
using XnTools;


// TODO: Change this to eCharacterState and move it inside RockyCharacterController. - JGB 2022-10-08
public enum CharacterState { Default, NoClip }

// [RequireComponent(typeof(PlayerController))]
[RequireComponent( typeof( RockyAnimationController ) )]
public class RockyCharacterController : MonoBehaviour, ICharacterController {
    static private RockyCharacterController _S;
    static public bool DEBUG_LAND_AND_LEAVE = false;
    static public List<Collider> PLAYER_COLLIDERS = new List<Collider>();
    static public RockyAnimationController RAC;

    public enum eJumpState {
        grounded, up, down, groundPound,
        crouchJump, longJump,
        bounceThisFrame, bounceUp, bounceDown, // bounce is like up, except releasing the jump button does not change it to down - JGB 2022-10-27
        highJump
    }; // TODO: Implement highJump - JGB 2022-10-09


    [Header( "Inscribed" )]
    public SO_KinematicCharacterControllerSettings rockyKCCSettings;
    public KinematicCharacterMotor Motor;
    public Transform MeshRoot;
    public GameObject prefabGroundPoundVFX;
    public List<Collider> IgnoredColliders = new List<Collider>();

    [Header( "Dynamic" )]
    [KinematicCharacterController.ReadOnly]
    [SerializeField]
    private eJumpState _jumpState = eJumpState.grounded;

    public eJumpState jumpState {
        get { return _jumpState; }
        set {
            _jumpState = value;
            _jumpStateStart = Time.time;
        }
    }

    private float _jumpStateStart = -1000;

    private bool _forceUngroundNextUpdate = false;
    private float _forceUngroundCheckDelay = 0;

    //[KinematicCharacterController.ReadOnly] [SerializeField]
    //private CharacterState _currentCharacterState = CharacterState.Default;

    public CharacterState CurrentCharacterState { get; private set; }

    [KinematicCharacterController.ReadOnly]
    [SerializeField]
    private Vector3 BaseVelocity;

    private Vector3 _moveInputVector;
    private Vector3 _lookInputVector;
    private bool _jumpRequested = false;
    private bool _jumpConsumed = false;
    private bool _jumpedThisFrame = false;
    private float _timeSinceJumpRequested = Mathf.Infinity;
    private float _timeSinceLastAbleToJump = 0f;
    private bool _doubleJumpConsumed = false;
    private bool _canWallJump = false;
    private Vector3 _wallJumpNormal = Vector3.zero;
    private Collider _wallJumpCollider = null;
    private Vector3 _wallJumpHitPoint;
    private Vector3 _wallJumpHitCharacterPosition;
    private Vector3 _internalVelocityAdd = Vector3.zero;
    // private bool     _zeroVelocityBeforeInternalVelocityAdd = false;
    private eAxes _zeroAxesBeforeVelAdd = eAxes.none;
    private bool _shouldBeCrouching = false;
    private bool _isCrouchingField = false;
    private float _isCrouchingStart = -1000;
    private Vector3 _jumpDir; // longJump or highJump locked direction

    private bool _isCrouching {
        get { return _isCrouchingField; }
        set {
            if ( value ) _isCrouchingStart = Time.time;
            _isCrouchingField = value;
        }
    }
    private Collider[] _probedColliders = new Collider[8];
    private RockyCharacterInputs inputsThisFrame;

    RockyAnimationController animCtrl;

    private VelocityTracker velTrack;

    // Start is called before the first frame update
    void Awake() {
        //Assign singleton
        if ( _S != this && _S != null ) {
            string errStr = "There is more than one player character in the scene, this needs to be fixed.\n";
            errStr += $"Prior RCC: {_S.gameObject.name}\tThis RCC:{gameObject.name}";
            Debug.LogError( errStr );
        }
        _S = this;

        PLAYER_COLLIDERS = new List<Collider>( GetComponentsInChildren<Collider>() );

        Motor.CharacterController = this;
        RAC = animCtrl = GetComponent<RockyAnimationController>();
        animCtrl?.InitializeAnimationController( rockyKCCSettings.CrawlSpeed, 1, rockyKCCSettings.MaxStableMoveSpeed * 0.5f );

        velTrack = GetComponent<VelocityTracker>();

        // Handle initial state
        TransitionToState( CharacterState.Default );
    }

    /// <summary>
    /// Handles movement state transitions and enter/exit callbacks
    /// </summary>
    public void TransitionToState( CharacterState newState ) {
        CharacterState tmpInitialState = CurrentCharacterState;
        OnStateExit( tmpInitialState, newState );
        CurrentCharacterState = newState;
        OnStateEnter( newState, tmpInitialState );
    }

    /// <summary>
    /// Event when entering a state
    /// </summary>
    public void OnStateEnter( CharacterState state, CharacterState fromState ) {
        switch ( state ) {
        case CharacterState.Default:
            break;
        case CharacterState.NoClip:
            Motor.SetCapsuleCollisionsActivation( false );
            Motor.SetMovementCollisionsSolvingActivation( false );
            Motor.SetGroundSolvingActivation( false );
            break;
        }
    }

    /// <summary>
    /// Event when exiting a state
    /// </summary>
    public void OnStateExit( CharacterState state, CharacterState toState ) {
        switch ( state ) {
        case CharacterState.Default:
            break;
        case CharacterState.NoClip:
            Motor.SetCapsuleCollisionsActivation( true );
            Motor.SetMovementCollisionsSolvingActivation( true );
            Motor.SetGroundSolvingActivation( true );
            break;

        }
    }

    /// <summary>
    /// This is called every frame by RockyPlayerInput in order to tell the character what its inputs are
    /// </summary>
    public void SetInputs( ref RockyCharacterInputs inputs ) {
        // Handle state transition from input
        if ( inputs.NoClip.down ) {
            TransitionToState( CurrentCharacterState == CharacterState.NoClip
                ? CharacterState.Default
                : CharacterState.NoClip );
        }

        // Clamp input
        Vector3 moveInputVector = Vector3.ClampMagnitude( new Vector3( inputs.MoveAxisRight, 0f, inputs.MoveAxisForward ), 1f );

        // Calculate camera direction and rotation on the character plane
        Vector3 cameraPlanarDirection = Vector3.ProjectOnPlane( inputs.CameraRotation * Vector3.forward, Motor.CharacterUp ).normalized;
        if ( cameraPlanarDirection.sqrMagnitude == 0f ) {
            cameraPlanarDirection = Vector3.ProjectOnPlane( inputs.CameraRotation * Vector3.up, Motor.CharacterUp ).normalized;
        }
        Quaternion cameraPlanarRotation = Quaternion.LookRotation( cameraPlanarDirection, Motor.CharacterUp );

        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
            // Move and look inputs
            _moveInputVector = cameraPlanarRotation * moveInputVector;
            _lookInputVector = cameraPlanarDirection;

            // Jumping input
            if ( inputs.Jump.down ) {
                _timeSinceJumpRequested = 0f;
                _jumpRequested = true;
            }

            // Crouching input
            if ( inputs.Crouch.down ) {
                switch ( jumpState ) {
                case eJumpState.up:
                case eJumpState.down:
                case eJumpState.bounceUp:
                case eJumpState.bounceDown:
                    if ( !_canWallJump ) {
                        // If Rocky is jumping, not in a wall slide, and Crouch is down: Ground Pound
                        jumpState = eJumpState.groundPound;
                    }
                    break;
                case eJumpState.grounded:
                    _shouldBeCrouching = true;
                    if ( !_isCrouching ) {
                        _isCrouching = true;
                        Motor.SetCapsuleDimensions( 0.5f, 1f, 0.5f );
                        // MeshRoot.localScale = new Vector3(1f, 0.5f, 1f); // Removed because the crouch animation does this. JGB 2022-10-09
                    }
                    break;
                }
            } else if ( inputs.Crouch.up || ( _isCrouching && inputs.Jump.down ) ) { // Jump can also cause crouching to release
                if ( inputs.Jump.down ) {
                    if ( Time.time - _isCrouchingStart < rockyKCCSettings.LongJumpCrouchTimeLimit ) {
                        jumpState = eJumpState.crouchJump; // Could be longJump or highJump
                        _shouldBeCrouching = false;
                    }
                } else {
                    _shouldBeCrouching = false;
                }
            }
            break;

        case CharacterState.NoClip:
            _moveInputVector = inputs.CameraRotation * moveInputVector;
            _lookInputVector = cameraPlanarDirection;
            break;

        }

        inputsThisFrame = inputs; // Cache a copy of the inputs to be used elsewhere (e.g., telling Jump state)
    }

    #region KCC Update Methods
    /// <summary>
    /// (Called by KinematicCharacterMotor during its update cycle)
    /// This is called before the character begins its movement update
    /// </summary>
    public void BeforeCharacterUpdate( float deltaTime ) {
        // This is called before the motor does anything
        // Manage teleporting to SpawnPoints
        if ( inputsThisFrame.RespawnAt > 0 ) {
            StartPointManager.s.RespawnAtNodeIndex( inputsThisFrame.RespawnAt - 1 );
            inputsThisFrame.RespawnAt = 0;
        }
    }

    /// <summary>
    /// (Called by KinematicCharacterMotor during its update cycle)
    /// This is called when the motor wants to know what its rotation should be right now
    /// This is where you tell your character what its rotation should be right now. 
    /// This is the ONLY place where you should set the character's rotation
    /// </summary>
    public void UpdateRotation( ref Quaternion currentRotation, float deltaTime ) {
        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
        case CharacterState.NoClip:
        default:
            if ( _lookInputVector != Vector3.zero && rockyKCCSettings.OrientationSharpness > 0f ) {
                // // Smoothly interpolate from current to target look direction
                // Vector3 smoothedLookInputDirection = Vector3.Slerp(Motor.CharacterForward, _lookInputVector,
                //     1 - Mathf.Exp(-rockyKCCSettings.OrientationSharpness * deltaTime)).normalized;

                // Orient in the direction of movement input
                Vector3 smoothedLookInputDirection = Vector3.Slerp( Motor.CharacterForward, _moveInputVector,
                    1 - Mathf.Exp( -rockyKCCSettings.OrientationSharpness * deltaTime ) ).normalized;

                // Set the current rotation (which will be used by the KinematicCharacterMotor)
                currentRotation = Quaternion.LookRotation( smoothedLookInputDirection, Motor.CharacterUp );
            }

            if ( rockyKCCSettings.OrientTowardsGravity ) {
                // Rotate from current up to invert gravity
                currentRotation =
                    Quaternion.FromToRotation( ( currentRotation * Vector3.up ), -rockyKCCSettings.Gravity ) *
                    currentRotation;
            }
            break;
        }
    }

    /// <summary>
    /// (Called by KinematicCharacterMotor during its update cycle)
    /// This is called when the motor wants to know what its velocity should be right now
    /// This is where you tell your character what its velocity should be right now. 
    /// This is the ONLY place where you can set the character's velocity
    /// </summary>
    public void UpdateVelocity( ref Vector3 currentVelocity, float deltaTime ) {
        Vector3 targetMovementVelocity = Vector3.zero;

        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
            if ( Motor.GroundingStatus.IsStableOnGround ) {
                // Reorient source velocity on current ground slope (this is because we don't want our smoothing to cause any velocity losses in slope changes)
                currentVelocity =
                    Motor.GetDirectionTangentToSurface( currentVelocity, Motor.GroundingStatus.GroundNormal ) *
                    currentVelocity.magnitude;

                // Calculate target velocity
                Vector3 inputRight = Vector3.Cross( _moveInputVector, Motor.CharacterUp );
                Vector3 reorientedInput = Vector3.Cross( Motor.GroundingStatus.GroundNormal, inputRight ).normalized *
                                          _moveInputVector.magnitude;

                if ( _isCrouching ) {
                    targetMovementVelocity = reorientedInput * rockyKCCSettings.CrawlSpeed;
                } else {
                    targetMovementVelocity = reorientedInput * rockyKCCSettings.MaxStableMoveSpeed;
                }

                // Smooth movement Velocity
                currentVelocity = Vector3.Lerp( currentVelocity, targetMovementVelocity,
                    1 - Mathf.Exp( -rockyKCCSettings.StableMovementSharpness * deltaTime ) );
            } else {
                // Add move input
                if ( _moveInputVector.sqrMagnitude > 0f ) {
                    targetMovementVelocity = _moveInputVector * rockyKCCSettings.MaxAirMoveSpeed;

                    // Prevent climbing on un-stable slopes with air movement
                    if ( Motor.GroundingStatus.FoundAnyGround ) {
                        Vector3 perpenticularObstructionNormal = Vector3
                            .Cross( Vector3.Cross( Motor.CharacterUp, Motor.GroundingStatus.GroundNormal ),
                                Motor.CharacterUp ).normalized;
                        targetMovementVelocity =
                            Vector3.ProjectOnPlane( targetMovementVelocity, perpenticularObstructionNormal );
                    }

                    Vector3 velocityDiff = Vector3.ProjectOnPlane( targetMovementVelocity - currentVelocity,
                        rockyKCCSettings.Gravity );
                    currentVelocity += velocityDiff * ( rockyKCCSettings.AirAccelerationSpeed * deltaTime );
                }

                // Gravity
                // currentVelocity += rockyKCCSettings.Gravity * deltaTime;
                Vector3 gravityDir = rockyKCCSettings.Gravity.normalized;
                switch ( jumpState ) {
                case eJumpState.grounded:
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityUp * deltaTime );
                    break;
                case eJumpState.up:
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityUp * deltaTime );
                    // When the jump has reached/passed its peak, the V3.Dot(currentVelocity, gravityDir) will be >= 0 
                    if ( !inputsThisFrame.Jump || ( Vector3.Dot( currentVelocity, gravityDir ) >= 0 ) )
                        jumpState = eJumpState.down;
                    break;
                case eJumpState.bounceThisFrame: // bounceThisFrame is when should bounce this frame. Also sets whether bounceUp or bounceDown
                    if ( inputsThisFrame.Jump ) {
                        jumpState = eJumpState.up;
                    } else {
                        jumpState = eJumpState.down;
                    }
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityUp * deltaTime );
                    break;
                case eJumpState.bounceUp:
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityUp * deltaTime );
                    // When the jump has reached/passed its peak, the V3.Dot(currentVelocity, gravityDir) will be >= 0 
                    if ( Vector3.Dot( currentVelocity, gravityDir ) >= 0 ) jumpState = eJumpState.down;
                    break;
                case eJumpState.bounceDown: // bounceDown is when we have greater gravity because jump was NOT held when bounced
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityDown * deltaTime );
                    // When the jump has reached/passed its peak, the V3.Dot(currentVelocity, gravityDir) will be >= 0 
                    if ( Vector3.Dot( currentVelocity, gravityDir ) >= 0 ) jumpState = eJumpState.down;
                    break;
                case eJumpState.down:
                    currentVelocity += gravityDir * ( rockyKCCSettings.gravityDown * deltaTime );
                    break;
                case eJumpState.groundPound:
                    if ( Time.time - _jumpStateStart < rockyKCCSettings.groundPoundHoverTime ) {
                        // Initially hover in place
                        currentVelocity = Vector3.zero;
                    } else {
                        // Limit movement to be along the Motor.CharacterUp axis.
                        currentVelocity = Vector3.Project( currentVelocity, Motor.CharacterUp );
                        currentVelocity += gravityDir * ( rockyKCCSettings.gravityGroundPound * deltaTime );
                    }
                    break;
                case eJumpState.crouchJump: // This happens at the beginning of a longJump
                    break;
                case eJumpState.longJump:
                    Vector3 gravVel = Vector3.Project( currentVelocity, Motor.CharacterUp );
                    gravVel += gravityDir * ( rockyKCCSettings.longJumpGravity * deltaTime );
                    _jumpDir = Vector3.ProjectOnPlane( _jumpDir, Motor.CharacterUp ).normalized;
                    currentVelocity = gravVel + ( _jumpDir * rockyKCCSettings.LongJumpMoveSpeed );
                    break;
                }

                // Drag
                currentVelocity *= ( 1f / ( 1f + ( rockyKCCSettings.Drag * deltaTime ) ) );
            }

            // Handle jumping
            {
                // Debug.LogWarning( $"{Time.time:0.000}: {_canWallJump}" );

                if ( _forceUngroundNextUpdate ) {
                    if ( _forceUngroundCheckDelay > 0 ) {
                        Motor.ForceUnground( _forceUngroundCheckDelay );
                    } else {
                        Motor.ForceUnground();
                    }
                    _forceUngroundNextUpdate = false;
                    _forceUngroundCheckDelay = 0;
                }

                _jumpedThisFrame = false;
                _timeSinceJumpRequested += deltaTime;

                if ( _jumpRequested ) {
                    // Handle double jump
                    if ( rockyKCCSettings.AllowDoubleJump ) {
                        if ( _jumpConsumed &&
                            !_doubleJumpConsumed &&
                            ( rockyKCCSettings.AllowJumpingWhenSliding
                                ? !Motor.GroundingStatus.FoundAnyGround
                                : !Motor.GroundingStatus.IsStableOnGround ) ) {
                            Motor.ForceUnground();

                            // Add to the return velocity and reset jump state
                            currentVelocity += ( Motor.CharacterUp * rockyKCCSettings.jumpVelocity ) -
                                               Vector3.Project( currentVelocity, Motor.CharacterUp );
                            _jumpRequested = false;
                            _doubleJumpConsumed = true;
                            _jumpedThisFrame = true;
                            jumpState = eJumpState.up;
                        }
                    }

                    // See if we actually are allowed to jump
                    if ( _canWallJump ||
                        ( !_jumpConsumed &&
                         ( ( rockyKCCSettings.AllowJumpingWhenSliding
                              ? Motor.GroundingStatus.FoundAnyGround
                              : Motor.GroundingStatus.IsStableOnGround ) ||
                          _timeSinceLastAbleToJump <= rockyKCCSettings.JumpPostGroundingGraceTime )
                        )
                       ) {
                        // Calculate jump direction before ungrounding
                        Vector3 jumpDirection = Motor.CharacterUp;

                        if ( _canWallJump ) {
                            jumpDirection =
                                ( _wallJumpNormal +
                                 Motor
                                     .CharacterUp ); // JGB 2022-10-07 Combined wall normal with CharacterUp to make wall jump more valuable
                        } else if ( Motor.GroundingStatus.FoundAnyGround &&
                                   !Motor.GroundingStatus.IsStableOnGround ) {
                            jumpDirection = Motor.GroundingStatus.GroundNormal;
                        }

                        // Makes the character skip ground probing/snapping on its next update. 
                        // If this line weren't here, the character would remain snapped to the ground when trying to jump. Try commenting this line out and see.
                        Motor.ForceUnground();

                        if ( jumpState == eJumpState.crouchJump ) {
                            // This only happens when longJump is requested by jumping while crouching
                            _jumpDir = Vector3.ProjectOnPlane( currentVelocity, Motor.CharacterUp );

                            if ( _jumpDir.magnitude > 0.1f ) {
                                _jumpDir.Normalize();
                                currentVelocity = _jumpDir * rockyKCCSettings.LongJumpMoveSpeed;
                                currentVelocity += Motor.CharacterUp * rockyKCCSettings.longJumpVelocity;
                                _jumpRequested = false;
                                _jumpConsumed = true;
                                _jumpedThisFrame = true;
                                jumpState = eJumpState.longJump;
                            } else {
                                jumpState = eJumpState.grounded;
                            }
                        } else {
                            // Add to the return velocity and reset jump state
                            currentVelocity += ( jumpDirection * rockyKCCSettings.jumpVelocity ) -
                                               Vector3.Project( currentVelocity, Motor.CharacterUp );
                            _jumpRequested = false;
                            _jumpConsumed = true;
                            _jumpedThisFrame = true;
                            jumpState = eJumpState.up;
                        }
                    }
                }

                // Reset wall jump
                // Enabled Rocky to slide down walls. - JGB 2022-10-07
                if ( _canWallJump && _wallJumpCollider != null ) {
                    if ( Motor.CharacterCollisionsOverlap(
                            Motor.TransientPosition,
                            Motor.TransientRotation,
                            new Collider[] { _wallJumpCollider },
                            rockyKCCSettings.WallJumpInflateValue ) == 0 ) {
                        // We're no longer touching the wall!
                        _canWallJump = false;
                        _wallJumpCollider = null;
                    }
                }
            }

            // Take into account additive velocity
            if ( _internalVelocityAdd.sqrMagnitude > 0f ) {
                currentVelocity = currentVelocity.ZeroAxes( _zeroAxesBeforeVelAdd );
                _zeroAxesBeforeVelAdd = eAxes.none;
                // if ( _zeroVelocityBeforeInternalVelocityAdd ) {
                //     _zeroVelocityBeforeInternalVelocityAdd = false;
                //     currentVelocity = Vector3.zero;
                // }
                // If this is upward, force unground
                if ( Vector3.Dot( Motor.CharacterUp, _internalVelocityAdd ) > 1 ) {
                    Motor.ForceUnground();
                }
                currentVelocity += _internalVelocityAdd;
                _internalVelocityAdd = Vector3.zero;
                // Debug.Log($"_internalVelocityAdded: {currentVelocity}"  );
            }

            // Limit fall velocity
            float fallVel = Vector3.Dot( Motor.CharacterUp, currentVelocity );
            if ( fallVel < rockyKCCSettings.maxFallVelocity ) {
                // Need to shorten along the Motor.CharacterUp axis
                float velDelta = fallVel - rockyKCCSettings.maxFallVelocity;
                currentVelocity -= ( Motor.CharacterUp * velDelta );
            }

            break;

        case CharacterState.NoClip:
            float verticalInput = 0f + ( inputsThisFrame.Jump ? 1f : 0f ) + ( inputsThisFrame.Crouch ? -1f : 0f );

            // Smoothly interpolate to target velocity
            targetMovementVelocity = ( _moveInputVector + ( Motor.CharacterUp * verticalInput ) ).normalized * rockyKCCSettings.NoClipMoveSpeed;
            currentVelocity = Vector3.Lerp( currentVelocity, targetMovementVelocity, 1 - Mathf.Exp( -rockyKCCSettings.NoClipSharpness * deltaTime ) );
            // TODO: The following line is a stop-gap for a tiny amount of drop due to gravity when in NoClip state. JGB 2022-10-08
            if ( verticalInput == 0 ) currentVelocity.y = 0;
            break;
        }

        BaseVelocity = currentVelocity;
    }

    /// <summary>
    /// (Called by KinematicCharacterMotor during its update cycle)
    /// This is called after the motor has finished everything in its update
    /// This is called after the character has finished its movement update
    /// </summary>
    public void AfterCharacterUpdate( float deltaTime ) {
        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
            // Handle jump-related values
            {
                // Handle jumping pre-ground grace period
                if ( _jumpRequested && _timeSinceJumpRequested > rockyKCCSettings.JumpPreGroundingGraceTime ) {
                    _jumpRequested = false;
                }

                if ( rockyKCCSettings.AllowJumpingWhenSliding
                        ? Motor.GroundingStatus.FoundAnyGround
                        : Motor.GroundingStatus.IsStableOnGround ) {
                    // If we're on a ground surface, reset jumping values
                    if ( !_jumpedThisFrame ) {
                        _doubleJumpConsumed = false;
                        _jumpConsumed = false;
                    }
                    _timeSinceLastAbleToJump = 0f;
                } else {
                    // Keep track of time since we were last able to jump (for grace period)
                    _timeSinceLastAbleToJump += deltaTime;
                }
            }

            // Handle uncrouching
            if ( _isCrouching && !_shouldBeCrouching ) {
                // Do an overlap test with the character's standing height to see if there are any obstructions
                Motor.SetCapsuleDimensions( 0.5f, 2f, 1f );
                if ( Motor.CharacterCollisionsOverlap(
                        Motor.TransientPosition,
                        Motor.TransientRotation,
                        _probedColliders ) > 0 ) {
                    // If obstructions, just stick to crouching dimensions
                    Motor.SetCapsuleDimensions( 0.5f, 1f, 0.5f );
                } else {
                    // If no obstructions, uncrouch
                    // MeshRoot.localScale = new Vector3(1f, 1f, 1f);
                    _isCrouching = false;
                }
            }
            break;
        }

        // Handle Animation via RockyAnimationController
        if ( animCtrl == null ) return;

        switch ( CurrentCharacterState ) {
        case CharacterState.NoClip:
            animCtrl.UpdateAnimationState( PlayerController.ePlayerState.SLOPESLIDE, false,
                Vector3.positiveInfinity );
            break;
        case CharacterState.Default:
        default:
            Vector3 vel = Motor.BaseVelocity;
            float spd = vel.magnitude;
            float minAnimMoveSpeed = 1; // Minimum speed to move from standing to walking (crouching to crawling)

            // Animation States Used:
            // WALKING 
            // JUMPING      x
            // LONGJUMPING
            // CROUCHING    x
            // CRAWLING     x
            // CLIMBING
            // MANTLE
            // DIVING
            // ROLLING
            // KNOCKBACK
            // GROUNDPOUND  x
            // WALLSLIDE    x
            // SLOPESLIDE
            // HANGING

            if ( _isCrouching ) {
                if ( spd > minAnimMoveSpeed ) {
                    animCtrl.UpdateAnimationState( PlayerController.ePlayerState.CRAWLING, true, vel );
                } else {
                    animCtrl.UpdateAnimationState( PlayerController.ePlayerState.CROUCHING, true, vel );
                }
            } else if ( _canWallJump ) {
                animCtrl.UpdateAnimationState( PlayerController.ePlayerState.WALLSLIDE, false, vel );
                // TODO: Somehow something (not just the wallslide animation) broke wall jumping. - JGB 2022-10-09
                // TODO: Rotate Rocky properly for the WallSlide animation
            } else if ( jumpState == eJumpState.groundPound ) {
                animCtrl.UpdateAnimationState( PlayerController.ePlayerState.GROUNDPOUND, false, vel );
            } else if ( jumpState == eJumpState.longJump ) {
                animCtrl.UpdateAnimationState( PlayerController.ePlayerState.LONGJUMPING, false, vel );
            } else if ( jumpState != eJumpState.grounded ) {
                animCtrl.UpdateAnimationState( PlayerController.ePlayerState.JUMPING, false, vel );
            } else { // Running or standing
                animCtrl.UpdateAnimationState( PlayerController.ePlayerState.WALKING, true, vel );
            }

            RockyDebugText.text = $"{jumpState}\n[ {vel.x:000.0}, {vel.y:000.0}, {vel.z:000.0} ]";
            break;
        }
    }

    /// <summary>
    /// This is called after when the motor wants to know if the collider can be collided with (or if we just go through it)
    /// </summary>
    /// <param name="coll">The collider being checked against the IgnoredColliders List</param>
    /// <returns>true if the Collider coll is valid</returns>
    public bool IsColliderValidForCollisions( Collider coll ) {
        if ( IgnoredColliders.Contains( coll ) ) {
            return false;
        }
        return true;
    }

    public void OnGroundHit( Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport ) {
        // This is called when the motor's ground probing detects a ground hit
    }

    /// <summary>
    /// This is called when the motor's movement logic detects a hit
    /// </summary>
    /// <param name="hitCollider"></param>
    /// <param name="hitNormal"></param>
    /// <param name="hitPoint"></param>
    /// <param name="hitStabilityReport"></param>
    public void OnMovementHit( Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport ) {
        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
            // We can wall jump only if we are not stable on ground and are moving against an obstruction
            if ( rockyKCCSettings.AllowWallJump && !Motor.GroundingStatus.IsStableOnGround &&
                !hitStabilityReport.IsStable ) {
                _canWallJump = true;
                _wallJumpNormal = hitNormal;
                _wallJumpCollider = hitCollider;
                _wallJumpHitPoint = hitPoint;
                _wallJumpHitCharacterPosition = Motor.TransientPosition;
            }
            break;
        }
    }

    public void ProcessHitStabilityReport( Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport hitStabilityReport ) {
        // This is called after every hit detected in the motor, to give you a chance to modify the HitStabilityReport any way you want
    }

    /// <summary>
    /// This is called after the motor has finished its ground probing, but before PhysicsMover/Velocity/etc.... handling
    /// </summary>
    /// <param name="deltaTime"></param>
    public void PostGroundingUpdate( float deltaTime ) {
        // Handle landing and leaving ground
        if ( Motor.GroundingStatus.IsStableOnGround && !Motor.LastGroundingStatus.IsStableOnGround ) {
            // If this was in a Ground Pound, spawn particles
            if ( jumpState == eJumpState.groundPound ) {
                Instantiate<GameObject>( prefabGroundPoundVFX, transform.position, Quaternion.Euler( -90, 0, 0 ) );
            }
            if ( jumpState != eJumpState.bounceThisFrame ) {
                jumpState = eJumpState.grounded;
            }
            _canWallJump = false;
            _wallJumpCollider = null;
            OnLanded();
        } else if ( !Motor.GroundingStatus.IsStableOnGround && Motor.LastGroundingStatus.IsStableOnGround ) {
            OnLeaveStableGround();
        }
    }

    protected void OnLanded() {
        if ( DEBUG_LAND_AND_LEAVE ) Debug.Log( "Landed" );
    }

    protected void OnLeaveStableGround() {
        if ( DEBUG_LAND_AND_LEAVE ) Debug.Log( "Left ground" );
    }

    public void AddVelocity( Vector3 velocity, eAxes zeroAxes = eAxes.none, bool isJump = false ) {
        switch ( CurrentCharacterState ) {
        case CharacterState.Default:
            _zeroAxesBeforeVelAdd |= zeroAxes;
            _internalVelocityAdd += velocity;
            if ( isJump ) {
                //Motor.ForceUnground();
                _forceUngroundNextUpdate = true;
            }
            jumpState = eJumpState.bounceThisFrame;

            // Debug.Log($"_internalVelocityAdd: {_internalVelocityAdd}"  );
            // velTrack.StartTracking(transform, 2);
            break;
        }
    }

    /// <summary>
    /// Increases velocity each frame it is called 
    /// </summary>
    /// <param name="acceleration"></param>
    /// <param name="isJump"></param>
    public void Accelerate( Vector3 acceleration, bool isJump = false ) {
        AddVelocity( acceleration * Time.fixedDeltaTime, eAxes.none, isJump );
    }

    public void OnDiscreteCollisionDetected( Collider hitCollider ) {
        // This is called by the motor when it is detecting a collision that did not result from a "movement hit".
    }
    #endregion


    #region PlayerController Methods that were moved here
    /// <summary>
    /// Used by another class to hard force Rocky into a jumping state
    /// </summary>
    public void ForceJumpState( float delayBeforeNextGroundCheck = .5f ) {
        jumpState = eJumpState.up;
        //Motor.ForceUnground(delayBeforeNextGroundCheck);
        _forceUngroundNextUpdate = true;
        _forceUngroundCheckDelay = delayBeforeNextGroundCheck;

        // playerState = ePlayerState.JUMPING;
        // DisableGroundedAnimationBools();
        // DisableJumpingAnimationBools();
        // anim.CrossFade("Jumping", 0);
        // anim.SetBool("Jumping", true);
        // TemporarilyDisableGroundedCheck(delayBeforeNextGroundCheck);
    }

    public bool IsGrounded() {
        return Motor.GroundingStatus.IsStableOnGround;
    }

    /// <summary>
    /// This function should be called by enemy hitboxes when enemies get jumped on.
    /// </summary>
    public void HandleEnemyBounce() {
        bool jumpBuffered = inputsThisFrame.Jump; // NOTE: This will do a big jump if the Jump key is held at all. Timing doesn't matter.

        float bounceSpeedY = ( jumpBuffered ) ? rockyKCCSettings.largeBounceVelocity : rockyKCCSettings.smallBounceVelocity;
        Vector3 bounceVel = Vector3.up * bounceSpeedY;
        // Will convert to a jump if currently in a ground pound.
        AddVelocity( bounceVel, eAxes.y, jumpBuffered || (jumpState == eJumpState.groundPound) );

        // //If the player is ground pounding, change them to a jumping state
        // if ( jumpState == eJumpState.groundPound ) {
        //     Motor.ForceUnground();
        //     jumpState = eJumpState.bounce;
        // }

        // TODO: Implement HandleEnemyBounce() - JGB 2022-10-12
        /*
        bool jumpBuffered = CheckInputBufferForInput(InputType.PLAYERJUMP);
        if (jumpBuffered)
        {
            //big enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += largeBounceForce;
        }
        else
        {
            //small enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += smallBounceForce;
        }

        //If the player is ground pounding, change them to a jumping state
        if (playerState == ePlayerState.GROUNDPOUND)
        {
            playerState = ePlayerState.JUMPING;
            DisableJumpingAnimationBools();
        }
        */
    }

    public void HandleOtherBounce() {
        HandleOtherBounce( Vector3.zero );
    }
    public void HandleOtherBounce( Vector3 bounceVel ) {
        bool jumpBuffered = inputsThisFrame.Jump; // NOTE: This will do a big jump if the Jump key is held at all. Timing doesn't matter.
        if (bounceVel == Vector3.zero) {
            //float bounceSpeedY = ( jumpBuffered ) ? rockyKCCSettings.largeBounceVelocity : rockyKCCSettings.smallBounceVelocity;
            bounceVel = Vector3.up * rockyKCCSettings.smallBounceVelocity;
        }
        // Will convert to a jump if currently in a ground pound.
        AddVelocity( bounceVel, eAxes.y, jumpBuffered || ( jumpState == eJumpState.groundPound) );
    }

    #endregion

    #region STATIC Methods

    /// <summary>
    /// This uses a cache of all Colliders on RockyCharacterController._S.gameObject or its
    ///  children to speed up checks in various collision methods like OnTriggerEnter().
    /// </summary>
    /// <param name="coll">The Collider that might be part of RCC._S.gameObject</param>
    /// <returns>RockyCharacterController._S or null</returns>
    static public RockyCharacterController GetRCCFromCollider( Collider coll ) {
        if ( PLAYER_COLLIDERS.Contains( coll ) ) return _S;
        return null;
    }

    /// <summary>
    /// This uses a cache of all Colliders on RockyCharacterController._S.gameObject or its
    ///  children to speed up checks in various collision methods like OnTriggerEnter().
    /// </summary>
    /// <param name="coll">The Collider that might be part of RCC._S.gameObject</param>
    /// <returns>true if coll is one of the player's Colliders</returns>
    static public bool IsRCCCollider( Collider coll ) {
        return PLAYER_COLLIDERS.Contains( coll );
    }

    static public void TeleportCharacter( Vector3 targetPosition ) {
        _S.Motor.SetPositionAndRotation( targetPosition, _S.transform.rotation );
    }

    static public void TeleportCharacter( Vector3 targetPosition, Quaternion targetRotation ) {
        _S.Motor.SetPositionAndRotation( targetPosition, targetRotation );
    }

    /// <summary>
    /// Returns true if Rocky is at or above the y position passed in.
    /// </summary>
    static public bool IsAbove( float y ) {
        return ( _S.transform.position.y >= y );
    }



    #endregion

}