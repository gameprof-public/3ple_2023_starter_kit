1. I utilized the red coins and timed platforms together to create platforming challenges. 
At the top of the tallest tower and by one of the lamp posts a button activates the platforms while the red coin ring activates the rest of the challenge.
I used the fans a little bit for vertical movement outside of the towers and for a platforming challenge in the tower. 
More fan utilization than the last version, with a new section added in the area with lamp posts.

2. Still going for an urban setting, with the larger tower being a focal popint. The lower section has turned into an area that could consist of small shops- a small town downtown feeling.
Most mechanical aspects are fans, being the main thing in the level (outside of timed platforms) that can be turned on and off.

3. No new mechanics added or edited.

4. No outside assets used.