using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class Trash : Pickup
{
    // public static float floatHeight = .75f; //How high trash items should float above the ground.

    [SerializeField] private GameObject emptyGraphicsParent;

    private Collider coll;

    private void Start()
    {
        coll = GetComponent<Collider>();

        // transform.position = new Vector3(transform.position.x, (transform.position.y + Trash.floatHeight), transform.position.z);

        //Make sure the trash inventory is initialized.
        if (TrashInventory.s == null)
        {
            TrashInventory tiFound = FindObjectOfType<TrashInventory>();

            if (tiFound != null)
            {
                tiFound.InitializeTrashInventory();
            }
            else
            {
                Debug.LogWarning("Make sure you put a Dumpster prefab in your scene!!!");
            }
        }

        TrashInventory.s.ReportTrashIsInLevel(); //Let the trash inventory know this trash is in the level.
    }

    /// <summary>
    /// This is called on all these items by the Dumpster
    /// </summary>
    /// <param name="graphicsPrefab"></param>
    public void InitializeGraphics(GameObject graphicsPrefab)
    {
        //Delete the in-editor graphics from this object
        int transCount = emptyGraphicsParent.transform.childCount;
        for (int i = 0; i < transCount; i++)
        {
            Destroy(emptyGraphicsParent.transform.GetChild(i).gameObject);
        }

        //Instantiate graphics
        GameObject newGraphics = GameObject.Instantiate<GameObject>(graphicsPrefab);
        newGraphics.transform.SetParent(emptyGraphicsParent.transform);
        newGraphics.transform.localPosition = Vector3.zero;
    }

    protected override void GetPickedUp()
    {
        if ( TrashInventory.s.AddTrashToInventory( this ) ) {
            TrashLover.UnclaimTrash( this ); //Make sure no enemies get weird trying to continue chasing this trash
            base.GetPickedUp();
        }
    }

    public GameObject GetGraphicsEmptyObject()
    {
        return emptyGraphicsParent;
    }

    //Disable colliders while being held
    public void DisableCollider()
    {
        coll.enabled = false;
    }

    public void ReEnableCollider()
    {
        coll.enabled = true;
    }

    /// <summary>
    /// This is just so that this function can be assigned into an AnimationEvent
    /// </summary>
    /// <param name = "transform" ></ param >
    public void ReEnableCollider(Transform transform)
    {
        coll.enabled = true;
    }

    /// <summary>
    /// This can get called by an enemy when trash needs to be dropped
    /// </summary>
    public void GetDropped()
    {

    }

    /// <summary>
    /// This can get called by an enemy when they steal a trash from Grabbie
    /// </summary>
    public void GetStolen()
    {

    }
}
