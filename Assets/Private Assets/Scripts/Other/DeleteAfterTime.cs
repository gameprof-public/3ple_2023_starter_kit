using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAfterTime : MonoBehaviour
{
    public float lifeTime;
    private float timeSpawned;
    private float endOfLifeTime;

    // Start is called before the first frame update
    void Start()
    {
        timeSpawned = Time.time;
        endOfLifeTime = timeSpawned + lifeTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= endOfLifeTime)
        {
            Destroy(gameObject);
        }
    }
}
