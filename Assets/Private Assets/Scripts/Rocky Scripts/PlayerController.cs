#define USE_KINEMATIC_CHARACTER_CONTROLLER

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.VFX;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.ProBuilder;
using UnityEngine.Events;
using UnityEngine.Serialization;
using XnTelemetry;

#if USE_KINEMATIC_CHARACTER_CONTROLLER
[RequireComponent(typeof(RockyCharacterController))]
[RequireComponent(typeof(RockyAnimationController))]
public class PlayerController : MonoBehaviour {
    static private PlayerController         _S;
    static private RockyCharacterController _RCC;
    static private RockyAnimationController _RAC;
    
    static private List<Interactable> CLOSE_INTERACTABLES = new List<Interactable>();

    public enum ePlayerState { WALKING, JUMPING, LONGJUMPING, CROUCHING, JUSTLANDED, CRAWLING, CLIMBING, MANTLE, HANGING, DIVING, ROLLING, KNOCKBACK, GROUNDPOUND, WALLSLIDE, SLOPESLIDE }


    private SkinnedMeshRenderer[] mRends;

    private void Awake() {
        _S = this;
        _RCC = GetComponent<RockyCharacterController>();
        _RAC = GetComponent<RockyAnimationController>();
        mRends = GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    /// <summary>
    /// Used by another class to hard force Rocky into a jumping state
    /// </summary>
    public void ForceJumpState(float delayBeforeNextGroundCheck = .5f) {
        _RCC.ForceJumpState( delayBeforeNextGroundCheck );
    }

    public bool IsGrounded() {
        return _RCC.IsGrounded();
    }
    
    

    static public ePlayerState GetPlayerState() {
        return _RAC.GetCurrentState();
        // return _S.playerState;
    }
    
    static public Vector3 GetPlayerPosition()
    {
        return _S.transform.position;
    }

    /// <summary>
    /// This function should be called by enemy hitboxes when enemies get jumped on.
    /// </summary>
    static public void HandleEnemyBounce() {
        _S.HandleEnemyBounce_NS();
    }
    /// <summary>
    /// This function should be called by enemy hitboxes when enemies get jumped on.
    /// </summary>
    public void HandleEnemyBounce_NS()
    {
        _RCC.HandleEnemyBounce();
        // Implemented HandleEnemyBounce() in RockyCharacterController - JGB 2022-10-27
        /*
        bool jumpBuffered = CheckInputBufferForInput(InputType.PLAYERJUMP);
        if (jumpBuffered)
        {
            //big enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += largeBounceForce;
        }
        else
        {
            //small enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += smallBounceForce;
        }

        //If the player is ground pounding, change them to a jumping state
        if (playerState == ePlayerState.GROUNDPOUND)
        {
            playerState = ePlayerState.JUMPING;
            DisableJumpingAnimationBools();
        }
        */
    }


    static public void KnockbackPlayer( Vector3 knockbackSourcePosition, float knockbackMult = 1,
                                        bool procInvulnerability = true,
                                        bool overrideKnockbackWithNormal = false ) {
        _S.KnockbackPlayer_NS(knockbackSourcePosition, knockbackMult, procInvulnerability, overrideKnockbackWithNormal);
    }
    //private float knockbackForce = 20.5f;
    private void KnockbackPlayer_NS( Vector3 knockbackSourcePosition, float knockbackMult = 1,
                                     bool procInvulnerability = true,
                                     bool overrideKnockbackWithNormal = false ) {

        Vector3 directionAwayFromSource = transform.position - knockbackSourcePosition;

        directionAwayFromSource.Normalize();
        // TODO: Make this not hard coded!
        float knockbackForce = _RCC.rockyKCCSettings.smallBounceVelocity * 2;
        Vector3 force = directionAwayFromSource * (knockbackForce * knockbackMult);
        force.y = knockbackForce;// make sure we always go up in the sky at in a significant way
        _RCC.AddVelocity( force );

        if ( procInvulnerability ) {
            //Make me invulnerable
            invulnerable = true;
            timeInvulnerabilityExpires = Time.time + invulnerabilityDuration;

            //Start mesh flashing for invulnerability
            StartCoroutine( FlashMeshForInvulnerability() );
        }



        // TODO: Implement KnockbackPlayer() - JGB 2022-10-12
        /*
        justHit = true;
        DisableCharacterInput();//Disables input until the player is grounded again, handled in HandleMovement()

        Vector3 directionAwayFromSource = transform.position - knockbackSourcePosition;

        //Try to detect the normal of whatever we hit and use that as the direction of the knockback instead...
        //Could feel better for hitting walls instead of using the above direction, which maybe a frame old and cause weird behavior
        if ( overrideKnockbackWithNormal ) {
            RaycastHit hit;
            Ray r = new Ray( transform.position, transform.forward );
            if ( Physics.Raycast( r, out hit, 2f, groundCheckLayermask ) ) {
                directionAwayFromSource = hit.normal;
            }
        }

        directionAwayFromSource.Normalize();

        //Turn Grabbie to face the knockbacker
        //Vector3 lookAtTarget = new Vector3(knockbackSourcePosition.x, transform.position.y, knockbackSourcePosition.z);
        //transform.LookAt(lookAtTarget);

        Vector3 force = directionAwayFromSource * knockbackForce;
        force.y = knockbackForce;// make sure we always go up in the sky at in a significant way

        force *= knockbackMult;

        //Old application of the force
        //Set velocity to knockback force reletive to the damage source
        //velocity = force;

        //New application of the force
        pkb.AddForce( force );
        playerState = ePlayerState.KNOCKBACK;

        //Zero out my cCon velocity
        velocity = Vector3.zero;

        if ( procInvulnerability ) {
            //Make me invulnerable
            invulnerable = true;
            timeInvulnerabilityExpires = Time.time + invulnerabilityDuration;

            //Start mesh flashing for invulnerability
            StartCoroutine( FlashMeshForInvulnerability() );
        }

        //Handle animation for knockback
        anim.CrossFade( "Knockdown", 0 );
        anim.SetBool( "Knockback", true );
        */
    }

    public bool invulnerable = false;
    private float invulnerabilityDuration = 2f;
    private float timeInvulnerabilityExpires = -100;
    private void FixedUpdate() {
        HandleInvulnerability();
    }

    static public bool CheckIfInvulnerable() {
        return _S.invulnerable;
    }

    private void HandleInvulnerability() {
        if ( invulnerable && Time.time >= timeInvulnerabilityExpires ) {
            invulnerable = false;
        }
    }

    private float blinkEffectDuration = .1f;
    private IEnumerator FlashMeshForInvulnerability() {
        float t = 0;
        bool mRendsEnabled = true;
        while ( invulnerable ) {
            t += Time.deltaTime;

            if ( t >= blinkEffectDuration ) {
                t = 0;
                if ( mRendsEnabled ) {
                    foreach ( SkinnedMeshRenderer mRend in mRends ) {
                        mRend.gameObject.SetActive( false );
                    }
                    //graphicsGameObject.SetActive(false);
                    mRendsEnabled = false;
                } else {
                    foreach ( SkinnedMeshRenderer mRend in mRends ) {
                        mRend.gameObject.SetActive( true );
                    }
                    //graphicsGameObject.SetActive(true);
                    mRendsEnabled = true;
                }
            }
            yield return new WaitForSeconds( Time.deltaTime );
        }

        foreach ( SkinnedMeshRenderer mRend in mRends ) {
            mRend.gameObject.SetActive( true );
        }
        //graphicsGameObject.SetActive(true);
    }



    /// <summary>
    /// Change the player's health. Positive is healing, negative is damage.
    /// </summary>
    /// <param name="healthChange"></param>
    static public void ModifyHealth( int healthChange ) {
        _S.ModifyHealth_NS( healthChange );
    }
    /// <summary>
    /// Change the player's health. Positive is healing, negative is damage.
    /// </summary>
    /// <param name="healthChange"></param>
    private void ModifyHealth_NS( int healthChange ) {
        // TODO: Implement ModifyHealth_NS() - JGB 2022-10-12
        /*
        //Apply the change
        currentHealth += healthChange;

        //Clamp health to max health
        if ( currentHealth >= maxHealth ) currentHealth = maxHealth;

        //Check if deafeated
        if ( currentHealth <= 0 ) PlayerDefeated();

        UpdateHealthUI();
        */
    }
    
    static public void RegisterInteractable(Interactable i)
    {
        CLOSE_INTERACTABLES.Add(i);
    }

    static public void UnregisterInteractable(Interactable i)
    {
        CLOSE_INTERACTABLES.Remove(i);
    }
    
    static public void TeleportCharacter( Vector3 targetPosition ) {
        TeleportCharacter(targetPosition, _S.transform.rotation);
    }
    static public void TeleportCharacter(Vector3 targetPosition, Quaternion targetRotation) {
        RockyCharacterController.TeleportCharacter( targetPosition, targetRotation );
        // _S.cCon.enabled = false;
        // _S.transform.position = targetPosition;
        // _S.transform.rotation = targetRotation;
        // _S.cCon.enabled = true;
    }
    
    /// <summary>
    /// Increases velocity each frame it is called 
    /// </summary>
    /// <param name="acceleration"></param>
    /// <param name="isJump"></param>
    public void Accelerate( Vector3 acceleration, bool isJump=false ) {
        _RCC.Accelerate(acceleration, isJump);
    }

    /// <summary>
    /// Returns true if PlayerController is at or above the y position passed in.
    /// </summary>
    static public bool IsAbove( float y ) {
        return ( _S.transform.position.y >= y );
    }
}

#else

//[RequireComponent(typeof(Telemetry_Cloud))]
[RequireComponent(typeof(PlayerKnockback))]
// [RequireComponent(typeof(PlayerController))]
public class PlayerController : MonoBehaviour
{
    static private PlayerController _S;
    
    static private List<Interactable> CLOSE_INTERACTABLES = new List<Interactable>();
    

    public enum ePlayerState { WALKING, JUMPING, LONGJUMPING, CROUCHING, JUSTLANDED, CRAWLING, CLIMBING, MANTLE, HANGING, DIVING, ROLLING, KNOCKBACK, GROUNDPOUND, WALLSLIDE, SLOPESLIDE }

    private ePlayerState playerState = ePlayerState.WALKING;

    [Header("Input Settings")]
    public InputActionAsset inputActionAsset;
    [Tooltip("If false, will use the PlayerInput component")]
    [SerializeField] private bool useHardcodedInput = false;
    private PlayerInput pInput;

    [Header("Player Health Settings")]
    [SerializeField] private int maxHealth;
    [SerializeField] private Heart[] hearts; //Indexed from left to right in the UI
    private int currentHealth;

    [Header("Variables for Knockback")]
    [SerializeField] private float knockbackForce;
    [FormerlySerializedAs( "invulnerabilityDuraion" )]
    [SerializeField] private float invulnerabilityDuration = 3f;
    [SerializeField] private float blinkEffectDuration = .1f;
    private bool invulnerable = false;
    private float timeInvulnerabilityExpires = -1f;
    private bool justHit = false;
    private PlayerKnockback pkb;

    [Header("Variables for Movement")]
    public float walkSpeed;
    public float gravity = -9.8f;

    public float turnSmoothTime = .1f;
    private float turnSmoothVelocity;
    [SerializeField] private float groundAcceleration = 2f;
    [SerializeField] private float groundDeceleration = 2f;
    [SerializeField] private float airAcceleration = 0;
    [SerializeField] private float airDeceleration = .5f;
    [SerializeField] private float crouchingAcceleration = 0;
    [SerializeField] private float crouchingDeceleration = 5f;
    [Tooltip("The angle at which we are considered to be doing a deceleration")]
    [SerializeField] private float maxTurningAngle = 70f;


    [Header("Variables for jumping")]
    public int numberJumps = 2;
    private bool jumpButtonHeld = false;
    private float timeSinceLanded = 0;
    [Tooltip("This is how long the player remains in the JUSTLANDED state after landing")]
    [SerializeField] private float justLandedDuration = .15f;
    [SerializeField] private float jumpForce = 20f;
    [Tooltip("This value is multiplied to the forward velocity when long jumping.")]
    [SerializeField] private float longJumpForwardMult = 2;
    private bool justLongJumped = false;

    [Header("Variables for enemy bouncing")]
    [SerializeField] private float smallBounceForce = 15f;
    [SerializeField] private float largeBounceForce = 35f;

    [Header("Variables for crouching")]
    public Transform tempAnimationTransform;
    private float crouchingHeight = .75f;
    private float standingHeight;
    private Vector3 crouchingCharacterControllerCenter = new Vector3(0, -0.21f, 0.03f);
    private Vector3 standingCharacterControllerCenter;

    [Header("Variables for crawling")]
    [SerializeField] private float crawlSpeed;
    [SerializeField] private float crawlAcceleration;
    [SerializeField] private float crawlDeceleration;


    [Header("Variables for looking")]
    public Transform playerCameraTransform;

    [Header("Variables for WallSliding")]
    [SerializeField] private float wallSlideGravity = 4f;
    [Tooltip("The amount of time you freeze on a wall after beginning a wall slide.")]
    [SerializeField] private float wallSlideFreezeDuration = 1f;
    private float timeWallSlideBegan = -1;
    [SerializeField] private LayerMask layersToIgnoreForSliding;
    private bool canWallSlide = false; //This is set to true when we're close enough to a wall to wallslide, but we may not begin the slide if we're moving upwards
    //^we only wanna wall slide if the y value of velocity is negative (we've passed the apex of a jump)
    private Collider wallSlideColliderReference; //Save a collider here when it's possible we might wall slide on it later

    [Header("Variables for Wall Jumping")]
    [Tooltip("This value is multiplied by the normal jump force, since wall jumps need to be a bit stronger.")]
    [SerializeField] private float wallJumpForceMult =2.25f;

    [Header("Variables for bonking")]
    [SerializeField] private ParticleSystem bonkParticleSystem;
    [SerializeField] private float minSpeedToBonk = 3f;
    [Tooltip("The percent force of a bonk compared to a normal knockback.")]
    [SerializeField] private float bonkKnockbackMult = .5f;

    [Header("Variables for rolling")]
    [SerializeField] private float rollingDuration = 1.25f;
    private float timeStartedRolling = -1;

    [Header("Variables for animation")]
    [SerializeField] private bool useAnimation;
    [SerializeField] private float speedForFullRun = 2f;
    [SerializeField] private GameObject graphicsGameObject;
    private Animator anim;
    //[SerializeField] private Animator animationController;

    [Header("Variables for VFX")]
    [Tooltip("VFX for crouching slide smoke.")]
    [SerializeField] private GameObject smokeVFXHolderPrefab;
    [Tooltip("An empty transform used to dictate the position of smoke spawn.")]
    [SerializeField] private Transform smokeSpawnLocationTransform;
    [Tooltip("Time between smoke object spawns.")]
    [SerializeField] private float timeBetweenSmokeSpawns;
    private float timeSinceLastSmokeSpawn = 0;



    [Header("Input Buffer Settings")]
    [Tooltip("How long in advance bufferable moves allow a button press")]
    [SerializeField] private float bufferWindow = .5f;
    private List<InputBufferData> buffersToClean;
    private float timeBetweenBufferCleanups = 2f;
    private float timeSinceLastBufferCleanup = 0;

    //Variables for Respawning
    private Vector3 respawnPosition;

    [Header("Settings for Interactables")]
    // private List<Interactable> CLOSE_INTERACTABLES = new List<Interactable>();
    private float timeStoppedByInteractable = 0;
    [SerializeField] private float timePausedToAllowInteraction = .25f;


    [Header("Variables for Climbing")]
    [Tooltip("When disabled, we will try to use materials for climb detection.")]
    [SerializeField] private bool useTagClimbDetection = true;
    [SerializeField] private List<Material> climbableMaterials; //set in inspector
    private List<string> climbableMaterialNames; //This will get the names of the climbable materials, used to compare to found materials
    [SerializeField] private Transform wallSnapPoint;
    [SerializeField] private float climbSpeed = 1f;
    [SerializeField] private float climbAccel = 3f;
    //Variables for limiting how often we need to calculate faces
    private int fixedFramesBetweenClimbChecks = 15;
    private int fixedFramesSinceLastClimbCheck = 0;

    [Header("Variables for Hanging")]
    [SerializeField] private Transform ledgeDetectionOriginTransform;
    [SerializeField] private float ledgeDetectionRayLength = .75f;
    [SerializeField] private Transform grabLocationReferenceTransform;
    [Tooltip("The distance a detected ledge needs to be within (compared to a point in front of Grabbie's head) to ledge grab.")]
    [SerializeField] private float maxLedgeGrabDistance = .25f;

    [Header("Variables for Diving")]
    [SerializeField] private float setHorizontalSpeedOfDive = 10f;
    [SerializeField] private float setVerticalSpeedOfDive = 2f;
    [SerializeField] private float diveDeceleration = .05f;
    [SerializeField] private float diveAcceleration = 0f;
    private bool divePressed = false;

    [Header("Variables for Ground Pounding")]
    [SerializeField] private float groundPoundHangTime = 1.25f;
    [SerializeField] private float groundPoundFallSpeed = 10f;
    private bool groundPoundPressed = false;
    private float timeGroundPoundStarted = -1f;

    [Header("Variables for Slope Sliding")]
    [SerializeField] private float slopeSlideAcceleration = 1f;
    [SerializeField] private float slopeSlideJumpForceMult = 1.75f;
    private float timeJumpedOffSlopSlide = -1; //A variable for storing the last time we jumped off a slope slide, we can use to not instantly slide again after a jump input
    private Vector3 mySlopeNormal;


    [Header("Variables for Grounded Check")]
    [SerializeField] private Transform groundedCheckTransform;
    [SerializeField] private LayerMask groundCheckLayermask;
    private UnityEvent onLanding;
    private bool isGrounded;
    private bool groundCheckEnabled = true;



    [Header("Debug Settings")]
    [SerializeField] private bool SpawnPointTeleportationDebugMode = true;
    [SerializeField] private bool debugPlayerState = true;
    [SerializeField] private GameObject debugStateTextObject;
    private TMP_Text debugStateText;


    //jumping variables
    private int jumpsPrimed = 0;
    private bool jumpPushed = false;

    //Logged Input
    private Vector2 moveInput;
    private bool crouchPressed;

    //Telemetry variable
    Telemetry_Cloud telemetry;

    //references
    private CharacterController cCon;

    //private variables
    private Vector3 velocity = Vector3.zero;
    private List<InputBufferData> inputBufferDatas;
    private SkinnedMeshRenderer[] mRends;


    private bool characterInputEnabled = true;



    private void Awake()
    {
        //Assign singleton
        if (_S != this)
        {
            if (_S != null) Debug.LogError("There is more than one player character in the scene, this needs to be fixed.");
            _S = this;
        }

        cCon = GetComponent<CharacterController>();

        if (playerCameraTransform == null)
        {
            Debug.LogWarning("The player character is missing a reference to its camera, this must be set in the inspector for the game to function.");
        }

        inputBufferDatas = new List<InputBufferData>();
        inputBufferDatas.Clear();
        buffersToClean = new List<InputBufferData>();
        buffersToClean.Clear();

        SetPlayerHealthToMax();

        mRends = GetComponentsInChildren<SkinnedMeshRenderer>();

        //characterStandingHeight = cCon.height;
        //defaultControllerRadius = cCon.radius;
        //defaultControllerY = cCon.center.y;

        pInput = GetComponent<PlayerInput>();

        respawnPosition = transform.position;

        if (debugPlayerState)
        {
            debugStateText = debugStateTextObject.GetComponent<TMP_Text>();
            debugStateTextObject.SetActive(true);
        }

        anim = GetComponentInChildren<Animator>();

        climbableMaterialNames = new List<string>();
        foreach(Material m in climbableMaterials)
        {
            climbableMaterialNames.Add(m.name);
        }

        pkb = GetComponent<PlayerKnockback>();

        onLanding = new UnityEvent();

        telemetry = GetComponent<Telemetry_Cloud>();

        SaveVariablesForCrouchProfiles();
    }

    private void Update()
    {
        //Update debug text if appropriate
        if (debugPlayerState)
        {
            debugStateText.text = playerState.ToString();
        }

        if (characterInputEnabled && useHardcodedInput)
            LogInput();

        HandleClosebyInteractablesTimer();
    }

    private void HandleClosebyInteractablesTimer()
    {
        //Track a timer for allowing interaction with closeby interactables
        if (CLOSE_INTERACTABLES.Count >= 1 && GetVector3SansY(velocity).magnitude <= .05f)
        {
            timeStoppedByInteractable += Time.deltaTime;
        }
        else
        {
            timeStoppedByInteractable = 0; //This timer is checked in HandleMovement to manage the actual interactions
        }
    }

    public void EnableCharacterInput()
    {
        characterInputEnabled = true;
    }

    public void DisableCharacterInput()
    {
        characterInputEnabled = false;
    }

    public bool GetCharacterInputStatus()
    {
        return characterInputEnabled;
    }

    public void EnableCharacterController()
    {
        cCon.enabled = true;
    }

    public void DisableCharacterController()
    {
        cCon.enabled = false;
    }

    //Hard coded input logging
    private void LogInput()
    {
        if (useHardcodedInput)
        {
            moveInput = InputManager.GetInputAxis(InputType.PLAYERMOVE);
            jumpPushed = InputManager.GetInputButton(InputType.PLAYERJUMP);
            crouchPressed = InputManager.GetInputButton(InputType.PLAYERCROUCH);
        }
    }

    #region PlayerInput Functions
    private void OnMove(InputValue value)
    {
        Vector2 moveValue = value.Get<Vector2>();
        //if (moveValue.magnitude > 0.05f)
        //{
        //    SendLastInputDeviceToInputManager(pInput.currentControlScheme);
        //}

        SendLastInputDeviceToInputManager(pInput.currentControlScheme);

        if (!useHardcodedInput)
            moveInput = moveValue;
    }

    //Cinemachine is taking care of this
    //private void OnLookInput (InputValue value)
    //{

    //}

    private void SendLastInputDeviceToInputManager(string scheme)
    {
        InputManager.s.ReportLastInputDevice(scheme);
    }

    private void OnJump(InputValue value)
    {
        SendLastInputDeviceToInputManager(pInput.currentControlScheme);

        if (!useHardcodedInput)
        {
            float jumpValue = value.Get<float>();
            if (jumpValue >= .05f) jumpPushed = true;
            else jumpPushed = false;
        }
    }

    private void OnCrouch(InputValue value)
    {
        SendLastInputDeviceToInputManager(pInput.currentControlScheme);
        if (!useHardcodedInput)
        {
            float crouchValue = value.Get<float>();
            if (crouchValue > .05f) crouchPressed = true;
            else crouchPressed = false;
        }
    }

    private void OnDive(InputValue value)
    {
        SendLastInputDeviceToInputManager(pInput.currentControlScheme);
        if (!useHardcodedInput)
        {
            float diveValue = value.Get<float>();
            if (diveValue > .05f) divePressed = true;
            else divePressed = false;
        }
    }

    private void OnGroundPound(InputValue value)
    {
        SendLastInputDeviceToInputManager(pInput.currentControlScheme);
        if (!useHardcodedInput)
        {
            float poundValue = value.Get<float>();
            if (poundValue > .05f) groundPoundPressed = true;
            else groundPoundPressed = false;
        }
    }
    #endregion

    #region Fixed Update and Movement Stuff
    private void FixedUpdate()
    {
        HandleMovement();

        if (useAnimation)
            HandleAnimation();

        HandleInputBuffer();

        HandleInvulnerability();
    }

    private void HandleInvulnerability()
    {
        if (invulnerable && Time.time >= timeInvulnerabilityExpires)
        {
            invulnerable = false;
        }
    }

    public bool CheckInputBufferForInput(InputType typeToCheck)
    {
        foreach (InputBufferData buff in inputBufferDatas)
        {
            if (buff.inputType == typeToCheck && (Time.time - buff.timeOfInput) < bufferWindow) return true;
        }
        return false;
    }

    private void HandleInputBuffer()
    {
        timeSinceLastBufferCleanup += Time.fixedDeltaTime;

        if (timeSinceLastBufferCleanup >= timeBetweenBufferCleanups)
        {
            CleanupBuffer();
        }
    }

    private void CleanupBuffer()
    {
        float cutoffTime = Time.time - timeBetweenBufferCleanups;
        for (int i =0; i < inputBufferDatas.Count; i++)
        {
            if (inputBufferDatas[i].timeOfInput <= cutoffTime)
            {
                buffersToClean.Add(inputBufferDatas[i]);
            }
        }
        foreach (InputBufferData buff in buffersToClean)
        {
            inputBufferDatas.Remove(buff);
        }
        buffersToClean.Clear();
        timeSinceLastBufferCleanup = 0;
    }

    private void CheckIfGrounded()
    {
        if (!groundCheckEnabled)
        {
            isGrounded = false;
            return;
        } 

        bool startedAirborn = !isGrounded;
        //float lateralRayOffset = .25f;

        //Boxcast Implementation
        RaycastHit hit;
        Vector3 org = transform.position;
        float hEx = cCon.radius;
        Vector3 halfExtents = new Vector3(hEx, hEx, hEx);
        float dist = (standingHeight / 2) - hEx;

        

        if (Physics.BoxCast(org, halfExtents, Vector3.down,out hit, transform.rotation, dist, groundCheckLayermask))
        {
            float groundDot = MathUtility.GetNormalDotProductFromRaycastHit(hit);
            if (groundDot < .75f && groundDot >= 0 && playerState != ePlayerState.SLOPESLIDE)//we hit a non-overhanging slope that's 45 degrees or steeper
            {
                mySlopeNormal = hit.normal;
                BeginSlopeSlide();
            }
            else if (groundDot < .75f && groundDot >= 0)//We're still on a slope, but already sliding
            {
                mySlopeNormal = hit.normal;
            }
            else if (playerState == ePlayerState.SLOPESLIDE)
            {
                EndSlopeSlide();
            }
            else
            {
                isGrounded = true;
            }
            //else
            //{
            //    isGrounded = false;
            //}
        }
        else
        {
            isGrounded = false;
        }

        //Death ray implementation
        //Do a bunch of rays to make sure we don't run into the issue where Grabbie is softlocked when groundpounding the edge of an object.
        //Vector3 origin = groundedCheckTransform.position;
        //Ray centerRay = new Ray(origin, Vector3.down);
        //Ray leftRay = new Ray(origin + (Vector3.left * lateralRayOffset), Vector3.down);
        //Ray frontRay = new Ray(origin + (Vector3.forward * lateralRayOffset), Vector3.down);
        //Ray rightRay = new Ray(origin + (Vector3.right * lateralRayOffset), Vector3.down);
        //Ray backRay = new Ray(origin + (Vector3.back * lateralRayOffset), Vector3.down);
        ////we need diagonal rays too sadly, or maybe a box cast would be better

        //Ray[] allRays = new Ray[] { centerRay, leftRay, frontRay, rightRay, backRay };

        //foreach (Ray r in allRays)
        //{
        //    RaycastHit hit;
        //    if (Physics.Raycast(r, out hit, .2f, groundCheckLayermask))
        //    {
        //        float groundDot = MathUtility.GetNormalDotProductFromRaycastHit(hit);
        //        if (groundDot < .75f && groundDot >= 0 && playerState != ePlayerState.SLOPESLIDE)//we hit a non-overhanging slope that's 45 degrees or steeper
        //        {
        //            mySlopeNormal = hit.normal;
        //            BeginSlopeSlide();
        //        }
        //        else if (groundDot < .75f && groundDot >= 0)//We're still on a slope, but already sliding
        //        {
        //            mySlopeNormal = hit.normal;
        //        }
        //        else if (playerState == ePlayerState.SLOPESLIDE)
        //        {
        //            EndSlopeSlide();
        //        }

        //        isGrounded = true;
        //        break;
        //    }
        //    else
        //    {
        //        isGrounded = false;
        //    }
        //}

        //Play any functions that happen upon landing, then clear them
        if (isGrounded && startedAirborn)
        {
            onLanding.Invoke();
            onLanding.RemoveAllListeners();
        }
    }

    private void TemporarilyDisableGroundedCheck(float duration)
    {
        StartCoroutine(DisableGroundCheckCoroutine(duration));
    }

    private IEnumerator DisableGroundCheckCoroutine(float duration)
    {
        groundCheckEnabled = false;

        yield return new WaitForSeconds(duration);

        groundCheckEnabled = true;
    }

    private void HandleAnimation()
    {
        //Debug.Log(anim.speed.ToString());
        anim.speed = 1;
        if (isGrounded)
        {
            //Make sure we're not playing the knocked down animation:
            anim.SetBool("Knockback", false);
            if (playerState == ePlayerState.CROUCHING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool("Crouching", true);
            }
            else if (playerState == ePlayerState.CRAWLING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool("Crawling", true);
                anim.speed = velocity.magnitude / crawlSpeed;
            }
            else if (playerState == ePlayerState.ROLLING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool("Rolling", true);
            }
            else
            {
                DisableGroundedAnimationBools();
                Vector3 velNoY = GetHorizontalVelocity();
                if (velNoY.magnitude > .05)
                {
                    anim.SetBool("Running", true);
                    float blendFloat = velNoY.magnitude / speedForFullRun;
                    if (blendFloat > 1) blendFloat = 1;
                    anim.SetFloat("RunBlend", blendFloat);
                }
                //else DisableGroundedAnimationBools();
            }
            

            DisableJumpingAnimationBools();
        }
        else //Airborn
        {
            bool jumpBool = anim.GetBool("Jumping");
            bool longJumpBool = anim.GetBool("LongJumping");

            if (playerState == ePlayerState.LONGJUMPING)
            {
                anim.SetBool("LongJumping", true);
                anim.SetBool("Jumping", false);
            }
            else if (playerState == ePlayerState.CLIMBING)
            {
                anim.SetBool("Climbing", true);
                anim.CrossFade("Climbing", 0);
                anim.speed = velocity.magnitude / climbSpeed / Time.deltaTime;
            }
            else if (playerState == ePlayerState.HANGING)
            {
                anim.SetBool("Hanging", true);
            }
            else if (playerState == ePlayerState.MANTLE)
            {
                anim.SetBool("MANTLE", true);
            }
            else
            {
                anim.SetBool("Jumping", true);
            }

            DisableGroundedAnimationBools();
        }
    }
    private void DisableJumpingAnimationBools()
    {
        anim.SetBool("Jumping", false);
        anim.SetBool("LongJumping", false);
        anim.SetBool("Diving", false);
        anim.SetBool("GroundPoundDrop", false);
        anim.SetBool("GroundPoundFlip", false);
        anim.SetBool("WallSlide", false);
        anim.SetBool("Hanging", false);
        anim.SetBool("MANTLE", false);
    }
    private void DisableGroundedAnimationBools()
    {
        anim.SetBool("Running", false);
        anim.SetBool("Crouching", false);
        anim.SetBool("Crawling", false);
        anim.SetBool("Rolling", false);
    }

    private Vector3 GetVector3SansY(Vector3 value)
    {
        Vector3 valNoY = value;
        valNoY.y = 0;
        return valNoY;
    }

    private void HandleInteractableInteraction()
    {
        //This function doesn't do anything now, because interactables just display their information immediately when Grabbie moves near since patch 2022.1.A2.

        //Interaction if appropriate
        //if (playerState == ePlayerState.WALKING && timeStoppedByInteractable >= timePausedToAllowInteraction)
        //{
        //    CLOSE_INTERACTABLES[0].AllowInteraction(); //Set the UI to prompt the player that interaction is possible. This probably shouldn't happen every fixed update in a perfect world...
        //    if (jumpPushed) //Actually interact if they have pushed the button.
        //    {
        //        CLOSE_INTERACTABLES[0].Interact();
        //        timeStoppedByInteractable = 0;
        //        jumpButtonHeld = true;
        //    }
        //}
    }



    private void HandleMovement()
    {
        //Check if Grounded using raycast solution. The cCon solution doesn't really work.
        CheckIfGrounded();

        //check player state
        SetPlayerState();

        //Set player height depending on her state
        SetPlayerHeightProfile();

        if (playerState == ePlayerState.CLIMBING)
        {
            MoveUsingClimbingRules();
            return;
        }

        //Special case for hanging
        if (playerState == ePlayerState.HANGING)
        {
            Debug.Log("I'm still hanging...");
            return;
        }

        //Special case for slope sliding
        if (playerState == ePlayerState.SLOPESLIDE)
        {
            Vector3 slopeDirection = -(Vector3.up - mySlopeNormal * Vector3.Dot(Vector3.up, mySlopeNormal));
            slopeDirection.Normalize();
            velocity += slopeDirection * slopeSlideAcceleration * Time.fixedDeltaTime;
            HandleTurning();
            HandleJumping();

            SpawnSmokeVFX(transform.position - new Vector3(0, cCon.height / 2, 0));

            cCon.Move(velocity * Time.fixedDeltaTime);

            return;
        }

        //Special case for when we're wall sliding
        if (canWallSlide && velocity.y < 0 && !isGrounded && playerState != ePlayerState.WALLSLIDE)
        {
            BeginWallSlide(wallSlideColliderReference.ClosestPointOnBounds(transform.position));
        }

        if (playerState == ePlayerState.WALLSLIDE)
        {
            if (timeWallSlideBegan != -1)
            {
                if ((Time.time - wallSlideFreezeDuration) >= timeWallSlideBegan)
                {
                    timeWallSlideBegan = -1;
                } 
                else
                {
                    //Just chill, we're pausing for a moment
                }
            }
            else
            {
                velocity.y += (wallSlideGravity * Time.fixedDeltaTime);
                SpawnSmokeVFX(wallSnapPoint.position); //Only spawn smoke while moving
            }


            HandleJumping();
            cCon.Move(velocity * Time.fixedDeltaTime);
            return;
        }

        HandleInteractableInteraction();
        

        //Daniel Walker McDonald helped me with this section
        Vector3 targetForwardVel = DirectionAwayFromCamera();
        Vector3 targetRightVel = new Vector3(targetForwardVel.z, 0, -targetForwardVel.x);


        //reset jumps if appropriate
        if (isGrounded) jumpsPrimed = numberJumps;

        //Adjust target velocity based on movement
        if (playerState == ePlayerState.CRAWLING)
        {
            targetForwardVel *= moveInput.y * crawlSpeed;
            targetRightVel *= moveInput.x * crawlSpeed;
        }
        else
        {
            targetForwardVel *= moveInput.y * walkSpeed;
            targetRightVel *= moveInput.x * walkSpeed;
        }

        //Change velocity, but not by more than accel // decel
        float accel = GetAccelerationBasedOnState();
        float decel = GetDeccelerationBasedOnState();

        //Acceleration and deceleration
        //Try to find out if we're accelerating or decelerating.
        bool accelerating = true;
        Vector3 targetVel = (targetForwardVel + targetRightVel) / 2;

        //Adjust target vel if necesarry based on our state.
        targetVel = GetTargetVelBasedOnState(targetVel);

        //Angular decel check //this one seems good
        float angleBetweenVelandTarget = Vector3.Angle(velocity, targetVel);
        if (angleBetweenVelandTarget >= maxTurningAngle || targetVel == Vector3.zero) accelerating = false;

        float accelValue;
        if (accelerating) accelValue = accel;
        else accelValue = decel;

        //We don't want to move the character using target velocities while she is being knocked back
        //We also don't want to really move in the X or Z while ground pounding.
        if (playerState != ePlayerState.KNOCKBACK && playerState != ePlayerState.GROUNDPOUND) 
        {
            velocity.x = ChangeWithCap(velocity.x, targetVel.x, accelValue * Time.deltaTime);
            velocity.z = ChangeWithCap(velocity.z, targetVel.z, accelValue * Time.deltaTime);
        }


        //handle rotating the character to properly face the correct direction
        //Only do this if I wasn't just hit
        if (!justHit)
        {
            HandleTurning();
        }


        //apply gravity
        ApplyGravity();

        //Dive if appropriate
        HandleDiving();

        //Ground Pound if appropriate
        HandleGroundPound();

        //handle jumping
        HandleJumping();


        //Play smoke VFX if appropriate
        ManageSlidingSmokeVFX(velocity);

        //MOVE
        cCon.Move(velocity * Time.fixedDeltaTime);
    }

    private void HandleTurning()
    {
        float targetAngle = 0;
        Vector3 turnReference = velocity;
        turnReference.y = 0;
        if (Mathf.Abs(turnReference.magnitude) > .05)
        { //Borrowed this one from Brakeyes
            targetAngle = Mathf.Atan2(velocity.x, velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }

    private void ApplyGravity()
    {
        if (!isGrounded || !groundCheckEnabled)
        {
            velocity.y += gravity * Time.fixedDeltaTime;
        }
        else
        {
            //only do this if not just hit since we want some vertical knockback to happen
            if (!justHit)
            {
                velocity.y = -.25f;
            }
            else
            {
                justHit = false;
                EnableCharacterInput();  //This was disabled when I got hit
            }
        }
    }

    private void HandleJumping()
    {
        if (jumpPushed)
        {
            InputBufferData jumpBuffer = new InputBufferData();
            jumpBuffer.inputType = InputType.PLAYERJUMP;
            jumpBuffer.timeOfInput = Time.time;
            inputBufferDatas.Add(jumpBuffer);
            //Debug.Log("aaron jump buffer logged.");
        }

        //Return if we're already jumping or diving
        if (playerState == ePlayerState.JUMPING || playerState == ePlayerState.DIVING)
        {
            return;
        }

        //Check if jump button has been released after being pushed
        if (jumpButtonHeld && !jumpPushed)
        {
            jumpButtonHeld = false;
        }

        //Actually jump
        if (jumpPushed && jumpsPrimed > 0 && !jumpButtonHeld)
        {
            //do normal jump or long jump
            switch (playerState)
            {
                //Long Jump has been removed from the game, so this is commented out to stop the move from ever happening.
                //case ePlayerState.CROUCHING:
                //    velocity = new Vector3(velocity.x * longJumpForwardMult, velocity.y, velocity.z * longJumpForwardMult);
                //    velocity.y += longJumpVerticalForce;
                //    justLongJumped = true; //use this to properly set long jump state in SetPlayerState()
                //    playerState = ePlayerState.LONGJUMPING;
                //    CapHorizontalVelocityToMaxLongJumpVelocity();
                //    break;
                case ePlayerState.WALLSLIDE:
                    playerState = ePlayerState.JUMPING;
                    velocity = Vector3.zero; //reset the velocity
                    Vector3 wallJumpDirection = (-transform.forward + transform.up).normalized;
                    velocity += (jumpForce * wallJumpDirection * wallJumpForceMult);

                    anim.SetBool("WallSlide", false);

                    timeWallSlideBegan = -1f; //Reset the freeze timing.

                    break;

                case ePlayerState.SLOPESLIDE:
                    EndSlopeSlide();
                    velocity.y += (jumpForce * slopeSlideJumpForceMult);
                    break;

                default:
                    playerState = ePlayerState.JUMPING;
                    velocity.y += jumpForce;
                    break;
            }
            jumpsPrimed--;
            jumpButtonHeld = true;

            //Don't immediately get grounded again
            TemporarilyDisableGroundedCheck(.25f);

            //Report the actual jump to telemetry
            Telemetry_Cloud.LOG( SO_TelemetrySettings.JUMPSTRING );
        }
    }

    private void HandleGroundPound()
    {
        //Check if it's time to ground pound
        if (playerState != ePlayerState.JUMPING && playerState != ePlayerState.GROUNDPOUND)
        {
            return;
        }
        //If we're just starting the ground pound
        if (playerState != ePlayerState.GROUNDPOUND && groundPoundPressed)
        {
            timeGroundPoundStarted = Time.time;
            playerState = ePlayerState.GROUNDPOUND;

            //TODO subscribe to the on land to play some kind of shockwave thing, or smoke
            onLanding.AddListener(LandFromGroundPound);

            anim.SetBool("GroundPoundFlip", true);
        }

        //At this point if we're still not ground pounding, we don't want to do the rest of this function
        if (playerState != ePlayerState.GROUNDPOUND) return;

        //Default to no velocity
        velocity = Vector3.zero;

        //If we've surpassed the hang time, also set Y velocity to be the ground pound speed.
        if (Time.time - timeGroundPoundStarted >= groundPoundHangTime)
        {
            velocity.y = -groundPoundFallSpeed;
            anim.SetBool("GroundPoundDrop", true);
        }
    }

    private void LandFromGroundPound()
    {
        timeGroundPoundStarted = -1;
        SpawnSmokeVFX();
        anim.SetBool("GroundPoundFlip", false);
        anim.SetBool("GroundPoundDrop", false);
    }

    private void HandleDiving()
    {
        //Check if it's not time to dive
        if (playerState != ePlayerState.JUMPING || !divePressed)
        {
            return;
        }

        //Otherwhise it is time to dive
        playerState = ePlayerState.DIVING;

        //Dive in forward direction version
        //Vector3 newVel = transform.forward;

        //Dive in direction input
        Vector3 newVel = new Vector3(moveInput.x, 0, moveInput.y);
        if (moveInput.magnitude < .05f)
        {
            newVel = graphicsGameObject.transform.forward; // if we aren't inputting movement, just dive forward
        }
        else
        {
            //If there is input we need to interpret it based on the pointing direction of the camera.
            newVel = Camera.main.transform.TransformDirection(newVel);
            newVel.y = 0;
            newVel.Normalize();
        }//end in directional input

        newVel *= setHorizontalSpeedOfDive;
        newVel.y = setVerticalSpeedOfDive;

        velocity = newVel;

        anim.SetBool("Diving", true);

        //Report dive to telemetry
        Telemetry_Cloud.LOG( SO_TelemetrySettings.DIVESTRING );
    }

    /// <summary>
    /// This may need to be changed later if there is some kind of speed enhancing mechanic. This probably wouldn't play nicely with that.
    /// The reason it's here for now is so you can't do two concecutive longjumps and continuously multiply your speed.
    /// </summary>
    private void CapHorizontalVelocityToMaxLongJumpVelocity()
    {
        float maxHorizontalMagnitude = walkSpeed * longJumpForwardMult;

        Vector3 horizontalVel = GetHorizontalVelocity();

        float horizontalMag = horizontalVel.magnitude;

        if (horizontalMag > maxHorizontalMagnitude)
        {
            horizontalVel.Normalize();
            horizontalVel *= maxHorizontalMagnitude;
            velocity = new Vector3(horizontalVel.x, velocity.y, horizontalVel.z);
        }
    }

    private Vector3 GetHorizontalVelocity()
    {
        Vector3 horizontalVel = new Vector3(velocity.x, 0, velocity.z);
        return horizontalVel;
    }

    private Vector3 GetTargetVelBasedOnState(Vector3 targetVel)
    {
        Vector3 newTargetVel = targetVel;

        switch(playerState)
        {
            case ePlayerState.CROUCHING:
            case ePlayerState.GROUNDPOUND:
                newTargetVel = Vector3.zero;
                break;
            //case ePlayerState.CRAWLING:
            //    Vector3 newTargetVelNoY = Vector3.ClampMagnitude(GetVector3SansY(targetVel), crawlSpeed);
            //    newTargetVelNoY.y = targetVel.y;
            //    break;
        }

        return newTargetVel;
    }

    

    private float GetAccelerationBasedOnState()
    {
        float accel = 0;

        switch (playerState)
        {
            case ePlayerState.WALKING:
            case ePlayerState.ROLLING:
            case ePlayerState.JUSTLANDED:
                accel = groundAcceleration;
                break;
            case ePlayerState.JUMPING:
            case ePlayerState.LONGJUMPING:
                accel = airAcceleration;
                break;
            case ePlayerState.CROUCHING:
                accel = crouchingAcceleration;
                break;
            case ePlayerState.CRAWLING:
                accel = crawlAcceleration;
                break;
            case ePlayerState.DIVING:
                accel = diveAcceleration;
                break;
            //case ePlayerState.KNOCKBACK:
            //    accel = 0;
            //    break;
        }

        return accel;
    }

    private float GetDeccelerationBasedOnState()
    {
        float decel = 0;

        switch (playerState)
        {
            case ePlayerState.WALKING:
                decel = groundDeceleration;
                break;
            case ePlayerState.JUMPING:
            case ePlayerState.LONGJUMPING:
            case ePlayerState.JUSTLANDED:
            case ePlayerState.ROLLING:
                decel = airDeceleration;
                break;
            case ePlayerState.CROUCHING:
                decel = crouchingDeceleration;
                break;
            case ePlayerState.CRAWLING:
                decel = crawlDeceleration;
                break;
            case ePlayerState.DIVING:
                decel = diveDeceleration;
                break;
            //case ePlayerState.KNOCKBACK:
            //    decel = 0;
            //    break;
        }
        return decel;
    }

    private void SetPlayerState()
    {
        //These states are handled elsewhere in the code (see grounded check and handlejumping)
        if (playerState == ePlayerState.SLOPESLIDE || playerState == ePlayerState.HANGING)
        {
            return;
        }

        if (isGrounded && groundCheckEnabled)
        {
            //Get reference for crawling check
            Vector3 velNoY = GetVector3SansY(velocity);

            //Special case for when a player is crawling or crouching under a low ceiling
            if (!crouchPressed)
            {
                if (playerState == ePlayerState.CROUCHING || playerState == ePlayerState.CRAWLING)
                {
                    if (SomethingAboveGrabbiesHead())
                    {
                        //if (velNoY.magnitude < .1f) playerState = ePlayerState.CRAWLING;
                        //else playerState = ePlayerState.CROUCHING;
                        playerState = ePlayerState.CRAWLING;
                        return;
                    }
                }
            }

            if (playerState == ePlayerState.DIVING || playerState == ePlayerState.ROLLING)
            {
                //If we just landed from a dive, log the time started rolling
                if (playerState == ePlayerState.DIVING)
                {
                    timeStartedRolling = Time.time;
                }

                //Check if we've been rolling for too long
                if (playerState == ePlayerState.ROLLING && (Time.time >= (timeStartedRolling + rollingDuration)))
                {
                    //Go to crouching if something is right above our head
                    if (SomethingAboveGrabbiesHead())
                    {
                        playerState = ePlayerState.CROUCHING;
                    }
                    else
                    {
                        playerState = ePlayerState.WALKING;
                    }
                }
                else
                {
                    playerState = ePlayerState.ROLLING;
                }
            }
            else if (timeSinceLanded <= justLandedDuration)
            {
                playerState = ePlayerState.JUSTLANDED;
            }
            else if (crouchPressed && velNoY.magnitude < .1f || crouchPressed && playerState == ePlayerState.CRAWLING) playerState = ePlayerState.CRAWLING;
            else if (crouchPressed) playerState = ePlayerState.CROUCHING;
            else 
            {
                playerState = ePlayerState.WALKING;
            }

            timeSinceLanded += Time.fixedDeltaTime;

            justLongJumped = false; //make sure we don't think we're longjumping
        }
        else
        {
            //These state changes are handled elswhere, so back out if they are the current state.
            if (playerState == ePlayerState.KNOCKBACK || playerState == ePlayerState.GROUNDPOUND || playerState == ePlayerState.WALLSLIDE)
            {
                return;
            }

            if (playerState == ePlayerState.HANGING) return; //replace this with actual stuff
            //CheckForLedgeGrabAndChangeStateIfAppropriate(); //First, check if we need to ledge grab
            if (playerState != ePlayerState.CLIMBING) //If we're climbing don't bail out unless the climbing is set to end seperately
            {
                if (justLongJumped)
                {
                    playerState = ePlayerState.LONGJUMPING;
                }
                else if (playerState == ePlayerState.DIVING)
                {
                    //We'll just stay diving
                }
                else
                {
                    playerState = ePlayerState.JUMPING;
                }

                timeSinceLanded = 0;
            }
        }
    }

    private float ChangeWithCap(float startingValue, float targetValue, float maxChange)
    {
        float value = startingValue;

        if (value < targetValue)
        {
            if ((value + maxChange) < targetValue) value = value + maxChange;
            else value = targetValue;
        }
        else
        {
            if ((value - maxChange) > targetValue) value = value - maxChange;
            else value = targetValue;
        }


        return value;
    }


    private Vector3 DirectionAwayFromCamera()
    {
        Vector3 dir = Vector3.zero;

        Vector3 unadjustedDir = transform.position - playerCameraTransform.position;
        unadjustedDir.y = 0;

        dir = Vector3.Normalize(unadjustedDir);

        return dir;
    }
#endregion

    static public ePlayerState GetPlayerState()
    {
        return _S.playerState;
    }

    private void ManageSlidingSmokeVFX(Vector3 veloc)
    {
        Vector3 horizontalVel = new Vector3(veloc.x, 0, veloc.z);

        bool smokeState = false;
        if (playerState == ePlayerState.CROUCHING || playerState == ePlayerState.JUSTLANDED) smokeState = true;

        if (smokeState && horizontalVel.magnitude >= .05f && timeSinceLastSmokeSpawn >= timeBetweenSmokeSpawns)
        {
            timeSinceLastSmokeSpawn = 0;
            SpawnSmokeVFX();
        }
        else if (smokeState && horizontalVel.magnitude >= .05f)
        {
            timeSinceLastSmokeSpawn += Time.fixedDeltaTime;
        }
        else if (!smokeState)
        {
            timeSinceLastSmokeSpawn = 0;
        }
    }

    private GameObject SpawnSmokeVFX(Vector3 smokePosition)
    {
        GameObject smokeGO = GameObject.Instantiate<GameObject>(smokeVFXHolderPrefab);
        smokeGO.transform.position = smokePosition;

        return smokeGO;
    }

    private GameObject SpawnSmokeVFX()
    {
        return SpawnSmokeVFX(smokeSpawnLocationTransform.position);
    }

    static public Vector3 GetPlayerPosition()
    {
        return _S.transform.position;
    }

    /// <summary>
    /// This function should be called by enemy hitboxes when enemies get jumped on.
    /// </summary>
    static public void HandleEnemyBounce() {
        _S.HandleEnemyBounce_NS();
    }
    /// <summary>
    /// This function should be called by enemy hitboxes when enemies get jumped on.
    /// </summary>
    public void HandleEnemyBounce_NS()
    {
        bool jumpBuffered = CheckInputBufferForInput(InputType.PLAYERJUMP);
        if (jumpBuffered)
        {
            //big enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += largeBounceForce;
        }
        else
        {
            //small enemy jump
            if (velocity.y <= 0) velocity.y = 0;
            velocity.y += smallBounceForce;
        }

        //If the player is ground pounding, change them to a jumping state
        if (playerState == ePlayerState.GROUNDPOUND)
        {
            playerState = ePlayerState.JUMPING;
            DisableJumpingAnimationBools();
        }
    }


    static public void KnockbackPlayer( Vector3 knockbackSourcePosition, float knockbackMult = 1,
                                        bool procInvulnerability = true,
                                        bool overrideKnockbackWithNormal = false ) {
        _S.KnockbackPlayer_NS(knockbackSourcePosition, knockbackMult, procInvulnerability, overrideKnockbackWithNormal);
    }

    private void KnockbackPlayer_NS( Vector3 knockbackSourcePosition, float knockbackMult = 1,
                                     bool procInvulnerability = true,
                                     bool overrideKnockbackWithNormal = false ) {
        justHit = true;
        DisableCharacterInput();//Disables input until the player is grounded again, handled in HandleMovement()

        Vector3 directionAwayFromSource = transform.position - knockbackSourcePosition;

        //Try to detect the normal of whatever we hit and use that as the direction of the knockback instead...
        //Could feel better for hitting walls instead of using the above direction, which maybe a frame old and cause weird behavior
        if ( overrideKnockbackWithNormal ) {
            RaycastHit hit;
            Ray r = new Ray( transform.position, transform.forward );
            if ( Physics.Raycast( r, out hit, 2f, groundCheckLayermask ) ) {
                directionAwayFromSource = hit.normal;
            }
        }

        directionAwayFromSource.Normalize();

        //Turn Grabbie to face the knockbacker
        //Vector3 lookAtTarget = new Vector3(knockbackSourcePosition.x, transform.position.y, knockbackSourcePosition.z);
        //transform.LookAt(lookAtTarget);

        Vector3 force = directionAwayFromSource * knockbackForce;
        force.y = knockbackForce;// make sure we always go up in the sky at in a significant way

        force *= knockbackMult;

        //Old application of the force
        //Set velocity to knockback force reletive to the damage source
        //velocity = force;

        //New application of the force
        pkb.AddForce( force );
        playerState = ePlayerState.KNOCKBACK;

        //Zero out my cCon velocity
        velocity = Vector3.zero;

        if ( procInvulnerability ) {
            //Make me invulnerable
            invulnerable = true;
            timeInvulnerabilityExpires = Time.time + invulnerabilityDuration;

            //Start mesh flashing for invulnerability
            StartCoroutine( FlashMeshForInvulnerability() );
        }

        //Handle animation for knockback
        anim.CrossFade( "Knockdown", 0 );
        anim.SetBool( "Knockback", true );
    }


    static public bool CheckIfInvulnerable()
    {
        return _S.invulnerable;
    }

    private IEnumerator FlashMeshForInvulnerability()
    {
        float t = 0;
        bool mRendsEnabled = true;
        while (invulnerable)
        {
            t += Time.deltaTime;

            if (t >= blinkEffectDuration)
            {
                t = 0;
                if (mRendsEnabled)
                {
                    foreach (SkinnedMeshRenderer mRend in mRends)
                    {
                        mRend.gameObject.SetActive(false);
                    }
                    //graphicsGameObject.SetActive(false);
                    mRendsEnabled = false;
                }
                else
                {
                    foreach (SkinnedMeshRenderer mRend in mRends)
                    {
                        mRend.gameObject.SetActive(true);
                    }
                    //graphicsGameObject.SetActive(true);
                    mRendsEnabled = true;
                }
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }

        foreach (SkinnedMeshRenderer mRend in mRends)
        {
            mRend.gameObject.SetActive(true);
        }
        //graphicsGameObject.SetActive(true);
    }

    /// <summary>
    /// Change the player's health. Positive is healing, negative is damage.
    /// </summary>
    /// <param name="healthChange"></param>
    static public void ModifyHealth( int healthChange ) {
        _S.ModifyHealth_NS( healthChange );
    }
    /// <summary>
    /// Change the player's health. Positive is healing, negative is damage.
    /// </summary>
    /// <param name="healthChange"></param>
    private void ModifyHealth_NS( int healthChange ) {
        //Apply the change
        currentHealth += healthChange;

        //Clamp health to max health
        if ( currentHealth >= maxHealth ) currentHealth = maxHealth;

        //Check if deafeated
        if ( currentHealth <= 0 ) PlayerDefeated();

        UpdateHealthUI();
    }

    public void SetPlayerHealthToMax()
    {
        currentHealth = maxHealth;
        UpdateHealthUI();
    }

    private void UpdateHealthUI()
    {
        if (currentHealth == 3)
        {
            foreach (Heart h in hearts)
            {
                h.GainHeart();
            }
        }
        else if (currentHealth == 2)
        {
            hearts[2].LoseHeart();
        }
        else if (currentHealth == 1)
        {
            hearts[2].LoseHeart();
            hearts[1].LoseHeart();
        }
        else if (currentHealth == 0)
        {
            foreach (Heart h in hearts)
            {
                h.LoseHeart();
            }
        }
    }

    private void PlayerDefeated()
    {
        //InputManager.s.DisablePlayerInput();
        Debug.Log("Player defeated");
        Respawn();
    }

    public void ForcePlayerDefeat()
    {
        PlayerDefeated();
    }

    private void Respawn()
    {
        cCon.enabled = false;

        transform.position = respawnPosition;
        SetPlayerHealthToMax();
        velocity = Vector3.zero;

        cCon.enabled = true;
    }

    private void BeginClimbing(Vector3 snapLocation, Collider snapCollider)
    {
        playerState = ePlayerState.CLIMBING;
        velocity = Vector3.zero;
        SnapToWall(snapLocation, snapCollider);

        fixedFramesSinceLastClimbCheck = 0;
    }

    private void EndClimbing()
    {
        playerState = ePlayerState.JUMPING;
        anim.SetBool("Climbing", false);

        //Give a little upward kick if they're holding up
        //TODO: make this movement less jarring, or actually add mantling lol
        if (moveInput.y >= .25f)
            velocity.y = 1f;
    }

    private void SnapToWall(Vector3 snapLocation, Collider snapCollider)
    {
        //GameManager.s.SpawnDebugGameObject(snapLocation);

        //Gotta enable and disable the character controller so it doesn't whine about position setting
        cCon.enabled = false;

        transform.position = snapLocation;

        Vector3 snapDelta = wallSnapPoint.position - transform.position;
        snapDelta.Normalize();
        float dist = Vector3.Distance(transform.position, wallSnapPoint.position);
        snapDelta *= dist;

        

        transform.position = transform.position - snapDelta;

        //face the snap location
        Vector3 closeSpot = snapCollider.ClosestPointOnBounds(transform.position);
        Vector3 snapSpotToFace = closeSpot; //We may need to synchronize the Y of this, and the player position
        transform.LookAt(snapSpotToFace);

        Vector3 debugDir = (snapSpotToFace - transform.position).normalized;
        Debug.DrawRay(transform.position, debugDir, Color.red, 10f);

        cCon.enabled = true;
    }

    private void MoveUsingClimbingRules()
    {
        fixedFramesSinceLastClimbCheck++;

        //Check we're still on a climbable material if we're using material rules
        if (!useTagClimbDetection)
        {
            if (fixedFramesSinceLastClimbCheck >= fixedFramesBetweenClimbChecks)
            {
                fixedFramesSinceLastClimbCheck = 0;

                //Fire a ray and gather some data
                Ray r = new Ray(transform.position, transform.forward);

                RaycastHit hit;
                Collider collSave = null;

                if (Physics.Raycast(r, out hit, 3f))
                {
                    collSave = hit.collider;
                }

                if (!UseRaycastToCheckForClimbingValidity(collSave))
                {
                    EndClimbing();
                }
                //TODO: adding continual snapping so you can climb around shallow corners
                //else
                //{
                //    //snap to the wall
                //    Vector3 snapLocation = collSave.ClosestPointOnBounds(transform.position);

                //    SnapToWall(snapLocation);
                //    Vector3 snapSpotToFace = new Vector3(snapLocation.x, transform.position.y, snapLocation.z);
                //    transform.LookAt(snapSpotToFace);
                //}
            }
        }
        else
        {
            //Do tag checking version of end checking.
            Ray r = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(r, out hit, 1f))
            {
                if (hit.collider.tag != "Climbable") EndClimbing();
                else if (fixedFramesSinceLastClimbCheck >= fixedFramesBetweenClimbChecks)
                {
                    fixedFramesSinceLastClimbCheck = 0;
                    SnapToWall(hit.collider.ClosestPointOnBounds(wallSnapPoint.position), hit.collider);
                }
            }
            else
            {
                EndClimbing();
            }
        }

        //Make sure the player's snapped to the wall
        //TODO: add something for this later to support shifting shape of wall
        

        Vector3 targetVel = Vector3.zero;
        targetVel.y = moveInput.y;
        targetVel.x = moveInput.x;
        targetVel.Normalize();
        targetVel *= climbSpeed * Time.fixedDeltaTime;

        Vector3 debugValue = targetVel / Time.fixedDeltaTime;

        Debug.Log("target before change: " + debugValue.ToString());

        //Adjust the movement to be reletive to the character
        targetVel = transform.TransformDirection(targetVel);

        debugValue = targetVel / Time.fixedDeltaTime;

        Debug.DrawRay(transform.position, debugValue, Color.red, 100f);

        Debug.Log("Target after change" + debugValue.ToString());



        velocity.z = ChangeWithCap(velocity.z, targetVel.z, climbAccel * Time.fixedDeltaTime);
        velocity.y = ChangeWithCap(velocity.y, targetVel.y, climbAccel * Time.fixedDeltaTime);
        velocity.x = ChangeWithCap(velocity.x, targetVel.x, climbAccel * Time.fixedDeltaTime);

        cCon.Move(velocity);

        //GetTargetVelBasedOnState(targetVel);
    }

    public void HandleLeavingAWall(Collider exitedWall)
    {
        //Check if the thing we're leaving is in the geography layermask
        if (!(groundCheckLayermask == (groundCheckLayermask | exitedWall.gameObject.layer)))
        {
            return;
        }

        canWallSlide = false;  //We're not near a wall so we shouldn't slide
        wallSlideColliderReference = null;

        if (playerState == ePlayerState.WALLSLIDE)
        {
            DisableGroundedAnimationBools();
            DisableJumpingAnimationBools();
            if (IsGrounded())
            {
                playerState = ePlayerState.WALKING;
            }
            else
            {
                playerState = ePlayerState.JUMPING;
                anim.SetBool("Jumping", true);
            }
        }
    }

    //TODO: This probably doesn't really need to take a collider as a parameter, it should just get it itself...

    public bool UseRaycastToCheckForClimbingValidity()
    {
        return UseRaycastToCheckForClimbingValidity(null);
    }


    private bool UseRaycastToCheckForClimbingValidity(Collider colliderToCheck)
    {
        //This is redundant with some code below
        //TODO: fix that
        if (colliderToCheck == null)
        {
            //Fire a ray and gather some data
            Ray r = new Ray(transform.position, transform.forward);

            RaycastHit hit;
            if (Physics.Raycast(r, out hit, 3f))
            {
                colliderToCheck = hit.collider;
            }
            
            else
            {
                Debug.Log("1");
                return false;
            }
        }


        bool canClimb = false;
        if (!useTagClimbDetection)
        {
            //Fire a ray and gather some data
            Vector3 target = colliderToCheck.ClosestPointOnBounds(transform.position);
            Vector3 targetDir = (target - transform.position).normalized;
            Ray r = new Ray(transform.position, transform.forward);

            RaycastHit hit;
            if (Physics.Raycast(r, out hit, 3f))
            {
                ProBuilderMesh pbm = hit.collider.GetComponent<ProBuilderMesh>();
                MeshRenderer mRend = hit.collider.GetComponent<MeshRenderer>();

                if (pbm != null)
                {
                    //Use Jeremy's code to find the face
                    Face foundFace = MeshUtilities.GetFaceFromProbuilderMeshAndRaycastHit(pbm, hit);


                    if (foundFace != null)
                    {
                        int subMesh = foundFace.submeshIndex;

                        Material foundMat = mRend.materials[subMesh];
                        string foundMatString = foundMat.name;

                        foreach (string s in climbableMaterialNames)
                        {
                            if (foundMatString.Contains(s)) canClimb = true; //This is weird, but the material on the face has "(instance)" tacked on the end so this is a way to get around that
                        }
                    }

                }

            }
            else
            {
                return false;
            }

        }
        //If using components for climb detection
        else
        {
            if (colliderToCheck.tag == "Climbable")
            {
                canClimb = true;
            }
        }

        return canClimb;
    }

    public void HandleHittingAWall(Collider other)
    {
        //Grabbie isn't a wall.
        if (other.tag == "Player")
        {
            return;
        }

        //Return if the thing we're hitting isn't actually in the layermask that includes walls.
        if (!(groundCheckLayermask == (groundCheckLayermask | other.gameObject.layer)))
        {
            return;
        }

        //We can just return if we're already climbing
        if (playerState == ePlayerState.CLIMBING) return;

        //We're probably not gonna have climbing, so this code doesn't need to run.
        //bool canClimb = false;

        //if (!useTagClimbDetection)
        //{
        //    canClimb = UseRaycastToCheckForClimbingValidity(other);
        //}
        //else
        //{
        //    Ray r = new Ray(transform.position, transform.forward);
        //    RaycastHit hit;
        //    if (Physics.Raycast(r, out hit, 1f))
        //    {
        //        if (hit.collider.tag == "Climbable") canClimb = true;
        //    }
        //}

        ////Climb if appropriate, if not go on to bonk checking
        //if (canClimb)
        //{
        //    BeginClimbing(other.ClosestPointOnBounds(wallSnapPoint.position), other);
        //    return;
        //}

        bool bonk = true;

        //This is redundant with one of the code elements above
        ////Only bonk on the world (default physics layer)
        //if (other.gameObject.layer != LayerMask.NameToLayer("Default"))
        //{
        //    bonk = false;
        //}

        //Only bonk if we're in a state that allows bonking
        if (playerState != ePlayerState.CROUCHING && playerState != ePlayerState.LONGJUMPING && playerState != ePlayerState.DIVING)
        {
            bonk = false;
        }
        //Don't bonk on treasure or interactables
        else if (other.tag == "Treasure" || other.tag == "Interactable" || other.tag == "Checkpoint")
        {
            bonk = false;
        }
        //Only bonk if we're going kinda fast
        else if (velocity.magnitude < minSpeedToBonk)
        {
            bonk = false;
        }

        if (bonk)
        {
            Vector3 bonkPoint = other.transform.position;
            Bonk(bonkPoint);
            return;
        }

        //See if we should be hanging
        //CheckForLedgeGrabAndChangeStateIfAppropriate();
        if (playerState == ePlayerState.HANGING)
        {
            return;
        }

        //If we get to here, we should wall slide

        //Fire a raycase to find the normal of the wall.
        //Vector3 closePointOnBounds = other.ClosestPointOnBounds(transform.position);
        canWallSlide = true;
        wallSlideColliderReference = other;

        //This has been moved to HandleMovement()
        //BeginWallSlide(closePointOnBounds);
        
    }

    private void TriangleHitDebug(Mesh mesh, int triangleIndex, Transform hitTransform)
    {
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3 p0 = vertices[triangles[triangleIndex * 3 + 0]];
        Vector3 p1 = vertices[triangles[triangleIndex * 3 + 1]];
        Vector3 p2 = vertices[triangles[triangleIndex * 3 + 2]];
        p0 = hitTransform.TransformPoint(p0);
        p1 = hitTransform.TransformPoint(p1);
        p2 = hitTransform.TransformPoint(p2);
        Debug.DrawLine(p0, p1, Color.red, 10);
        Debug.DrawLine(p1, p2, Color.red, 10);
        Debug.DrawLine(p2, p0, Color.red, 10);
    }

    private Vector3 GetPositionOfTriangle(Mesh mesh, int triangleIndex, Transform meshTransform)
    {
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3 v0 = vertices[triangles[triangleIndex * 3 + 0]];
        Vector3 v1 = vertices[triangles[triangleIndex * 3 + 1]];
        Vector3 v2 = vertices[triangles[triangleIndex * 3 + 2]];

        Vector3 averagePositionOfTriangle = (v0 + v1 + v2) / 3f;

        averagePositionOfTriangle = meshTransform.TransformPoint(averagePositionOfTriangle);

        return averagePositionOfTriangle;
    }

    //private float GetDistanceToTriangle(Mesh mesh, int triangleIndex, Transform meshTransform)
    //{
    //    Vector3[] vertices = mesh.vertices;
    //    int[] triangles = mesh.triangles;
    //    Vector3 v0 = vertices[triangles[triangleIndex * 3 + 0]];
    //    Vector3 v1 = vertices[triangles[triangleIndex * 3 + 1]];
    //    Vector3 v2 = vertices[triangles[triangleIndex * 3 + 2]];


    //}

    private bool CheckIfNormalFacesSomething(Vector3 normal, Vector3 directionTowardsReferenceObject)
    {
        float dot = Vector3.Dot(normal, directionTowardsReferenceObject);
        return (dot < 0);
    }

    private void Bonk(Vector3 bonkPoint)
    {
        KnockbackPlayer(bonkPoint, knockbackMult: bonkKnockbackMult, procInvulnerability: false, overrideKnockbackWithNormal: true);
        PlayBonkVFX();
    }

    private void PlayBonkVFX()
    {
        bonkParticleSystem.Play();
    }

    private void BeginWallSlide(Vector3 wallAttachPoint)
    {
        anim.SetBool("WallSlide", true);

        Vector3 target = wallAttachPoint;

        //Reset jump count
        jumpsPrimed = numberJumps;

        //Directly Face the wall
        target.y = transform.position.y;
        transform.LookAt(wallAttachPoint);

        //Position to attach to the wall
        Vector3 delta = target - wallSnapPoint.position;
        cCon.Move(delta);

        //Reset velocity, so we slowly start falling with gravity
        velocity = Vector3.zero;

        playerState = ePlayerState.WALLSLIDE;

        //Prepare wall slide pause
        timeWallSlideBegan = Time.time;
    }

    static public void RegisterInteractable(Interactable i)
    {
        CLOSE_INTERACTABLES.Add(i);
    }

    static public void UnregisterInteractable(Interactable i)
    {
        CLOSE_INTERACTABLES.Remove(i);
    }

    public void RegisterRespawnLocation(Vector3 newRespawnPosition)
    {
        respawnPosition = newRespawnPosition;
    }

    private void CheckForLedgeGrabAndChangeStateIfAppropriate()
    {
        ////Fire a ray from the ledge detection origin downwards
        Ray r = new Ray(ledgeDetectionOriginTransform.position, Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(r, out hit, ledgeDetectionRayLength,groundCheckLayermask))
        {
            Vector3 debugPos = ledgeDetectionOriginTransform.position + (Vector3.down * ledgeDetectionRayLength);
            Debug.DrawLine(ledgeDetectionOriginTransform.position, debugPos, Color.red, 5f);

            //Figure out where the top of the object we hit lives
            Vector3 pointOnTop = hit.point;
            float dist = Vector3.Distance(pointOnTop, grabLocationReferenceTransform.position);
            if (dist <= maxLedgeGrabDistance && hit.normal == Vector3.up) //Make sure we're close to the ledge and it's facing up
            {
                //Debug.Log("Dist: " + dist.ToString());
                //Debug.Log("Max dist: " + maxLedgeGrabDistance.ToString());

                //GRABBO
                GrabLedge(pointOnTop.y, hit);
            }
        }
    }

    #region Interactable Hooks
    /// <summary>
    /// Can be called if we want to immediately halt vertical (or other) movement, I.E. we just hit a bouncer.
    /// </summary>
    public void ZeroOutVelocity()
    {
        velocity = Vector3.zero;
    }
    public void ZeroOutYVelocity()
    {
        velocity.y = 0;
    }

    public void AddVelocity(Vector3 velocityToAdd)
    {
        velocity += velocityToAdd;
    }

    /// <summary>
    /// Used by another class to hard force Grabbie into a jumping state
    /// </summary>
    public void ForceJumpState(float delayBeforeNextGroundCheck = .5f)
    {
        playerState = ePlayerState.JUMPING;
        DisableGroundedAnimationBools();
        DisableJumpingAnimationBools();
        anim.CrossFade("Jumping", 0);
        anim.SetBool("Jumping", true);
        TemporarilyDisableGroundedCheck(delayBeforeNextGroundCheck);
    }
    #endregion

    private void GrabLedge(float edgeY, RaycastHit hit)
    {
        Debug.Log("The object I'm hanging on: " + hit.collider.gameObject.name);

        float positionToHeadDelta = Mathf.Abs(transform.position.y - grabLocationReferenceTransform.position.y);
        Vector3 targetPos = transform.position;
        targetPos.y = edgeY;
        targetPos.y -= positionToHeadDelta;

        //move to ledge
        cCon.enabled = false;
        transform.position = targetPos;
        cCon.enabled = true;

        //turn to face the wall
        Vector3 wallPoint = hit.collider.ClosestPointOnBounds(transform.position);
        Vector3 spotToFace = new Vector3(wallPoint.x, transform.position.y, wallPoint.z);
        transform.LookAt(spotToFace);

        playerState = ePlayerState.HANGING;
    }

    public bool IsGrounded()
    {
        return isGrounded;
    }

    #region Spawn Point Jumping
    public void JumpToSpawnPoint (int spawnPointIndex)
    {
        if (StartPointManager_2022_1.s == null)
        {
            Debug.LogWarning("Attempted to use the spawn point manager when one is not present in the scene!");
            return;
        }
        StartPointManager_2022_1.s.RespawnAtNodeIndex(spawnPointIndex);
    }

    /// <summary>
    /// The following functions collect input for the spawn point debug interaction
    /// </summary>

    private void OnSpawn1(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(0);
        }
    }
    private void OnSpawn2(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(1);
        }
    }
    private void OnSpawn3(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(2);
        }
    }
    private void OnSpawn4(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(3);
        }
    }
    private void OnSpawn5(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(4);
        }
    }
    private void OnSpawn6(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(5);
        }
    }
    private void OnSpawn7(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(6);
        }
    }
    private void OnSpawn8(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(7);
        }
    }
    private void OnSpawn9(InputValue value)
    {
        if (!useHardcodedInput && SpawnPointTeleportationDebugMode)
        {
            float spawnValue = value.Get<float>();
            if (spawnValue > .05f) JumpToSpawnPoint(8);
        }
    }
    #endregion

    #region Crouching stuff
    private void EnterCrochedProfile()
    {
        cCon.height = crouchingHeight;
        cCon.center = crouchingCharacterControllerCenter;
    }

    private void LeaveCrouchedProfile()
    {
        cCon.height = standingHeight;
        cCon.center = standingCharacterControllerCenter;
    }

    private void SaveVariablesForCrouchProfiles()
    {
        standingHeight = cCon.height;
        standingCharacterControllerCenter = cCon.center;
    }

    private void SetPlayerHeightProfile()
    {
        if (playerState == ePlayerState.ROLLING || playerState == ePlayerState.CROUCHING || playerState == ePlayerState.CRAWLING)
        {
            EnterCrochedProfile();
        }
        else
        {
            LeaveCrouchedProfile();
        }
    }

    /// <summary>
    /// Fires a raycast up to check if we need to force crouching due to a low ceiling
    /// </summary>
    /// <returns></returns>
    private bool SomethingAboveGrabbiesHead()
    {
        float dist = (cCon.height / 2) + .2f;
        Vector3 origin = transform.position + cCon.center;

        RaycastHit hit;
        if (Physics.Raycast(origin, Vector3.up, out hit, dist, groundCheckLayermask))
        {
            return true;
        }

        return false;
    }
    #endregion

    #region Slope Sliding
    private void BeginSlopeSlide()
    {
        //Dont' slide if we just jumped off a slope
        if (Time.time - timeJumpedOffSlopSlide <= .5f)
        {
            return;
        }

        DisableGroundedAnimationBools();
        DisableJumpingAnimationBools();
        anim.SetBool("SlopeSlide", true);
        anim.CrossFade("Slope Slide", 0);

        jumpsPrimed = numberJumps;

        playerState = ePlayerState.SLOPESLIDE;

        //Convert velocity into a brief slide would probably feel better
        velocity = Vector3.zero;
    }

    private void EndSlopeSlide(bool jumped = false)
    {
        if (jumped)
        {
            timeJumpedOffSlopSlide = Time.time;
            playerState = ePlayerState.JUMPING;
        }
        else
        {
            playerState = ePlayerState.WALKING;
        }

        anim.SetBool("SlopeSlide", false);
    }
    #endregion

#region useful functions
    static public void TeleportCharacter( Vector3 targetPosition ) {
        TeleportCharacter(targetPosition, _S.transform.rotation);
    }
    static public void TeleportCharacter(Vector3 targetPosition, Quaternion targetRotation)
    {
        _S.cCon.enabled = false;
        _S.transform.position = targetPosition;
        _S.transform.rotation = targetRotation;
        _S.cCon.enabled = true;
    }
#endregion
}

public class InputBufferData
{
    public float timeOfInput;
    public InputType inputType;
}

#if UNITY_EDITOR
//Uncomment this block to disable this script in the editor
[CustomEditor(typeof(PlayerController))]
public class PlayerControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif

#endif