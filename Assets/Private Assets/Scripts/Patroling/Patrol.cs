using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Patrol : MonoBehaviour
{

    [HideInInspector]public List<Vector3> movementPointList; // This is the list used to contain the custom points
    [SerializeField] public GameObject patrolHandle;

    /// <summary>
    /// This class looks empty but I plan to add all the methods to add points and remove points from a patrol
    /// Becuase we want multiple enemies on a single patrol, we can add and remove individually. 
    /// If the creator wants a similar patrol that has one or two difference, they may be needed to make a entier new
    /// instance of the script to attach to the enemy object.
    /// 
    /// However, I think we can find a way to make it easy for the students as the field here is serialized so all I they 
    /// have to do is attach it to a new game object and then edit the list in there
    /// 
    /// For now, its just a way to get a list attached to an object that can be saved as a prefab ie. DebuggerCube.
    /// Thats the best way I can think of doing this as we want a list to be applied to multiple enemies
    /// -Suraj
    /// </summary>

    // Start is called before the first frame update
    void Start()
    {
        PatrolHandle[] handles = GetComponentsInChildren<PatrolHandle>();

        if (movementPointList == null) movementPointList = new List<Vector3>();

        foreach (PatrolHandle p in handles)
        {
            movementPointList.Add(p.transform.position);
        }

        foreach(Vector3 v in movementPointList)
        {
            Debug.Log("Point:" + v.ToString());
        }
    }

    // Update is called once per frame
}

public class PatrolHandle: MonoBehaviour
{
    //This is simply a class to identify which transforms are patrol handles
}

#if UNITY_EDITOR
[CustomEditor(typeof(Patrol))]
public class PatrolEdit: Editor
{
    public override void OnInspectorGUI()
    {
        Patrol pScript = target as Patrol;

        base.OnInspectorGUI();

        if (GUILayout.Button("Add Patrol Point"))
        {
            //Create the handle here, add its position to the movementPointList.
            //This should probably create an new empty game object as a child of the patrol prefab, so it's position can be set in the scene.
            GameObject newPatrolPointHandle = Instantiate<GameObject>(pScript.patrolHandle);
            newPatrolPointHandle.transform.SetParent(pScript.transform);
            newPatrolPointHandle.transform.localPosition = Vector3.zero;
            newPatrolPointHandle.name = "Patrol Point Handle";
            newPatrolPointHandle.AddComponent<PatrolHandle>();
        }

        //Also have a button for removing handles, maybe deleting the last index in children or something?
        if (GUILayout.Button("Remove Patrol Point"))
        {
            if (pScript.transform.childCount == 0)
            {
                Debug.Log("There is no patrol handle to remove.");
                return; 
            }

            int childIndex = pScript.transform.childCount - 1;
            Transform targetTransform = pScript.transform.GetChild(childIndex);

            if (targetTransform != null)
            {
                DestroyImmediate(targetTransform.gameObject);
            }
        }
    }
}
#endif
