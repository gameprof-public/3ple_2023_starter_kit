using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PortaPottySpawner : Spawner
{
    [Header("Portapotty Settings")]
    [Tooltip("Set this to true for 'unarmored' portapotties, that can be destroyed by jumping on top of them.")]
    [SerializeField] private bool canBeDestroyed = true;

    private Animator anim;

    public override void Start()
    {
        base.Start();

        anim = GetComponent<Animator>();
    }

    protected override void SpawnEnemy()
    {
        StartCoroutine(OpenDoorThenSpawn());
    }

    private IEnumerator OpenDoorThenSpawn()
    {
        //Wait for the door to open. The number of seconds must be manually set based on the length of the animation.
        anim.SetBool("Open", true);
        yield return new WaitForSeconds(1);

        base.SpawnEnemy();

        yield return new WaitForSeconds(.5f);
        anim.SetBool("Open", false);
    }

    public void GetJumpedOn(Collider other)
    {
        //Make sure it's the player
        PlayerController pCon = other.GetComponent<PlayerController>();
        if (pCon == null)
        {
            return;
        }

        //Halt any in-progress spawning
        StopAllCoroutines();

        //Destroy if we got groundpounded, and we can be destroyed
        if (canBeDestroyed && PlayerController.GetPlayerState() == PlayerController.ePlayerState.GROUNDPOUND)
        {
            Destroy(gameObject);
        }

        //Bounce the player
        PlayerController.HandleEnemyBounce();

        //Play a VFX

        Debug.Log( $"{name} jumped on.  Player state: {PlayerController.GetPlayerState()}  canBeDestroyed: {canBeDestroyed}" );
        //Debug.Log("I got jumped on, player state: " + PlayerController.GetPlayerState().ToString() + "    Any my can be destroyed state: " + canBeDestroyed.ToString());


    }

    public void GetJumpedOn(Collider other, HitBox hitBox) {
        //TODO: Only call GetJumpedOn(other) if Rocky is above bottom y of hitbox
        GetJumpedOn( other );
    }
}
