using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;


namespace XnTelemetry {
    public class Telemetry_Cloud : MonoBehaviour {
        const float LOG_INTERVAL = 0.25f;

        static Telemetry_Cloud S;
        static List<Telem> ENTRIES = new List<Telem>();
        static float TIME_START;
        static public Vector3 V3_INVALID = new Vector3(-999, -999, -999);

        public bool recordTelemetry = true;
        [Tooltip("If you wish the rotation to be based on a Transform other than the this one, put it here.")]
        public Transform rotationTransform = null;
        public SO_TelemetrySettings telemetrySettings;

        // Start is called before the first frame update
        void Start() {
            if (!recordTelemetry) return;
            S = this;
            Reset();
            Log("Start");
            StartCoroutine(LogEveryInterval());
        }

        public void Log(string tag = "_") {
            if (rotationTransform == null) {
                ENTRIES.Add(new Telem(elapsed, transform.position, transform.rotation, tag));
            } else {
                ENTRIES.Add(new Telem(elapsed, transform.position, rotationTransform.rotation, tag));
            }
        }

        IEnumerator LogEveryInterval() {
            while (true) {
                Log();
                yield return new WaitForSeconds(LOG_INTERVAL);
            }
        }

        public void Reset() {
            StopAllCoroutines();
            TIME_START = Time.time;
            ENTRIES.Clear();
        }

        public float elapsed { get { return Time.time - TIME_START; } }

        void OnDestroy() {
            if (!recordTelemetry) return;


            if (telemetrySettings.sceneName == "") {
                telemetrySettings.sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            }

            if (telemetrySettings.saveToCloud) {
                SaveToCloud();
            } else {
                SaveLocal();
            }
        }

        void SaveLocal(string fileName = "") {
            string log = EntriesToString();
            Debug.Log(log);
            if (fileName == "") fileName = "Telemetry_";
            fileName += telemetrySettings.sceneName + "_";
            fileName += telemetrySettings.playerName.Replace(" ", "") + "_";

            fileName += System.DateTime.Now.ToDateTimeStamp();
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
            path += "/" + fileName + ".txt";
            File.WriteAllText(path, log);
            Debug.Log("Wrote telemetry to: " + path);
        }

        void SaveToCloud() {
            WWWForm form = new WWWForm();
            form.AddField("project", telemetrySettings.project);
            form.AddField("semester", telemetrySettings.semester);
            form.AddField("version", telemetrySettings.version);
            form.AddField("sceneName", telemetrySettings.sceneName);
            form.AddField("playerName", telemetrySettings.playerName);

            form.AddField("data", EntriesToString());

            Debug.Log("Form data\n" + System.Text.ASCIIEncoding.ASCII.GetString(form.data));

            // Post to WWW
            //StartCoroutine( POST_CR( "http://telemetry.prototools.net/postTelemetry.php", form ) );
#if UNITY_EDITOR
            new Telemetry_CloudHelper("http://telemetry.prototools.net/postTelemetry.php", form);
#else
            Telemetry_CloudHelper_MonoBehaviour.POST( "http://telemetry.prototools.net/postTelemetry.php", form );
#endif
        }

        //IEnumerator POST_CR(string url, WWWForm form) {
        //    using ( UnityWebRequest www = UnityWebRequest.Post( url, form ) ) {
        //        yield return www.SendWebRequest();

        //        if ( www.result != UnityWebRequest.Result.Success ) {
        //            Debug.Log( www.error );
        //        } else {
        //            Debug.Log( "Form upload complete!" );
        //        }
        //    }
        //}

        /*
        /// <summary>
        /// From https://stackoverflow.com/questions/38109658/unity-post-request-using-www-class-using-json
        /// </summary>
        /// <returns></returns>
        public WWW POST(string jsonStr) {
            WWW www;
            Hashtable postHeader = new Hashtable();
            postHeader.Add( "Content-Type", "application/json" );

            // convert json string to byte
            var formData = System.Text.Encoding.UTF8.GetBytes( jsonStr );

            www = new WWW( POSTAddUserURL, formData, postHeader );
            StartCoroutine( WaitForRequest( www ) );
            return www;
        }

        IEnumerator WaitForRequest( WWW data ) {
            yield return data; // Wait until the download is done
            if ( data.error != null ) {
                Debug.LogError( "There was an error sending Telemetry_Local POST request: " + data.error );
            } else {
                Debug.Log( "Telemetry_Local POST Request: " + data.text );
            }
        }
        */

        static public string EntriesToString() {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("Tag\tTime\tx\ty\tz\trX\trY\trZ");
            for (int i = 0; i < ENTRIES.Count; i++) {
                sb.AppendLine(ENTRIES[i].ToString());
            }
            return sb.ToString();
        }

        static public void LOG(string tag) {
            if (S != null) S.Log(tag);
        }


#region Telemetry_Local.Telem
        //[System.Serializable]
        public struct Telem {
            const char TAG_SPLIT_CHAR = ' ';

            public float      time;
            public Vector3    position;
            public Quaternion rotation;
            public string     tag;
            public string[]   splitTag;
            public Vector3    p0, p1, p2;

            public Telem(float t, Vector3 pos, Quaternion rot, string tg = "") {
                this.time = t;
                this.position = pos;
                this.rotation = rot;
                this.tag = tg;
                this.splitTag = tg.Split(TAG_SPLIT_CHAR);
                p0 = p1 = p2 = V3_INVALID; // Give these a base value that can be replaced in the Telemetry_LocalViewer
            }

            override public string ToString() {
                Vector3 rot = rotation.eulerAngles;
                return $"{tag}\t{time:0.00}\t{position.x:0.00}\t{position.y:0.00}\t{position.z:0.00}\t{rot.x:0.00}\t{rot.y:0.00}\t{rot.z:0.00}";
            }

            static public Telem FromString(string s) {
                string[] bits = s.Split('\t');
                if (bits.Length != 5 && bits.Length != 8) {
                    Debug.LogError($"Telem string parsed into wrong number of bits: {bits.Length}.\n{s}");
                    return new Telem(-1, Vector3.zero, Quaternion.identity, "ERROR");
                }
                string tag = bits[0];
                float time;
                Vector3 pos = Vector3.zero;
                Vector3 rot = Vector3.zero;
                try {
                    time = float.Parse(bits[1]);
                    pos.x = float.Parse(bits[2]);
                    pos.y = float.Parse(bits[3]);
                    pos.z = float.Parse(bits[4]);
                } catch {
                    Debug.LogError($"Telem floats did not parse correctly.\n{s}");
                    return new Telem(-1, Vector3.zero, Quaternion.identity, "ERROR");
                }
                try {
                    rot.x = float.Parse(bits[5]);
                    rot.y = float.Parse(bits[6]);
                    rot.z = float.Parse(bits[7]);
                } catch {
                    // Don't actually need to do anything here, just ignore missing rotation. - JGB
                }
                return new Telem(time, pos, Quaternion.Euler(rot), tag);
            }
        }
#endregion
    }
}
