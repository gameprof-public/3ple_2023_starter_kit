using System.Collections;
using System.Collections.Generic;
using KinematicCharacterController;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class StartPointManager : MonoBehaviour
{
    public static StartPointManager s;

    // [SerializeField] public Transform playerTransform;
    
    [HideInInspector] 
    [SerializeField] private GameObject startPointPrefab;
    
    [SerializeField]
    [KinematicCharacterController.ReadOnly]
    private List<Transform> spawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        if (s != this)
        {
            if (s != null) Debug.LogWarning("You have more than one StartPointManager in the scene, this is gonna cause problems!");
            s = this;
        }

        RefreshSpawnPoints();
        int childCount = transform.childCount;
        if (childCount != 0)
        {
            RespawnAtNodeIndex(0);
        }
    }

    void RefreshSpawnPoints() {
        if ( spawnPoints == null ) spawnPoints = new List<Transform>();
        spawnPoints.Clear();
        for ( int i = 0; i < transform.childCount; i++ ) {
            spawnPoints.Add( transform.GetChild(i) );
        }
    }

    public void RespawnAtNodeIndex(int nodeIndex) {
        int childcount = transform.childCount;

        if (nodeIndex >= childcount) {
            Debug.Log(
                "Attempted to spawn at a spawn node that doesn't exist (i.e. maybe you pressed 9 but don't have 9 spawn nodes).");
            return;
        }

        PlayerController.TeleportCharacter(
            transform.GetChild(nodeIndex).position + Vector3.up,
            transform.GetChild(nodeIndex).rotation );
        

        // KinematicCharacterMotor motor = playerTransform.GetComponent<KinematicCharacterMotor>();
        // if (motor != null) {
        //     motor.SetPosition(transform.GetChild(nodeIndex).position +
        //                       Vector3.up); //Raise us up a bit so Rocky doesn't fall through the floor
        //     motor.SetRotation(transform.GetChild(nodeIndex).rotation);
        // } else {
        //     // PlayerController pc = playerTransform.GetComponent<PlayerController>();
        //     // bool pcIsNotNull = (pc != null);
        //     // if (pcIsNotNull) pc.DisableCharacterController();
        //     //
        //     // playerTransform.position =
        //     //     transform.GetChild(nodeIndex).position +
        //     //     Vector3.up; //Raise us up a bit so Player doesn't fall through the floor
        //     // playerTransform.rotation = transform.GetChild(nodeIndex).rotation;
        //     //
        //     // if (pcIsNotNull) pc.EnableCharacterController();
        // }
    }

#if UNITY_EDITOR
    private void OnValidate() {
        RefreshSpawnPoints();
    }

    public void AddStartNode()
    {
        //Create a new node and send it to the StartPoint
        GameObject newStartNode = PrefabUtility.InstantiatePrefab(startPointPrefab) as GameObject;
        newStartNode.transform.SetParent(transform);
        newStartNode.transform.localPosition = Vector3.zero;

        int childCount = transform.childCount;

        newStartNode.GetComponent<StartPointHandle>().SetStartNodeNumber(childCount);
        newStartNode.name = "Start Node #" + childCount.ToString();
        
        RefreshSpawnPoints();
    }

    public void RemoveStartNode()
    {
        int indexToRemove = transform.childCount - 1;
        DestroyImmediate(transform.GetChild(indexToRemove).gameObject);
        RefreshSpawnPoints();
    }
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(StartPointManager))]
public class StartPointEditor : Editor
{
    public override void OnInspectorGUI()
    {
        StartPointManager sPoint = target as StartPointManager;
        
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
        }
        
        base.OnInspectorGUI();

        if (GUILayout.Button("Add Start Node"))
        {
            sPoint.AddStartNode();
        }

        //Also have a button for removing handles, maybe deleting the last index in children or something?
        if (GUILayout.Button("Remove Start Node"))
        {
            sPoint.RemoveStartNode();
        }
    }
}
#endif
