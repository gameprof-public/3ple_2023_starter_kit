using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// A class for applying knockback force to the player.
/// Borrowed from a comment here: https://answers.unity.com/questions/242648/force-on-character-controller-knockback.html
/// </summary>
public class PlayerKnockback : MonoBehaviour
{
    [SerializeField] private float dissipation = 3f;
    private Vector3 impact = Vector3.zero;
    private CharacterController cCon;
    private PlayerController pCon;
    private float timeHit = -1;

    private void Start()
    {
        cCon = GetComponent<CharacterController>();
        pCon = GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        if (pCon.IsGrounded() && ((Time.time - timeHit) > .2f))
        {
            ResetHitState();
            return;
        }

        //Apply the impact force
        if (impact.magnitude > .4f)
        {
            cCon.Move(impact * Time.fixedDeltaTime);
        }
        else
        {
            ResetHitState();
        }
        //Reduce the impact each cycle, simulating drag or force dissipation I guess
        if (impact != Vector3.zero)
        {
            impact = Vector3.Lerp(impact, Vector3.zero, dissipation * Time.fixedDeltaTime);
        }
    }

    public void AddForce(Vector3 force)
    {
        timeHit = Time.time;
        impact += force;
    }

    private void ResetHitState()
    {
        impact = Vector3.zero;
        timeHit = -1;
    }
}

#if UNITY_EDITOR
//Uncomment this block to disable the player controller's editor
[CustomEditor(typeof(PlayerKnockback))]
public class PlayerKnockbackEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif
