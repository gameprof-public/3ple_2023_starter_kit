using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class StartPointManager_2022_1 : MonoBehaviour
{
    public static StartPointManager_2022_1 s;

    [HideInInspector] [SerializeField] private GameObject startPointPrefab;
    [SerializeField]
    [KinematicCharacterController.ReadOnly]
    private List<Transform> spawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        if (s != this)
        {
            if (s != null) Debug.LogWarning("You have more than one StartPointManager_2022_1 in the scene, this is gonna cause problems!");
            s = this;
        }

        RefreshSpawnPoints();
        int childCount = transform.childCount;
        if (childCount != 0)
        {
            RespawnAtNodeIndex(0);
        }
    }

    void RefreshSpawnPoints() {
        if ( spawnPoints == null ) spawnPoints = new List<Transform>();
        spawnPoints.Clear();
        for ( int i = 0; i < transform.childCount; i++ ) {
            spawnPoints.Add( transform.GetChild(i) );
        }
    }

    public void RespawnAtNodeIndex(int nodeIndex)
    {
        int childcount = transform.childCount;
        if (nodeIndex >= childcount)
        {
            Debug.Log("Attempted to spawn at a spawn node that doesn't exist (i.e. maybe you pressed 9 but don't have 9 spawn nodes).");
            return;
        }

        Transform spTrans = transform.GetChild( nodeIndex );
        PlayerController.TeleportCharacter( spTrans.position + Vector3.up, spTrans.rotation );
    }
#if UNITY_EDITOR
    private void OnValidate() {
        RefreshSpawnPoints();
    }

    public void AddStartNode()
    {
        //Create a new node and send it to the StartPoint
        GameObject newStartNode = PrefabUtility.InstantiatePrefab(startPointPrefab) as GameObject;
        newStartNode.transform.SetParent(transform);
        newStartNode.transform.localPosition = Vector3.zero;

        int childCount = transform.childCount;

        newStartNode.GetComponent<StartPointHandle>().SetStartNodeNumber(childCount);
        newStartNode.name = "Start Node #" + childCount.ToString();
        
        RefreshSpawnPoints();
    }

    public void RemoveStartNode()
    {
        int indexToRemove = transform.childCount - 1;
        DestroyImmediate(transform.GetChild(indexToRemove).gameObject);
        RefreshSpawnPoints();
    }
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(StartPointManager_2022_1))]
public class StartPointEditor_2022_1 : Editor
{
    public override void OnInspectorGUI()
    {
        StartPointManager_2022_1 sPoint = target as StartPointManager_2022_1;
        
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
        }

        base.OnInspectorGUI();

        if (GUILayout.Button("Add Start Node"))
        {
            sPoint.AddStartNode();
        }

        //Also have a button for removing handles, maybe deleting the last index in children or something?
        if (GUILayout.Button("Remove Start Node"))
        {
            sPoint.RemoveStartNode();
        }
    }
}
#endif
