using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    private bool playerCloseToMe = false;
    private float timePlayerCloseToMe = 0;

    private void Update()
    {
        if (playerCloseToMe)
        {
            timePlayerCloseToMe += Time.deltaTime;
        }
    }

    public virtual void RegisterInteractableIfAppropriate(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController.RegisterInteractable(this);
            playerCloseToMe = true;
        }
    }

    public virtual void UnregisterInteractableIfAppropriate(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController.UnregisterInteractable(this);
            playerCloseToMe = false;
            timePlayerCloseToMe = 0;
        }
    }

    public virtual void Interact()
    {

    }

    public virtual void AllowInteraction() //Called by the PlayerController when they have stopped and can interact with this object.
    {

    }
}
