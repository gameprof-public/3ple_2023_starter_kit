using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is for platforms within a timed platform challenge.
/// </summary>
public class TimedPlatform : MonoBehaviour
{
    [SerializeField] private MeshRenderer myMeshRenderer;
    private float myFlashDuration = .1f;

    public void Appear()
    {
        gameObject.SetActive(true);
    }

    public void StartFlashingMyMesh()
    {
        StartCoroutine(FlashMyMesh(myFlashDuration));
    }

    private IEnumerator FlashMyMesh(float flashDuration)
    {
        float timeLastFlashProcced = Time.time;

        while (true)
        {
            if ((Time.time - timeLastFlashProcced) >= flashDuration)
            {
                timeLastFlashProcced += flashDuration;
                ToggleGraphicsObject();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void HaltFlashing()
    {
        myMeshRenderer.enabled = true;
        StopAllCoroutines();
    }

    private void ToggleGraphicsObject()
    {
        if (myMeshRenderer.enabled)
        {
            myMeshRenderer.enabled =false;
        }
        else
        {
            myMeshRenderer.enabled = true;
        }
    }

    public void Disappear()
    {
        HaltFlashing();
        gameObject.SetActive(false);
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        transform.parent?.GetComponent<RedCoinChallengeStarter>()?.OnDrawGizmosSelected();
    }
#endif
}
