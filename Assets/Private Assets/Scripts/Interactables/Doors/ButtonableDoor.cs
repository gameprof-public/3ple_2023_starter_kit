using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Animator))]
public class ButtonableDoor : ButtonInteractable
{
    public enum UnlockMode { TIMER, PERMANENT}

    [SerializeField] private UnlockMode unlockMode = UnlockMode.PERMANENT;

    private Animator anim;

    [HideInInspector] public float timerDuration;
    private float timeTimerStarted = -1;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (unlockMode == UnlockMode.TIMER && timeTimerStarted != -1)
        {
            if ((Time.time - timeTimerStarted) > timerDuration)
            {
                Close();
            }
        }
    }

    /// <summary>
    /// Make this easily interacted with via button.
    /// </summary>
    public override void OnPress()
    {
        Open();
    }

    private void Open()
    {
        anim.SetBool("Open", true);
        if (unlockMode == UnlockMode.TIMER)
        {
            timeTimerStarted = Time.time;
        }
    }

    private void Close()
    {
        anim.SetBool("Open", false);
        timeTimerStarted = -1;
    }

    public UnlockMode GetUnlockMode()
    {
        return unlockMode;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ButtonableDoor))]
public class ButtonableDoorEditor: Editor
{
    private ButtonableDoor myDoor;
    private SerializedProperty timerDurationSerialized;

    private void OnEnable()
    {
        myDoor = (ButtonableDoor)target;

        timerDurationSerialized = serializedObject.FindProperty("timerDuration");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        ButtonableDoor.UnlockMode mode = myDoor.GetUnlockMode();

        if (mode == ButtonableDoor.UnlockMode.TIMER)
        {
            timerDurationSerialized.floatValue = EditorGUILayout.FloatField("Timer Duration", timerDurationSerialized.floatValue);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif
