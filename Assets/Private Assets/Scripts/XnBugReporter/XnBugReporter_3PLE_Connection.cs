using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class XnBugReporter_3PLE_Connection : MonoBehaviour {
    static private XnBugReporter_3PLE_Connection _S;

    public TMP_InputField nameInputField, levelInputField;

    private void Awake() {
        _S = this;
    }

    static public void SetVals(string name, string level) {
        _S.nameInputField.text = name;
        _S.levelInputField.text = level;
    }

}
