using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

[CreateAssetMenu(fileName = "Bake Lighting Utility", menuName = "ScriptableObjects/Bake Lighting Utility SO", order = 1)]
public class BakeLightingUtilitySO : ScriptableObject
{
    private int sceneCount;
    private int currentIndex;

    public void BeginLightBakingUtility()
    {
        Debug.Log("Beginning baking utility...");

        sceneCount = EditorSceneManager.sceneCountInBuildSettings;
        currentIndex = 0;

        Debug.Log(sceneCount.ToString() + " scenes detected in build settings...");


        BakeAndPrepNextBake();

        Debug.Log("***Finished baking light for all scenes !***");
    }

    private void BakeAndPrepNextBake()
    {
        if (currentIndex != 0)
        {
            Lightmapping.bakeCompleted -= BakeAndPrepNextBake;
        }

        string path = EditorBuildSettings.scenes[currentIndex].path;
        Debug.Log("Attempting to open scene at path: " + path + "...");
        EditorSceneManager.OpenScene(path);

        if (currentIndex != (sceneCount -1 ))
        {
            Lightmapping.bakeCompleted += BakeAndPrepNextBake;
        }

        Lightmapping.Bake();
    }
}

[CustomEditor(typeof(BakeLightingUtilitySO))]
public class BakeLightingUtilityEditor: Editor
{
    private BakeLightingUtilitySO myBLU;

    private void OnEnable()
    {
        myBLU = (BakeLightingUtilitySO)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Bake lighting for all build scenes"))
        {
            myBLU.BeginLightBakingUtility();
        }
    }
}
#endif