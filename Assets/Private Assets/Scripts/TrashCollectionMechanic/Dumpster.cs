using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Dumpster : Interactable
{
    public static Dumpster s;

    [Header("Stuff for Dumpster UI")]
    [SerializeField] private GameObject dumpsterTextObject;
    [SerializeField] private GameObject preinteractableUIObject;
    [SerializeField] private GameObject interactableUIObject;
    [SerializeField] private TMP_Text trashCountText;
    private bool displayingTrashCountText = false;

    //Trashs will look at this singleton
    [Header("Settings for trash graphics")]
    [SerializeField] private GameObject[] trashGraphicsPrefabs;
    private List<Trash> trashList;

    // Start is called before the first frame update
    void Awake()
    {
        if (s != this)
        {
            if (s!= null)
            {
                Debug.LogWarning("There are more than one dumbsters in your scene. Please make sure there is only one.");
            }
            s = this;
        }

        trashList = new List<Trash>();
        Trash[] tempTrash = FindObjectsOfType<Trash>();
        foreach (Trash t in tempTrash)
        {
            trashList.Add(t);
        }

        SetupTrashGraphics();
    }

    private void SetupTrashGraphics()
    {
        //First, shuffle our trash graphics
        for (int i = 0; i < trashGraphicsPrefabs.Length; i++)
        {
            GameObject temp = trashGraphicsPrefabs[i];
            int randIndex = Random.Range(0, trashGraphicsPrefabs.Length);
            trashGraphicsPrefabs[i] = trashGraphicsPrefabs[randIndex];
            trashGraphicsPrefabs[randIndex] = temp;
        }

        //Loop through the trash and assign graphics
        int nextGraphicsIndex = 0;
        foreach (Trash t in trashList)
        {
            t.InitializeGraphics(trashGraphicsPrefabs[nextGraphicsIndex]);
            nextGraphicsIndex++;
            //Go back to the start if we run out of graphics prefabs
            if (nextGraphicsIndex == trashGraphicsPrefabs.Length)
            {
                nextGraphicsIndex = 0;
            }
        }
    }

    public void SetUpTrashGraphics(Trash t)
    {
        int trashGraphicsIndex = Random.Range(0, trashGraphicsPrefabs.Length);
        t.InitializeGraphics(trashGraphicsPrefabs[trashGraphicsIndex]);
    }

    // Update is called once per frame
    void Update()
    {
        if (displayingTrashCountText)
        {
            string trashCountTextString = TrashInventory.s.GetTrashInDumpsterCount().ToString() + " / " + TrashInventory.s.GetTotalTrashInLevelCount().ToString();
            trashCountText.text = trashCountTextString;
        }
    }

    public void CloseDumpster()
    {
        dumpsterTextObject.SetActive(false);
        preinteractableUIObject.SetActive(false);
        interactableUIObject.SetActive(false);

        Quaternion zero = new Quaternion(0, 0, 0, 0);

        dumpsterTextObject.transform.rotation = zero;
        preinteractableUIObject.transform.rotation = zero;
        interactableUIObject.transform.rotation = zero;

        displayingTrashCountText = false;
    }

    public override void Interact()
    {
        interactableUIObject.SetActive(false);
        dumpsterTextObject.SetActive(true);
        displayingTrashCountText = true;
    }

    public override void RegisterInteractableIfAppropriate(Collider other)
    {
        base.RegisterInteractableIfAppropriate(other);
        //if (!dumpsterTextObject.activeInHierarchy)
        //    preinteractableUIObject.SetActive(true);

        Interact(); //This is now just straight up called, so the DUmpster shows its info when Grabbie is near. This was changed to this way in 2022.1.A2.
    }

    public override void UnregisterInteractableIfAppropriate(Collider other)
    {
        base.UnregisterInteractableIfAppropriate(other);
        CloseDumpster();
    }

    public override void AllowInteraction()
    {
        //This is called by the player controller when Grabbie stops nearby, so we can use it to show some UI of how much trash has been desposited
        if (!dumpsterTextObject.activeInHierarchy)
        {
            preinteractableUIObject.SetActive(false);
            interactableUIObject.SetActive(true);
        }
    }
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(Dumpster))]
public class DumptsterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif
