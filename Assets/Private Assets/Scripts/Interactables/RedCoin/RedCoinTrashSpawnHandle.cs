using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedCoinTrashSpawnHandle : MonoBehaviour
{
    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, .33f);
    }
}
