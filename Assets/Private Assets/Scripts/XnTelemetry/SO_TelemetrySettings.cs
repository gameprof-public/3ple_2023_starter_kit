using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace XnTelemetry {
    [CreateAssetMenu(fileName = "TelemetrySettings", menuName = "ScriptableObjects/TelemetrySettings")]
    public class SO_TelemetrySettings : ScriptableObject {
        public const string DIVESTRING = "Dive";
        public const string JUMPSTRING = "Jump";

        [Header("Cloud Configuration")]
        public bool saveToCloud = true;
        [XnTools.ReadOnly]
        public string project = "3PLE";
        [XnTools.ReadOnly][Tooltip("Semester must be in the format: 2022.1 (i.e., 4 digits, a decimal, 1 digit, because the DB needs it that way)!")]
        public string semester = "2022.4";
        [HideInInspector]
        public string serverURL = "http://telemetry.prototools.net/queryTelemetryEntries.php";

        [Header("Leave versionOverride empty to pull from Application.version")]
        public string versionOverride = "";

        [Header("Current Play Session Info")]
        public string playerName = "UNKNOWN";
        public string sceneName = "";


        public string version {
            get {
                if (versionOverride != "") return versionOverride;
                return Application.version;
            }
        }
    }
    
    public static class DateTimeExtension {
        static public string ToTimeStamp( this System.DateTime time ) {
            string str = $"{time.Hour:00}{time.Minute:00}";
            return str;
        }

        static public string ToDateStamp( this System.DateTime time ) {
            string str = $"{time.Year:0000}-{time.Month:00}-{time.Day:00}";
            return str;
        }

        static public string ToDateTimeStamp( this System.DateTime time ) {
            return $"{time.ToDateStamp()}_{time.ToTimeStamp()}";
        }
    }
    
    
    #region Telem
    public struct Telem {
        const char TAG_SPLIT_CHAR = ' ';
        
        static public Vector3 V3_INVALID = new Vector3(-999, -999, -999);

        public float time;
        public Vector3 position, p0, p1, p2;
        public Quaternion rotation;
        public string tag;
        public string[] splitTag;

        public Telem(float t, Vector3 pos, Quaternion rot, string tg = "") {
            this.time = t;
            this.position = pos;
            this.rotation = rot;
            this.tag = tg;
            this.splitTag = tg.Split(TAG_SPLIT_CHAR);
            p0 = p1 = p2 = V3_INVALID; // Give these a base value that can be replaced in the Telemetry_LocalViewer
        }

        override public string ToString() {
            Vector3 rot = rotation.eulerAngles;
            return $"{tag}\t{time:0.00}\t{position.x:0.00}\t{position.y:0.00}\t{position.z:0.00}\t{rot.x:0.00}\t{rot.y:0.00}\t{rot.z:0.00}";
        }

        static public Telem FromString(string s) {
            string[] bits = s.Split('\t');
            if (bits.Length != 5 && bits.Length != 8) {
                Debug.LogError($"Telem string parsed into wrong number of bits: {bits.Length}.\n{s}");
                return new Telem(-1, Vector3.zero, Quaternion.identity, "ERROR");
            }
            string tag = bits[0];
            float time;
            Vector3 pos = Vector3.zero;
            Vector3 rot = Vector3.zero;
            try {
                time = float.Parse(bits[1]);
                pos.x = float.Parse(bits[2]);
                pos.y = float.Parse(bits[3]);
                pos.z = float.Parse(bits[4]);
            } catch {
                Debug.LogError($"Telem floats did not parse correctly.\n{s}");
                return new Telem(-1, Vector3.zero, Quaternion.identity, "ERROR");
            }
            try {
                rot.x = float.Parse(bits[5]);
                rot.y = float.Parse(bits[6]);
                rot.z = float.Parse(bits[7]);
            } catch {
                // Don't actually need to do anything here, just ignore missing rotation. - JGB
            }
            return new Telem(time, pos, Quaternion.Euler(rot), tag);
        }
    }
    #endregion
}
