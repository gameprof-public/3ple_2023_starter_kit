using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class InWorldButton_2022_1 : MonoBehaviour
{
    [Header("Drag and Drop Interactables")]
    [SerializeField] private ButtonInteractable[] onPressInteractables = new ButtonInteractable[0];
    [SerializeField] private ButtonInteractable[] onUnpressInteractables = new ButtonInteractable[0];

    [Header("Event-Based Interactables")]
    public UnityEvent OnPound;
    public UnityEvent OnUnpound;

    private bool pressed = false;

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

#if UNITY_EDITOR
    public void OnDrawGizmosSelected()
    {
        //Draw gizmos for the interactables
        foreach (ButtonInteractable bi in onPressInteractables)
        {
            GameObject iObject = bi.gameObject;
            AnimationUtility.DrawBezierGizmo(transform, iObject.transform, Color.yellow);

            //Also draw gizmos for the interactable
            bi.OnDrawGizmosSelected();
        }
        foreach (ButtonInteractable bi2 in onUnpressInteractables)
        {
            GameObject iObject = bi2.gameObject;
            AnimationUtility.DrawBezierGizmo(transform, iObject.transform, Color.yellow);

            //Also draw gizmos for the interactable
            bi2.OnDrawGizmosSelected();
        }

        //Draw gizmos for the event-based ones
        for (int i =0; i < OnPound.GetPersistentEventCount(); i++)
        {
            MonoBehaviour eMono = OnPound.GetPersistentTarget(i) as MonoBehaviour;
            Transform eT = eMono.transform;
            AnimationUtility.DrawBezierGizmo(transform,eT, Color.yellow);
        }
        for (int i = 0; i < OnUnpound.GetPersistentEventCount(); i++)
        {
            MonoBehaviour eMono = OnUnpound.GetPersistentTarget(i) as MonoBehaviour;
            Transform eT = eMono.transform;
            AnimationUtility.DrawBezierGizmo(transform, eT, Color.yellow);
        }
    }
#endif

    public void GetPressedIfPounded(Collider coll)
    {
        //Check if it's the player
        PlayerController pCon = coll.GetComponent<PlayerController>();

        //Return if it's not the player or if the player isn't ground pounding
        if (pCon == null || PlayerController.GetPlayerState() != PlayerController.ePlayerState.GROUNDPOUND)
        {
            return;
        }

        anim.SetBool("pressed", true);
        pressed = true;

        //Actually proc the interactables
        OnPound.Invoke();
        foreach (ButtonInteractable bi in onPressInteractables)
        {
            bi.OnPress();
        }
    }

    public void GetUnpressedIfPressed(Collider coll)
    {
        //Check if it's the player
        PlayerController pCon = coll.GetComponent<PlayerController>();

        //Return if it's not the player or if the player isn't ground pounding
        if (pCon == null || !pressed)
        {
            return;
        }

        anim.SetBool("pressed", false);
        pressed = false;

        //Actually trigger
        OnUnpound.Invoke();
        foreach (ButtonInteractable bi in onUnpressInteractables)
        {
            bi.OnUnpress();
        }
    }
}
