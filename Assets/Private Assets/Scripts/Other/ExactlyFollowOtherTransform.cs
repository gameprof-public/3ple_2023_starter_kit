using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExactlyFollowOtherTransform : MonoBehaviour
{
    [SerializeField] private Transform otherTransform;
    private void Update()
    {
        transform.position = otherTransform.position;
        transform.rotation = otherTransform.rotation;
    }
}
