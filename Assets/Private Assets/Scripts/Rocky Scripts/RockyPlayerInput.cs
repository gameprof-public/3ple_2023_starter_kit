using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class RockyPlayerInput : MonoBehaviour {
    static bool DEBUG_RCI = false;

    public Transform        cameraTransform;
    public CinemachineBrain cameraBrain;

    [TextArea(1, 4)]
    public string inputState;
    

    private PlayerInput pInput;
    private RockyCharacterController rCC;

    private RockyCharacterInputs rCICurrent, rCINext;

    private void Awake() {
        rCC = GetComponentInParent<RockyCharacterController>();
        pInput = GetComponent<PlayerInput>();
        rCICurrent = new RockyCharacterInputs();
        rCINext = new RockyCharacterInputs();
        rCICurrent.RespawnAt = rCINext.RespawnAt = 0; // Just to be sure
    }

    private void Update() {
        HandleCharacterInput();
        inputState = rCICurrent.ToString();

#if TEST_ADD_VELOCITY
        // Apply impulse
        if ( Input.GetKeyDown( KeyCode.Q ) ) {
            rCC.Motor.ForceUnground( 0.1f );
            rCC.AddVelocity( Vector3.one * 10f );
        }
#endif
    }

    private void LateUpdate() {
        cameraBrain?.ManualUpdate();
    }

    private void HandleCharacterInput() {
        // Pull latest camera rotation information
        rCINext.CameraRotation = cameraTransform.rotation;
        // Copies values of rCINext to rCICurrent
        rCICurrent = rCINext; 
        // Apply inputs to character
        rCC.SetInputs( ref rCICurrent );
        if ( DEBUG_RCI ) Debug.Log( rCICurrent );

        // rCINext carries over values for the following call to HandleCharacterInput()
        //  until they are overridden by additional calls to OnMove() and such.
        //  It is done this way so that the rCICurrent passed to rCC is constant until the next frame.

        // Progress ButtonState values (converting pressed to down, for instance)
        rCINext.ProgressButtonStates();
        rCINext.RespawnAt = 0; // Reset this to 0 to avoid constantly teleporting
    }

#region PlayerInput Functions
    private void OnMove( InputValue value ) {
        Vector2 v2Val = value.Get<Vector2>();
        //Debug.Log( $"moveValue: {moveValue}" );
        rCINext.MoveAxisForward = v2Val.y;
        rCINext.MoveAxisRight = v2Val.x;
    }

    //Cinemachine is taking care of this
    //private void OnLookInput( InputValue value ) {
    //    //Vector2 v2Val = value.Get<Vector2>();
    //    ////Debug.Log( $"moveValue: {moveValue}" );
    //    //rCINext.MoveAxisForward = v2Val.y;
    //    //rCINext.MoveAxisRight = v2Val.x;
    //}

    // OnLook() is handled by the input messages being forwarded to
    //  SteppedRotationCameraController on the GameCamera GameObject.

    //bool jumpDownIsReady = true;
    private void OnJump( InputValue value ) {
        float fVal = value.Get<float>();
        rCINext.Jump.Set( fVal >= .05f );
    }
    
    private void OnCrouch( InputValue value ) {
        float fVal = value.Get<float>();
        rCINext.Crouch.Set( fVal >= .05f );
    }

    private void OnDive( InputValue value ) {
        float fVal = value.Get<float>();
        rCINext.Dive.Set( fVal >= .05f );
    }
    private void OnNoClip( InputValue value ) {
        float fVal = value.Get<float>();
        rCINext.NoClip.Set( fVal >= .05f );
    }
    
    private void OnSpawn1(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 1; }
    private void OnSpawn2(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 2; }
    private void OnSpawn3(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 3; }
    private void OnSpawn4(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 4; }
    private void OnSpawn5(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 5; }
    private void OnSpawn6(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 6; }
    private void OnSpawn7(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 7; }
    private void OnSpawn8(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 8; }
    private void OnSpawn9(InputValue value) { if ( value.Get<float>() >= 0.05f ) rCINext.RespawnAt = 9; }
    
#endregion
}

public struct RockyCharacterInputs {
    public float       MoveAxisForward;
    public float       MoveAxisRight;
    public Quaternion  CameraRotation;
    public ButtonState Jump, Crouch, Dive, NoClip;
    public int         RespawnAt; // The default value for this is 0 for NO TELEPORT

    public void ProgressButtonStates() {
        Jump.Progress();
        Crouch.Progress();
        Dive.Progress();
        NoClip.Progress();
    }

    public override string ToString() {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append( $"Move: [{MoveAxisForward:0.00}, {MoveAxisRight:0.00}]" );
        sb.Append( $"  Jump: {Jump.ToString()}" );
        sb.Append( $"  Crch: {Crouch.ToString()}" );
        sb.Append( $"  NoCl: {NoClip.ToString()}" );
        sb.Append( $"  Spwn: {RespawnAt}" );
        // sb.Append( $"  Dive: {Dive.ToString()}" );
        sb.Append( $"  Rot: {CameraRotation.eulerAngles}" );
        return sb.ToString();
    }
}