using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowDistanceOnText : MonoBehaviour
{
    public Transform t1;
    public Transform t2;

    private TMP_Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = Vector3.Distance(t1.position, t2.position).ToString();
    }
}
