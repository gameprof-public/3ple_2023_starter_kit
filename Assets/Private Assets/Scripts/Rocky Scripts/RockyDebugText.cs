using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Simple way to show debug messages above Rocky. JGB 2022-10-30
/// </summary>
[RequireComponent(typeof(TMP_Text))]
public class RockyDebugText : MonoBehaviour {
    static RockyDebugText _S;

    public bool showDebugText = false;
    TMP_Text tMP;

    void Start() {
        _S = this;
        tMP = GetComponent<TMP_Text>();
        tMP.text = "";

        tMP.enabled = false;
#if UNITY_EDITOR
        // ONLY show debug messages in the Editor
        if (showDebugText) tMP.enabled = true;
#endif
    }

    private void Update() {
        Vector3 camVec = Camera.main.transform.position;
        // Looking at the camera would actually flip text the opposite direction, so look away instead.
        camVec += ( transform.position - camVec ) * 2;
        transform.LookAt( camVec, Vector3.up );
    }

#if UNITY_EDITOR
    private void OnValidate() {
        if (Application.isPlaying) {
            if (tMP != null) tMP.enabled = showDebugText;
        }
    }
#endif

    static public string text {
        get { return _S.tMP.text; }
        set {
            if ( _S == null ) return; // Protect against bugs if _S is null.
            _S.tMP.text = value;
        }
    }
}