using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A superclass for items that can be picked up by the player.
/// </summary>
/// 

[RequireComponent(typeof(SphereCollider))]
public class Pickup : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Check if it's the player
        if ( other.CompareTag("Player") )
        {
            GetPickedUp();
        }
    }

    protected virtual void GetPickedUp() {
        Destroy( gameObject );
    }
}
