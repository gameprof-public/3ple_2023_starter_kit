using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    [SerializeField] private Animator heartAnimationController;

    public void LoseHeart()
    {
        heartAnimationController.SetBool("Empty", true);
    }

    public void GainHeart()
    {
        heartAnimationController.SetBool("Empty", false);
    }
}
