using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class WorldUISpawner : MonoBehaviour
{
    [SerializeField] private GameObject worldUIPrefab;

    public GameObject SpawnWorldUIObject(Vector3 position, float lifetime)
    {
        GameObject gO = GameObject.Instantiate<GameObject>(worldUIPrefab);
        gO.transform.position = position;

        IEnumerator coro = DestroyAfterTimer(gO, lifetime);
        StartCoroutine(coro);


        return gO;
    }

    IEnumerator DestroyAfterTimer(GameObject objectToDestroy, float timer)
    {
        yield return new WaitForSeconds(timer);
        Destroy(objectToDestroy);
    }
}

#if UNITY_EDITOR
/// <summary>
/// This obscures editor settings from students.
/// </summary>
[CustomEditor(typeof(WorldUISpawner))]
public class WorldUISpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
    }
}
#endif