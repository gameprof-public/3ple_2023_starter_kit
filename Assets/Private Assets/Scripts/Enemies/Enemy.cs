using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Enemy : MonoBehaviour
{
    [Header("Generic Enemy Settings")]
    [HideInInspector] public NavMeshAgent agent;

    //Health Variables
    [HideInInspector] public float health;
    [SerializeField] private bool vulnerableToDamage;

    //Speed variable
    [SerializeField] private float speed = 3;

    [SerializeField] protected List<Vector3> patrolPoints;

    [SerializeField] public GameObject patrolObject = null;
    [SerializeField] public bool switchToCustomPoints = false;

    [Header("Patrol Settings")]
    [SerializeField] protected float patrolRadius;
    protected bool onPatrolPath = false;
    protected List<float> minList;

    [Header("Aggro Settings")]
    [SerializeField] protected float minIdleTime;
    [SerializeField] protected float maxIdleTime;
    protected float timeIdleEnd = -1;
    [SerializeField] protected float aggroRange = 5f;
    [SerializeField] protected float escapeRange = 9f;

    //Event variables
    private EnemyEvent OnDeath;

    // Start is called before the first frame update
    public virtual void Start()
    {
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }

        agent.speed = speed;

        if (OnDeath == null)
        {
            OnDeath = new EnemyEvent();
        }
    }

    /// <summary>
    /// This function can be called by a spawning class to make sure the OnDeath event is declared before it needs subscribers.
    /// This makes it so a subscription can happen before Enemy.Start() when needed.
    /// </summary>
    public void ForceEventInitialization()
    {
        if (OnDeath == null)
        {
            OnDeath = new EnemyEvent();
        }
    }

    public virtual void Update()
    {
        if(patrolObject != null && patrolObject.GetComponent<Patrol>().movementPointList.Count != 0) 
        {
            patrolPoints = patrolObject.GetComponent<Patrol>().movementPointList;
            switchToCustomPoints = true;
        }

    }

    public bool IsVulnerableToDamae()
    {
        return vulnerableToDamage;
    }

    public void SetMaxHealth(float maxHealth)
    {
        health = maxHealth;
    }

    public Vector3 RandomNavmeshPointWithinSphere(float radius)
    {
        //Built with reference from here : https://answers.unity.com/questions/475066/how-to-get-a-random-point-on-navmesh.html

        Vector3 point = Vector3.zero;

        Vector3 randomSpot = Random.insideUnitSphere * radius;
        randomSpot += transform.position;

        //Find a spot on the mesh
        NavMeshHit hit;
        NavMesh.SamplePosition(randomSpot, out hit, radius, 1);

        point = GetPositionOnNavmesh(randomSpot, maxRadius: radius);

        return point;
    }

    public static Vector3 GetPositionOnNavmesh(Vector3 referencePosition, float maxRadius = 99)
    {
        Vector3 point;

        NavMeshHit hit;
        NavMesh.SamplePosition(referencePosition, out hit, maxRadius, 1);

        point = hit.position;

        return point;
    }

    protected void ClearDestination()
    {
        agent.destination = transform.position;
    }

    public void CheckIfJumpedOn(Collider other)
    {
        if (other.tag != "Player") return;
        else GetJumpedOn();
    }

    public void CheckIfJumpedOn(Collider other, HitBox hitBox) {
        if ( other.tag != "Player" ) return;

        // TODO: Do something with the hitBox information (like making sure Rocky only damages enemies if above them)
        if (RockyCharacterController.IsRCCCollider(other)) { // We know for sure this is Rocky
            if (RockyCharacterController.IsAbove(hitBox.hitBoxBottomY)) {
                GetJumpedOn();
            }
        }
    }

    public virtual void HurtPlayer(Collider other)
    {
        //TODO: All of this should really be in PlayerController
        if (other.tag != "Player") return; //Don't hurt the player when I run into other things.

        if (PlayerController.CheckIfInvulnerable()) return; //Don't do anything if they're invulnerable

        // Never damage the player on the same frame that they jumped on this Enemy (not perfect, but better than nothing) - JGB 2022-10-30
        if ( jumpedOnTime == Time.time ) return;

        //damage the player
        PlayerController.ModifyHealth(-1);

        //Knock the player back
        PlayerController.KnockbackPlayer(transform.position);
    }


    public virtual void HurtPlayer( Collider other, HitBox hitBox ) {
        if (RockyCharacterController.IsRCCCollider(other) ) {
            if (!RockyCharacterController.IsAbove(hitBox.hitBoxTopY)) {
                HurtPlayer( other );
            }
        }
    }

    protected bool PlayerDetected()
    {
        //check if the player is roughly on the same plane as the enemy, if not no good.
        Vector3 playerPos = PlayerController.GetPlayerPosition();
        if (Mathf.Abs(transform.position.y - playerPos.y) > 1) return false;

        //See if the player is too far away.
        if (Vector3.Distance(playerPos, transform.position) > aggroRange) return false;

        //Welp, guess I'll aggro
        return true;
    }

    public virtual void AddPatrolPoint()
    {
        Debug.Log("I pushed the patrol point button.");
    }

    protected virtual Vector3 FindNearestPointOnPath(List<Vector3> patrolList, Vector3 mainLoc)
    {


        float x2 = mainLoc.x;       // location of the enemey
        float y2 = mainLoc.y;
        float z2 = mainLoc.z;
        for (int i = 0; i < patrolList.Count; i++)
        {
            float x1 = patrolList[i].x; //I switched 2 and 1 here normally | supposed to be x2-x1^2 ...
            float y1 = patrolList[i].y; //But since its just getting the magnitued, all we care is abs delta
            float z1 = patrolList[i].z; //Location of the point in the list.


            float distance = Mathf.Sqrt(Mathf.Pow((x1 - x2), 2) +
                Mathf.Pow((y1 - y2), 2) + Mathf.Pow((z1 - z2), 2)); //Distance between the two
            minList.Add(distance);
        }
        int index = 0;
        float min = minList[0];

        for (int k = 0; k < minList.Count; k++)  //Tried to find an easy min function, apparently it doesn't exist
        {
            if (minList[k] < min)  //This just finds the minimum, keeps track of the index.
            {                     // Maybe we shoulda used a dictionary? Easy change to make
                min = minList[k];
                index = k;
            }
        }

        return patrolList[index];

    }

    protected virtual void SetMoveDestination(Vector3 destination)
    {
        agent.SetDestination(destination);
    }

    private float jumpedOnTime = -999;
    public virtual bool GetJumpedOn()
    {
        //Check if the player is jumping, only allow this if they are
        if (PlayerController.GetPlayerState() != PlayerController.ePlayerState.JUMPING && PlayerController.GetPlayerState() != PlayerController.ePlayerState.GROUNDPOUND) return false;

        //Handle enemy bounce
        PlayerController.HandleEnemyBounce();

        jumpedOnTime = Time.time;

        return true;
    }

    protected virtual void Die()
    {
        OnDeath.Invoke(this);
        Destroy(gameObject);
    }

    public void SubscribeToOnDie(UnityAction<Enemy> subscribedFunction)
    {
        OnDeath.AddListener(subscribedFunction);
    }

    public void UnSubscribeToOnDie(UnityAction<Enemy> subscribedFunction)
    {
        OnDeath.RemoveListener(subscribedFunction);
    }
}

[System.Serializable]
public class EnemyEvent : UnityEvent<Enemy>
{

}

#if UNITY_EDITOR
[CustomEditor(typeof(Enemy))]
//[CanEditMultipleObjects]
public class EnemyEditor : Editor
{
    public override void OnInspectorGUI() {
        
        bool altHeld = ( Event.current.modifiers == EventModifiers.Alt );
        if ( serializedObject.forceChildVisibility || altHeld ) {
            serializedObject.forceChildVisibility = EditorGUILayout.ToggleLeft( "Show hidden fields…", serializedObject.forceChildVisibility );
        }

        base.OnInspectorGUI();

        Enemy enemy = target as Enemy;

        if (GUILayout.Button("Add Patrol Point"))
        {
            enemy.AddPatrolPoint();
        }
    }
}
#endif