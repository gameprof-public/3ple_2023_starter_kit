using UnityEngine;

/// <summary>
/// This class includes public functions for helping handle animation of the player character using the new player controller.
/// </summary>
public class RockyAnimationController : MonoBehaviour {
    static private readonly int AP_Jumping         = Animator.StringToHash( "Jumping" );
    static private readonly int AP_LongJumping     = Animator.StringToHash( "LongJumping" );
    static private readonly int AP_Diving          = Animator.StringToHash( "Diving" );
    static private readonly int AP_GroundPoundDrop = Animator.StringToHash( "GroundPoundDrop" );
    static private readonly int AP_GroundPoundFlip = Animator.StringToHash( "GroundPoundFlip" );
    static private readonly int AP_WallSlide       = Animator.StringToHash( "WallSlide" );
    static private readonly int AP_Hanging         = Animator.StringToHash( "Hanging" );
    static private readonly int AP_Mantle          = Animator.StringToHash( "Mantle" );
    static private readonly int AP_Running         = Animator.StringToHash( "Running" );
    static private readonly int AP_Crouching       = Animator.StringToHash( "Crouching" );
    static private readonly int AP_Crawling        = Animator.StringToHash( "Crawling" );
    static private readonly int AP_Rolling         = Animator.StringToHash( "Rolling" );
    static private readonly int AP_Knockback       = Animator.StringToHash( "Knockback" );
    static private readonly int AP_RunBlend        = Animator.StringToHash( "RunBlend" );
    static private readonly int AP_Climbing        = Animator.StringToHash( "Climbing" );
    
    [Header("Set in inspector")]
    [Tooltip("Drag in the game object that holds the character's graphics")]
    [SerializeField] private GameObject graphicsGameObject;
    private Animator anim;

    // Instead of eAnimationState, this now uses PlayerController.ePlayerState - JGB 2022-10-12
    // public enum eAnimationState { WALKING, JUMPING, LONGJUMPING, CROUCHING, CRAWLING, CLIMBING, MANTLE, DIVING, ROLLING, KNOCKBACK, GROUNDPOUND, WALLSLIDE, SLOPESLIDE, HANGING, JUSTLANDED }
    //public enum ePlayerState    { WALKING, JUMPING, LONGJUMPING, CROUCHING, CRAWLING, CLIMBING, MANTLE, DIVING, ROLLING, KNOCKBACK, GROUNDPOUND, WALLSLIDE, SLOPESLIDE, HANGING, JUSTLANDED }

    //Private variables for tracking our current state
    private bool                          initialized  = false;
    private PlayerController.ePlayerState _currentState = PlayerController.ePlayerState.WALKING;
    private bool                          grounded;
    private float                         currentSpeed;
    private float                         currentHorizontalSpeed;

    public PlayerController.ePlayerState currentState {
        get { return _currentState; }
        private set { _currentState = value; }
    }

    public PlayerController.ePlayerState GetCurrentState() {
        return currentState;
    }

    //Private variables for setting speed of the animations
    private float crawlSpeed;
    private float climbSpeed;
    private float speedOfFullRun;

    //Use these functions to actually fire animations - they should be hooked up in the new character controller
#region functions to hook up
    /// <summary>
    /// Call this function when the player spawns to prep the animation controller.
    /// Any floats in the paramaters that seem extraneous can just be set to 0 without any harm.
    /// </summary>
    /// <param name="maxCrawlSpeed">The speed of a max crawl.</param>
    /// <param name="speedRequiredForFullRunAnimation">How fast the character has to be moving to blend to the full run animation.</param>
    public void InitializeAnimationController(float maxCrawlSpeed, float maxClimbSpeed, float speedRequiredForFullRunAnimation)
    {
        if ( initialized ) Debug.LogError( "Attemptd to re-initialize RockyAnimationController." );
        anim = GetComponentInChildren<Animator>();

        crawlSpeed = maxCrawlSpeed;
        speedOfFullRun = speedRequiredForFullRunAnimation;
        climbSpeed = maxClimbSpeed;

        initialized = true;
    }

    /// <summary>
    /// This function should be called every fixed update to update the animation controller on the current state of the character
    /// </summary>
    public void UpdateAnimationState(PlayerController.ePlayerState newCurrentState, bool isGrounded, Vector3 currentVelocity)
    {
        grounded = isGrounded;
        _currentState = newCurrentState;
        currentSpeed = currentVelocity.magnitude;

        //Calculate current horizontal speed
        // TODO: Need to make this work with Motor.CharacterUp != Vector3.up to allow variable gravity direction. - JGB 2022-10-09
        Vector3 horizontalVel = new Vector3(currentVelocity.x, 0, currentVelocity.z);
        currentHorizontalSpeed = horizontalVel.magnitude;

        HandleAnimation();
    }
    
    
    
#endregion

#region private functions
    private void HandleAnimation()
    {
        //Debug.Log(anim.speed.ToString());
        anim.speed = 1;
        if (grounded)
        {
            //Make sure we're not playing the knocked down animation:
            anim.SetBool(AP_Knockback, false);
            if (_currentState == PlayerController.ePlayerState.CROUCHING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool(AP_Crouching, true);
            }
            else if (_currentState == PlayerController.ePlayerState.CRAWLING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool(AP_Crawling, true);
                anim.speed = currentSpeed / crawlSpeed;
            }
            else if (_currentState == PlayerController.ePlayerState.ROLLING)
            {
                DisableGroundedAnimationBools();
                anim.SetBool(AP_Rolling, true);
            }
            else
            {
                DisableGroundedAnimationBools();
                if (currentHorizontalSpeed > .05)
                {
                    anim.SetBool(AP_Running, true);
                    float blendFloat = currentHorizontalSpeed / speedOfFullRun;
                    if (blendFloat > 1) blendFloat = 1;
                    anim.SetFloat(AP_RunBlend, blendFloat);
                }
                //else DisableGroundedAnimationBools();
            }


            DisableJumpingAnimationBools();
        }
        else //Airborn
        {
            if (_currentState == PlayerController.ePlayerState.GROUNDPOUND) {
                if (currentSpeed <= 1) {
                    anim.SetBool(AP_GroundPoundDrop, false);
                    anim.SetBool(AP_GroundPoundFlip, true);
                } else {
                    anim.SetBool(AP_GroundPoundDrop, true);
                    anim.SetBool(AP_GroundPoundFlip, false);
                }
            }
            else if (_currentState == PlayerController.ePlayerState.LONGJUMPING)
            {
                anim.SetBool(AP_LongJumping, true);
                anim.SetBool(AP_Jumping, false);
            }
            else if (_currentState == PlayerController.ePlayerState.CLIMBING)
            {
                anim.SetBool(AP_Climbing, true);
                anim.CrossFade("Climbing", 0);
                anim.speed = currentSpeed / climbSpeed / Time.deltaTime;
            }
            else if (_currentState == PlayerController.ePlayerState.HANGING)
            {
                anim.SetBool(AP_Hanging, true);
            }
            else if (_currentState == PlayerController.ePlayerState.MANTLE)
            {
                anim.SetBool(AP_Mantle, true);
            }
            else if (_currentState == PlayerController.ePlayerState.WALLSLIDE) {
                anim.SetBool(AP_WallSlide, true);
            }
            else
            {
                anim.SetBool(AP_Jumping, true);
            }

            DisableGroundedAnimationBools();
        }
    }

    private void DisableGroundedAnimationBools()
    {
        anim.SetBool(AP_Running, false);
        anim.SetBool(AP_Crouching, false);
        anim.SetBool(AP_Crawling, false);
        anim.SetBool(AP_Rolling, false);
    }

    private void DisableJumpingAnimationBools()
    {
        anim.SetBool(AP_Jumping, false);
        anim.SetBool(AP_LongJumping, false);
        anim.SetBool(AP_Diving, false);
        anim.SetBool(AP_GroundPoundDrop, false);
        anim.SetBool(AP_GroundPoundFlip, false);
        anim.SetBool(AP_WallSlide, false);
        anim.SetBool(AP_Hanging, false);
        anim.SetBool(AP_Mantle, false);
    }

#endregion
}
