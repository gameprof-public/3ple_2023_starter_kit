using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathUtility
{
    public static float GetNormalDotProductFromRaycastHit(RaycastHit hit)
    {
        float dot = Vector3.Dot(Vector3.up, hit.normal);
        return dot;
    }
}
