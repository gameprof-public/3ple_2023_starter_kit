using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MrBrainwash : MonoBehaviour
{
    public RockyCharacterController rockyCharacterController;

    private PlayerController pc;
    private CharacterController cc;
    private GameObject graphicsGO;

    // Start is called before the first frame update
    void Start()
    {
        pc = GetComponent<PlayerController>();
        cc = GetComponent<CharacterController>();

        pc.enabled = false;
        cc.enabled = false;

        graphicsGO = transform.Find("Graphics").gameObject;
        graphicsGO.SetActive(false);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = rockyCharacterController.transform.position;
    }
}
