using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// These nodes are spawned into the scene by StartPoint's custom editor, and draw a gizmo
/// on themselves indicating what number button to press to go to that location.
/// </summary>
public class StartPointHandle : MonoBehaviour
{
    private int startNodeNumber;

    public void SetStartNodeNumber(int newStartNodeNumber)
    {
        startNodeNumber = newStartNodeNumber;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        float radius = 0.325f;
        Vector3 gizmoCenter = transform.position + Vector3.up * radius;

        Gizmos.DrawLine(gizmoCenter, (gizmoCenter + transform.forward*2));

        Gizmos.DrawSphere(gizmoCenter, radius);
    }
}

